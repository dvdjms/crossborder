<?php
// Before removing this file, please verify the PHP ini setting `auto_prepend_file` does not point to this.

// This file was the current value of auto_prepend_file during the Wordfence WAF installation (Wed, 05 Aug 2020 10:19:27 +0000)
if (file_exists('/scripts/env.php')) {
	include_once '/scripts/env.php';
}
if (file_exists('/srv/htdocs/wp-content/plugins/wordfence/waf/bootstrap.php')) {
	define("WFWAF_LOG_PATH", '/srv/htdocs/wp-content/wflogs/');
	include_once '/srv/htdocs/wp-content/plugins/wordfence/waf/bootstrap.php';
}
?>