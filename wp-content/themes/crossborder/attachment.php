<?php
/**
 * The template for displaying pages
 
 */

get_header(); ?>
	
	
		
		<!-- attachement -->
	
		
			<?php if (wp_attachment_is_image($post->id)) :
					$att_image = wp_get_attachment_image_src( $post->id, "large");
				?>
				<div class="attachment">
					
					<img src="<?php echo $att_image[0];?>" width="<?php echo $att_image[1];?>" height="<?php echo $att_image[2];?>"  class="attachment-medium" alt="<?php echo $post->post_excerpt; ?>" />
					
					<?php if($post->post_excerpt):?>
					<div class="caption"> <p><?php echo $post->post_excerpt; ?></p></div>
					<?php endif;?>
					
					
					<?php include(get_template_directory().'/template-parts/content-share.php') ?>
				
				</div>
				<?php endif; ?>
		
	
	

<?php get_footer(); ?>
