<?php
/**
 * The template for displaying all pages.

 */
get_header(); ?>
<!--page -->
<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'template-parts/content', 'page' ); ?>
<?php get_template_part( 'template-parts/content', 'blocks' ); ?>






<?php endwhile; // End of the loop. ?>
<?php get_footer(); ?>
