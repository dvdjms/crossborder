<?php
/**
 * The template for displaying all single whitepaper posts.

 */

get_header(); ?>

<!--single whitepaper -->
			<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part( 'template-parts/content', 'single-white-paper' ); ?>

		<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
