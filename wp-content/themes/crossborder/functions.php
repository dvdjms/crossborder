<?php
/**
 * xborder functions and definitions
 *
 * @package xborder
 */
require get_template_directory() . '/inc/theme_setup.php';
//change post label;
//require get_template_directory() . '/inc/change_post_labels.php';
//custom post;
require get_template_directory() . '/inc/custom_post.php';
//custom taxonomy;
require get_template_directory() . '/inc/custom_taxonomy.php';

require get_template_directory() . '/inc/register_nav.php';

//require get_template_directory() . '/inc/register_sidebar.php';

//require get_template_directory() . '/inc/google_fonts.php';

require get_template_directory() . '/inc/typekit.php';

require get_template_directory() . '/inc/html_5_shim.php';
/*** Enqueue scripts and styles.*/
require get_template_directory() . '/inc/enqeue.php';

require get_template_directory() . '/inc/custom_login.php';

require get_template_directory() . '/inc/clean_the_head.php';

//require get_template_directory() . '/inc/testimonials.php';
//
require get_template_directory() . '/inc/image_sizes.php';

require get_template_directory() . '/inc/acf.php';

require get_template_directory() . '/inc/xborder.php';

require get_template_directory() . '/inc/vimeo.php';


add_theme_support( 'automatic-feed-links' );
if ( ! isset( $content_width ) ) $content_width = 900;

/**
 * Hide custom fields
 */
$whitelist = array(
    'David'
);
$current_user = wp_get_current_user();

if(!in_array($current_user->display_name, $whitelist)) add_filter('acf/settings/show_admin', '__return_false');



function xborder_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
	if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
	}

	
	$time_string = get_the_date();
	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'xborder' ),
		 '<span class="date">' . $time_string . '</span>'
	);

	

	echo $posted_on ; // WPCS: XSS OK.

}




/*-----------------------------------------------------------------------------
	
		Add page slug to body class

---------------------------------------------------------------------------*/

function add_slug_body_class( $classes ) {

global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}
return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );

/*-----------------------------------------------------------------------------
	
		Get an image

---------------------------------------------------------------------------*/
function getImage($array, $size){
			$newArr = array();
			if (count($array)<=1) return false;
			
			$id = $array['ID'];
			
			$sized = $array['sizes'][$size];
			if(!$sized) {
				$s='';
				$sized = $array['url'];
			}
				
			//
			if($size!=''){
				$img_width = $array['sizes'][$size. '-width' ];
				$img_height = $array['sizes'][$size. '-height' ];
			} else {
				$img_width = $array['width'];
				$img_height = $array['height'];	
			}
			
			$ratio = round($img_width / $img_height, 2);
			if($ratio > 1) {
				$aspect = 'landscape';
			} else {
				$aspect = 'portrait';
			}
			
			$img_src = wp_get_attachment_image_src( $id, $size );
			$img_srcset = wp_get_attachment_image_srcset( $id, $size );
			$img_srcset_sizes = wp_get_attachment_image_sizes( $id, $size );
			$img_alt_text = $array['alt'];
			$img_meta = wp_get_attachment_metadata( $id );
			$img_title = $array['title'];
			$img_caption = $array['caption'];
			$img_desc = $array['description'];
			

			
			$img = '<img class="' . $aspect . '" '; 
			$img .= 'src="' . esc_url( $img_src[0] )  . '" ';
			$img .= 'srcset="' . esc_attr( $img_srcset )  . '" ';
			$img .= 'sizes="' . esc_attr( $img_srcset_sizes )  . '" ';
			$img .= 'alt="' . $img_alt_text  . '" />';
			
			$unveil = '<img class="unveil ' . $aspect . '" '; 
			$unveil .= 'src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 ' . $img_width . ' ' . $img_height . '\'%3E%3C/svg%3E" '; 
			$unveil .= 'data-src="' . esc_url( $img_src[0] )  . '" ';
			if($img_srcset) $unveil .= 'data-srcset="' . esc_attr( $img_srcset )  . '" ';
			$unveil .= 'sizes="' . esc_attr( $img_srcset_sizes )  . '" ';
			$unveil .= 'width="' . $img_width . '"';
			$unveil .= 'height="' . $img_height . '"';
			$unveil .= 'style="--ratio:' . $ratio . ';" ';
			$unveil .= 'alt="' . $img_alt_text  . '" />';
			
			//data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 3 2'%3E%3C/svg%3E
			
			$layzr = '<img class="lazy-image ' . $aspect . '" '; 
			$layzr .= 'src="data:image/svg+xml,%3Csvg xmlns=\'http://www.w3.org/2000/svg\' viewBox=\'0 0 ' . $img_width . ' ' . $img_height . '\'%3E%3C/svg%3E" '; 
			$layzr .= 'data-normal="' . esc_url( $img_src[0] )  . '" ';
			if($img_srcset) $layzr .= 'data-srcset="' . esc_attr( $img_srcset )  . '" ';
			$layzr .= 'sizes="' . esc_attr( $img_srcset_sizes )  . '" ';
			$layzr .= 'width="' . $img_width . '" ';
			$layzr .= 'height="' . $img_height . '" ';
			$layzr .= 'style="--ratio:' . $ratio . ';" ';
			$layzr .= 'alt="' . $img_alt_text  . '" />';


			
			
			$newArr['alt'] = $img_alt_text;
			$newArr['url'] = $sized;
			$newArr['width'] = $img_width;
			$newArr['height'] = $img_height;
			$newArr['caption'] = $img_caption;
			$newArr['img'] = $img;
			$newArr['unveil'] = $unveil;
			$newArr['lazy'] = $layzr;
			$newArr['srcset'] = $img_srcset;
			$newArr['srcset-sizes'] = $img_srcset_sizes;
		
			return $newArr;
	
}



/*-----------------------------------------------------------------------------
	
						Search with no term
		
---------------------------------------------------------------------------*/

add_filter( 'request', 'my_request_filter' );
function my_request_filter( $query_vars ) {
    if( isset( $_GET['s'] ) && empty( $_GET['s'] ) ) {
        $query_vars['s'] = " ";
    }
    return $query_vars;
}

/*-----------------------------------------------------------------------------
	
						Substring to nearest word

---------------------------------------------------------------------------*/
function subString($s, $n, $more='&hellip;'){
	
	$s = strip_tags ($s);
	if(strlen($s) > $n){
		$pos = strpos($s, ' ', $n - 10);
		if ($pos === false) {
			 $pos = strpos($s, ' ', $n - 20);
		}
		
		$string = substr($s,0,$pos ).$more; 
	} else {
		$string = $s;
	}
	$string = '<p>'.$string.'</p>';
	
	return $string;
	
}




/*-----------------------------------------------------------------------------
	
					Remove unwanted items from admin

---------------------------------------------------------------------------*/


function custom_menu_page_removing() {
   // remove_menu_page( 'edit.php' );
  remove_menu_page( 'edit-comments.php' );
  remove_menu_page( 'link-manager.php' );
}

add_action( 'admin_menu', 'custom_menu_page_removing' );




/*-----------------------------------------------------------------------------
	
						Remove admin bump

---------------------------------------------------------------------------*/


add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}

/*-----------------------------------------------------------------------------
	
		Admin style
		
---------------------------------------------------------------------------*/


function admin_style() {
  wp_enqueue_style('admin-styles', versioned_resource_file('/css/admin-override.css'));
}
add_action('admin_enqueue_scripts', 'admin_style');

/*-----------------------------------------------------------------------------
	
		Remove [...] from excerpt
		
---------------------------------------------------------------------------*/
function new_excerpt_more( $more ) {
    return ' ...';
}
add_filter('excerpt_more', 'new_excerpt_more');


/*-----------------------------------------------------------------------------
	
		Colums shortcode
		
---------------------------------------------------------------------------*/




function column_shortcode( $atts, $content='', $code = NULL ) {
	 extract(shortcode_atts(array(
      'columns' => 2,
   ), $atts));
  
    return '<div class="column column-size-'.$columns.'"><p>'. $content . '</p></div>';
}
add_shortcode( 'column', 'column_shortcode' );

/*-----------------------------------------------------------------------------
	
		News page content
		
---------------------------------------------------------------------------*/

add_action( 'edit_form_after_title', 'rgc_posts_page_edit_form' );
function rgc_posts_page_edit_form( $post ) {
	$posts_page = get_option( 'page_for_posts' );
	if ( $posts_page === $post->ID ) {
		add_post_type_support( 'page', 'editor' );
	}
}

/* -----------------------------------------------------------------------------------------------------------
															    
											REMOVE HTTP
															    
------------------------------------------------------------------------------------------------------------- */


function remove_http($url) {
   $disallowed = array('http://', 'https://');
   foreach($disallowed as $d) {
      if(strpos($url, $d) === 0) {
         return str_replace($d, '', $url);
      }
   }
   return $url;
}


/*------------------------------------------------------------------------------------------*/
/* HOLDING PAGE
/*------------------------------------------------------------------------------------------*/
add_action( 'template_redirect', function() {
   

    if(isset($_SERVER['REDIRECT_URL']) &&  $_SERVER['REDIRECT_URL'] === '/solutions/') {
    
        if (!is_user_logged_in() ) {
            wp_redirect( site_url( '/transfer-pricing/' ) );        // redirect all...
            exit();
        }
                
    }


});
?>