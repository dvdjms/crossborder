<?php

function xborder_nav_setup() {
register_nav_menus(array( 'main-menu' => __( 'Main', 'xborder' ), 
							
							'social' => __( 'Social', 'xborder' ),
							'footer-menu' => __( 'Footer', 'xborder' ),							
							
							 ));
}


add_action( 'after_setup_theme', 'xborder_nav_setup' );


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class($classes, $item){
     if($item->title == "Home"){ //Notice you can change the conditional from is_single() and $item->title
             $classes[] = "home-link";
     }
     
       if($item->title == "Contact"){ //Notice you can change the conditional from is_single() and $item->title
             $classes[] = "contact-link";
     }
     return $classes;
}
?>