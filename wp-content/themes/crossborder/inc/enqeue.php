<?php


$GLOBALS["TEMPLATE_URL"] = get_template_directory_uri()."/";
$GLOBALS["TEMPLATE_RELATIVE_URL"] = wp_make_link_relative($GLOBALS["TEMPLATE_URL"]);


function xborder_enqueue_scripts() {

	wp_enqueue_style( 'styles',versioned_resource_file("/css/styles.css")  );
	wp_enqueue_script( 'main', versioned_resource_file('/js/min/scripts.js'), array(), 0 , true);
	/*
	if ( is_page_template( 'templates/t_careers.php' ) ):
	  wp_enqueue_script( 'list', '//cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js', array(), 0 , true);
	  wp_enqueue_script( 'lever', versioned_resource_file('/js/lever/min/index-min.js'), array('list'), 0 , true);
	  
  endif;
  */
	
}
add_action( 'wp_enqueue_scripts', 'xborder_enqueue_scripts' );



function versioned_resource_file($file_url){
  $file = get_template_directory().$file_url;
  $file_version = "";

  if(file_exists($file)) {
    $file_version.= "?v=".filemtime($file);
  }

  return get_template_directory_uri().$file_url.$file_version;
}

?>