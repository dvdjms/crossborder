<?php

function xborder_custom_posts() {
	    ////CUSTOM Jobs
  $labels = array(
    'name'               => _x( 'Jobs', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'Jobs', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'Job', 'xborder' ),
    'add_new_item'       => __( 'Add New Jobs', 'xborder' ),
    'edit_item'          => __( 'Edit Jobs', 'xborder' ),
    'new_item'           => __( 'New Jobs', 'xborder'),
    'all_items'          => __( 'All Jobs', 'xborder' ),
    'view_item'          => __( 'View Job', 'xborder' ),
    'search_items'       => __( 'Search Jobs', 'xborder' ),
    'not_found'          => __( 'No Jobs found', 'xborder' ),
    'not_found_in_trash' => __( 'No Jobs found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Jobs'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Our Jobs',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor','excerpt'),
    'has_archive'   => true,
    'menu_icon'   => 'dashicons-welcome-write-blog',
	'exclude_from_search' => false,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'job' ),
  );
  register_post_type( 'job', $args ); 
  
  	    ////CUSTOM Person
  $labels = array(
    'name'               => _x( 'People', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'People', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'Person', 'xborder' ),
    'add_new_item'       => __( 'Add New People', 'xborder' ),
    'edit_item'          => __( 'Edit People', 'xborder' ),
    'new_item'           => __( 'New People', 'xborder'),
    'all_items'          => __( 'All People', 'xborder' ),
    'view_item'          => __( 'View Person', 'xborder' ),
    'search_items'       => __( 'Search People', 'xborder' ),
    'not_found'          => __( 'No People found', 'xborder' ),
    'not_found_in_trash' => __( 'No People found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'People'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Our People',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => false,
    'menu_icon'   => 'dashicons-buddicons-buddypress-logo',
	'exclude_from_search' => true,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'person' ),
  );
  register_post_type( 'person', $args ); 
  
  ////CUSTOM Podcast
  $labels = array(
    'name'               => _x( 'Podcasts', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'Podcasts', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'Podcast', 'xborder' ),
    'add_new_item'       => __( 'Add New Podcasts', 'xborder' ),
    'edit_item'          => __( 'Edit Podcasts', 'xborder' ),
    'new_item'           => __( 'New Podcasts', 'xborder'),
    'all_items'          => __( 'All Podcasts', 'xborder' ),
    'view_item'          => __( 'View Podcast', 'xborder' ),
    'search_items'       => __( 'Search Podcasts', 'xborder' ),
    'not_found'          => __( 'No Podcasts found', 'xborder' ),
    'not_found_in_trash' => __( 'No Podcasts found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Podcasts'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Our Podcasts',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => false,
    'menu_icon'   => 'dashicons-microphone',
	'exclude_from_search' => true,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'podcast' ),
  );
  register_post_type( 'podcast', $args ); 
  
  
  $labels = array(
    'name'               => _x( 'Countries', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'Countries', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'Country', 'xborder' ),
    'add_new_item'       => __( 'Add New Countries', 'xborder' ),
    'edit_item'          => __( 'Edit Countries', 'xborder' ),
    'new_item'           => __( 'New Countries', 'xborder'),
    'all_items'          => __( 'All Countries', 'xborder' ),
    'view_item'          => __( 'View Country', 'xborder' ),
    'search_items'       => __( 'Search Countries', 'xborder' ),
    'not_found'          => __( 'No Countries found', 'xborder' ),
    'not_found_in_trash' => __( 'No Countries found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Countries'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Our Countries',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array( 'title', 'editor'),
    'has_archive'   => false,
    'menu_icon'   => 'dashicons-admin-site-alt',
	'exclude_from_search' => true,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'solutions/transfer-pricing/country-regulations' ),
  );
  register_post_type( 'country', $args ); 
  
  
   $labels = array(
    'name'               => _x( 'Rewards', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'Rewards', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'Reward', 'xborder' ),
    'add_new_item'       => __( 'Add New Rewards', 'xborder' ),
    'edit_item'          => __( 'Edit Rewards', 'xborder' ),
    'new_item'           => __( 'New Rewards', 'xborder'),
    'all_items'          => __( 'All Rewards', 'xborder' ),
    'view_item'          => __( 'View Reward', 'xborder' ),
    'search_items'       => __( 'Search Rewards', 'xborder' ),
    'not_found'          => __( 'No Rewards found', 'xborder' ),
    'not_found_in_trash' => __( 'No Rewards found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Rewards'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Rewards',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title'),
    'has_archive'   => true,
    'menu_icon'   => 'dashicons-awards',
	'exclude_from_search' => false,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'reward' ),
  );
  register_post_type( 'reward', $args ); 


  $labels = array(
    'name'               => _x( 'White Papers', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'White Paper', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'White Paper', 'xborder' ),
    'add_new_item'       => __( 'Add New White Paper', 'xborder' ),
    'edit_item'          => __( 'Edit White Paper', 'xborder' ),
    'new_item'           => __( 'New White Paper', 'xborder'),
    'all_items'          => __( 'All White Papers', 'xborder' ),
    'view_item'          => __( 'View White Papers', 'xborder' ),
    'search_items'       => __( 'Search White Papers', 'xborder' ),
    'not_found'          => __( 'No White Papers found', 'xborder' ),
    'not_found_in_trash' => __( 'No White Papers found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'White Papers'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'White Papers',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title','editor','excerpt'),
    'has_archive'   => true,
    'menu_icon'   => 'dashicons-media-default',
	'exclude_from_search' => false,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'white-paper' ),
  );
  register_post_type( 'white-paper', $args ); 


  $labels = array(
    'name'               => _x( 'Articles', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'Article', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'Article', 'xborder' ),
    'add_new_item'       => __( 'Add New Article', 'xborder' ),
    'edit_item'          => __( 'Edit Article', 'xborder' ),
    'new_item'           => __( 'New Article', 'xborder'),
    'all_items'          => __( 'All Articles', 'xborder' ),
    'view_item'          => __( 'View Articles', 'xborder' ),
    'search_items'       => __( 'Search Articles', 'xborder' ),
    'not_found'          => __( 'No Articles found', 'xborder' ),
    'not_found_in_trash' => __( 'No Articles found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Articles'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Articles',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title','editor','excerpt'),
    'has_archive'   => true,
    'menu_icon'   => 'dashicons-text-page',
	'exclude_from_search' => false,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'article' ),
  );
  register_post_type( 'article', $args ); 


  $labels = array(
    'name'               => _x( 'Interview', 'post type general name', 'xborder' ),
    'singular_name'      => _x( 'Interview', 'post type singular name', 'xborder' ),
    'add_new'            => _x( 'Add New', 'Interview', 'xborder' ),
    'add_new_item'       => __( 'Add New Interview', 'xborder' ),
    'edit_item'          => __( 'Edit Interview', 'xborder' ),
    'new_item'           => __( 'New Interview', 'xborder'),
    'all_items'          => __( 'All Interviews', 'xborder' ),
    'view_item'          => __( 'View Interviews', 'xborder' ),
    'search_items'       => __( 'Search Interviews', 'xborder' ),
    'not_found'          => __( 'No Interviews found', 'xborder' ),
    'not_found_in_trash' => __( 'No Interviews found in the Trash', 'xborder' ), 
    'parent_item_colon'  => '',
    'menu_name'          => 'Interview'
  );
  $args = array(
    'labels'        => $labels,
    'description'   => 'Interview',
    'public'        => true,
    'menu_position' => 5,
    'supports'      => array('title','editor','excerpt'),
    'has_archive'   => true,
    'menu_icon'   => 'dashicons-testimonial',
	'exclude_from_search' => false,
	'capability_type' => 'post',
	'rewrite' => array( 'slug' => 'interview' ),
  );
  register_post_type( 'interview', $args ); 

}

add_action( 'init', 'xborder_custom_posts' );


?>
