<?php
	
	/*-----------------------------------------------------------------------------
	
		TNY MCE class

---------------------------------------------------------------------------*/
function wpb_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}

add_filter('mce_buttons_2', 'wpb_mce_buttons_2');


// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {  
    // Define the style_formats array
  
    $style_formats = array(  
        // Each array child is a format with it's own settings
        array(  
            'title' => 'Call to action',  
            'selector' => 'a',  
            'classes' => 'cta'             
        ),
         array(  
            'title' => 'Large paragraph',  
            'selector' => 'p',  
            'classes' => 'largefont'             
        ),
         array(  
            'title' => 'Fiona Show Logo',  
            'selector' => 'h4',  
            'classes' => 'fiona-show-logo'             
        )
    );  
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );  

    return $init_array;  

} 
// Attach callback to 'tiny_mce_before_init' 
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

///ADD editor styles for cta
function my_theme_add_editor_styles() {

  
  add_editor_style( 'css/custom-editor-style.css' );
 

}

add_action( 'init', 'my_theme_add_editor_styles' );



/*-----------------------------------------------------------------------------
	
		ACF wisywig
		
---------------------------------------------------------------------------*/

add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars )
{
	// Uncomment to view format of $toolbars
	
/*
	echo '< pre >';
		print_r($toolbars);
	echo '< /pre >';
	die;
	
	
	( [1] => Array ( [0] => formatselect [1] => bold [2] => italic [3] => bullist [4] => numlist [5] => blockquote [6] => alignleft [7] => aligncenter [8] => alignright [9] => link [10] => unlink [11] => wp_more [12] => spellchecker [13] => fullscreen [14] => wp_adv ) [2] => Array ( [0] => strikethrough [1] => hr [2] => forecolor [3] => pastetext [4] => removeformat [5] => charmap [6] => outdent [7] => indent [8] => undo [9] => redo [10] => wp_help ) [3] => Array ( ) [4] => Array ( ) ) [Basic] => Array ( [1] => Array ( [0] => bold [1] => italic [2] => underline [3] => blockquote [4] => strikethrough [5] => bullist [6] => numlist [7] => alignleft [8] => aligncenter [9] => alignright [10] => undo [11] => redo [12] => link [13] => unlink [14] => fullscreen ) ) ) 
*/
	

	// Add a new toolbar called "Very Simple"
	// - this toolbar has only 1 row of buttons
	$toolbars['Basic with styles' ][1] = array('formatselect', 'undo', 'redo','link', 'unlink', 'styleselect');
	$toolbars['Basic' ][1] = array('formatselect', 'bold' , 'italic' , 'underline', 'blockquote', 'bullist', 'numlist', 'undo', 'redo','link', 'unlink', 'styleselect' );
	$toolbars['Text and link' ][1] = array( 'undo', 'redo','link', 'unlink');
	

	// Edit the "Full" toolbar and remove 'code'
	// - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
/*
	if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false )
	{
	    unset( $toolbars['Full' ][2][$key] );
	}
*/

	// remove the 'Basic' toolbar completely
	//unset( $toolbars['Basic' ] );

	// return $toolbars - IMPORTANT!
	return $toolbars;
}



function remove_h1_from_editor( $settings ) {
    $settings['block_formats'] = 'Paragraph=p;Module heading=h4;Large Heading =h2;Medium Heading =h3;';
    return $settings;
}

add_filter( 'tiny_mce_before_init', 'remove_h1_from_editor' );
/*-----------------------------------------------------------------------------
	
		TNY MCE COLours

---------------------------------------------------------------------------*/



function acf_iris_palette(){

  ?>
	<script type="text/javascript">
	
		    acf.add_filter('color_picker_args', function( args, $field ){
		
			// 
			args.palettes = [	'#000000',
								'#655DC6',
								'#D83BB3',
								'#00ADBB',
								'#D4D802',
								'#FF5F00',
								'#00C2CC',
								'#A8B2B0'
							]
			
			
			// return
			return args;
					
		});


	</script>
  <?php
}
add_action( 'admin_print_scripts', 'acf_iris_palette', 90 );


/*-----------------------------------------------------------------------------
	
		ACF echo function
		
---------------------------------------------------------------------------*/
function e_acf($var, $tag = '' , $data = null){
	
	if($var){
	
		$html = '';
		if($tag != ''):
			 $html .=  '<' . $tag;
			 if($data) $html .= ' ' . $data;
			 $html .= '>';
		endif;
		
		$html .= $var;
		
		if($tag !='') $html .= '</' . $tag . '>';
	
		return $html;
	}
		
}



/*-----------------------------------------------------------------------------
	
		ACF image
		
---------------------------------------------------------------------------*/
function img_acf($var, $size = 'large', $type='img'){
	
	if($var) {
		$arr =  getImage($var, $size);
		return $arr[$type];
		}

}
/*-----------------------------------------------------------------------------
	
		ACF Link
		
---------------------------------------------------------------------------*/

function link_acf($linkArr, $class=null){
	
	
if($linkArr && is_array($linkArr)) {
	
	$link = '<a';
	$link .= ' href="' . $linkArr['url'] . '"'; 
	if($linkArr['target']) $link.= ' target="'. $linkArr['target']. '"';
	if($class) $link.= ' class="'. $class. '"';
	$link .='>'; 
	$link .= $linkArr['title'];
	$link .= '</a>';
	
	return $link;
	} else {
		return;
	}	
	
}
/* -----------------------------------------------------------------------------------------------------------
															    
												ACF COLUMNS
															    
------------------------------------------------------------------------------------------------------------- */
function add_acf_columns ( $columns ) {
   return array_merge ( $columns, array ( 
	 'from' => __ ( 'Event date' )
   ) );
 }
 add_filter ( 'manage_event_posts_columns', 'add_acf_columns' );

//
function my_page_columns($columns)
{
    return array_merge ( $columns, array ( 
		'tracking_code' => __ ( 'Tracking code' )
	  ) );
    return $columns;
}

function my_custom_columns($column)
{
    global $post;
    
    if ($column == 'tracking_code') {
        echo get_field( "internal_tracking_code", $post->ID );
    }
    else {
         echo '—';
    }
}

add_action("manage_article_posts_custom_column", "my_custom_columns");
add_filter("manage_article_posts_columns", "my_page_columns");



function event_custom_column ( $column, $post_id ) {
   switch ( $column ) {
     case 'from':
     
       echo get_post_meta ( $post_id, 'from', true );
       break;
    
   }
 }
 add_action ( 'manage_event_posts_custom_column', 'event_custom_column', 10, 2 );
 
 
 
/*-----------------------------------------------------------------------------
	
		Options page
		
---------------------------------------------------------------------------*/
if( function_exists('acf_add_options_page') ) {

	$parent = acf_add_options_page(array(
		'page_title' 	=> 'XBS settings',
		'menu_title' 	=> 'XBS settings',
		'position'	=> 2,
		'icon_url' => 'dashicons-admin-tools',
		'redirect' 		=> false));
	

	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Country Guide',
		'menu_title' 	=> 'Country Guide',
		'parent_slug' 	=> $parent['menu_slug'],
	));

	
}



 
