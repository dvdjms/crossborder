<?php 

function getVimeojson($vimeo_url){
    if( !$vimeo_url ) return false;
    $data = json_decode( file_get_contents( 'http://vimeo.com/api/oembed.json?url=' . $vimeo_url ) );
    if( !$data ) return false;
    return $data;
}


function getVimeothumbnail($vimeo_url){
    if( !$vimeo_url ) return false;
    $data = json_decode( file_get_contents( 'http://vimeo.com/api/oembed.json?url=' . $vimeo_url ) );
    if( !$data ) return false;
    return $data->thumbnail_url;
}


?>