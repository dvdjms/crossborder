<?php	function list_terms($id, $cat, $list=true){
		$terms = get_the_terms($id, $cat);
		$count = 0;                         
		if ( $terms && ! is_wp_error( $terms ) ) : 
		 
		    $links = '';
		 
		    foreach ( $terms as $term ) {
			 if($list)  $links .= '<li>';
			   
			   //$links .= '<a href="'.get_term_link($term->slug, $cat).'">';
			   if(!$list && $count > 0)  $links .= ', ';
		       $links .= $term->name;
		       
		       //$links .='</a>';
		      if($list)   $links .= '</li>';
		      $count++;
		    }
		                         
		    if($list)  return '<ul class="term-list">' . $links . '</ul>';
			return $links;
		  
		endif; 
		
	}
	

function list_primary_term($id, $cat, $list=true){
		
		$yoast = '_yoast_wpseo_primary_'.$cat;
		$primary = get_post_meta($id, $yoast);
		
		if($primary){
			$term = get_term( $primary[0] , $cat );
			if($list) $links = '<li>' . $term->name . '</li>';
			if(!$list) $links = $term->name;
			return $links;
		}
		
		$terms = get_the_terms($id, $cat);
		          
		                 
		if ( $terms && ! is_wp_error( $terms ) ) : 
		 
		    $links = '';
			$term = $terms[0];
		   // foreach ( $terms as $term ) {
			 if($list)  $links .= '<li>';
			   
			 
		       $links .= $term->name;
		       
		       //$links .='</a>';
		      if($list)   $links .= '</li>';
		
		 
		                         
		    if($list)  return '<ul class="term-list">' . $links . '</ul>';
			return $links;
		  
		endif; 
		
		
	}
	
/* --------------------------------------------------------------------------------------------------------------------------------
															    
													Pre get posts
															    
-------------------------------------------------------------------------------------------------------------------------------- */




function load_jobs($query) {

  if ($query->is_main_query() && is_post_type_archive('job') && !is_admin()) {
	
	if (session_status() == PHP_SESSION_NONE) session_start();
	

	if(isset($_SESSION['tax-query'])){
		 
		  $tax_query  = $_SESSION['tax-query'];
		  $query->set('posts_per_page',2);
		  $query->set( 'tax_query', $tax_query);
		  
	  } elseif (isset($_POST)){
		
		$taxonomies = [];
		$tax_query['relation'] = 'AND';
		
		foreach($_POST as $key => $value){
			if(!isset($taxonomies[$value])){
				$taxonomies[$value] = [$key];
			} else {
				$arr = $taxonomies[$value];
				$arr[] = $key;
				$taxonomies[$value] = $arr;
			}
			
		}
	
		foreach ($taxonomies as $key => $value){
			
			$arr =[];
			$arr['taxonomy'] = $key;
			$arr['field'] = 'slug';
			$termsArr=[];
			foreach($value as $term){
				
				$termsArr[] = $term;
			}
			$arr['terms'] = $termsArr;
			
			$tax_query[] = $arr;
		}
		$query->set('posts_per_page',12);
		$query->set( 'tax_query', $tax_query);
		
		print_r($tax_query);
		//$_SESSION['tax-query'] = $tax_query;
	
	  }  else {
		  return;
		
	}
	
  } else {
	if(!is_post_type_archive('job')) {
		
			if (session_status() == PHP_SESSION_NONE && !is_admin()) session_start();
			if (isset($_SESSION['tax-query'])) $_SESSION['tax-query'] = null;
			
	}
	
	 return;
  }

 }
//add_action('pre_get_posts', 'load_jobs');

function load_rewards($query) {
	// do not modify queries in the admin
	if( is_admin() ) {
		
		return $query;
		
	}
    if (($query->is_main_query() && is_post_type_archive('reward')) || ($query->is_main_query() && is_tax('reward-category'))  ) {
	// $query->set();
		print_r($_POST);
		
		$query->set('posts_per_page', 16);
		
		foreach($_POST as $key => $value){
			if($key == 'sort') {
				$sortby = $value;
				
				if($sortby == 'point_value'){
					$query->set('orderby','meta_value_num');
					$query->set('meta_key', $sortby);	
					$query->set('order', 'ASC'); 		
				}
			}
			
			if($key == 'points') {
				
				$points = $value;
				
				$array = array(
					array(
						'key' => 'point_value',
						'value' => $points,
						'type' => 'numeric', // specify it for numeric values
						'compare' => '<='
					)
				);
				
				if($points > 0) $query->set('meta_query', $array);
			
				
			}
					
		}
	
	}
	
	return $query;

 }
add_action('pre_get_posts', 'load_rewards');


function buildSelect($taxonmy_array, $all_name){	
$select_html = '';

	if(!empty($taxonmy_array)){
		$select_html .= '<fieldset class="multiple-select">';
		$select_html .= '<legend><span>'.$all_name.'</span><svg height="4" viewBox="0 0 10 4" width="10" xmlns="http://www.w3.org/2000/svg"><path d="m1151 19 4 4 4-4" fill="none" stroke="#ffffff" transform="translate(-1150 -19)"/></svg></legend>';
		$select_html .= '<div class="checks">';
		$select_html .= '<label class="label-checkbox"><input type="checkbox" checked ><span class="psuedo-checkbox">'.$all_name.'</span> </label>';
		
		foreach($taxonmy_array as $value){
			$select_html .= '<label class="label-checkbox"><input type="checkbox" value="' . esc_attr($value->taxonomy) . '"';
			$select_html .= ' name="' . esc_attr($value->slug) . '">';
			$select_html .= '<span class="psuedo-checkbox">' . esc_attr($value->name) . '</span> </label>'; 
		}
		
		$select_html .= '</div></fieldset>';
	}	
	
	return $select_html;
	
}




/* -----------------------------------------------------------------------------------------------------------
															    
													LEVER JOBS
															    
------------------------------------------------------------------------------------------------------------- */

 function getJson($url){
	  $trans  = get_transient($url);
	 if(!empty($trans)){
	  	return $trans;
	  } else {
	  
	  	$b = wp_remote_retrieve_body(wp_remote_get($url, array( 'timeout' => 20000)));
	  	
	  	$json =  json_decode($b);
	  	
	//    set_transient($url, $json, DAY_IN_SECONDS);
	  	
	  	return $json;
	  	
	  	}
	  
  }

  
  
  function cvf_convert_object_to_array($data) {

    if (is_object($data)) {
        $data = get_object_vars($data);
    }

    if (is_array($data)) {
        return array_map(__FUNCTION__, $data);
    }
    else {
        return $data;
    }
}

add_action('add_meta_boxes', function() {
    remove_meta_box('nf_admin_metaboxes_appendaform', ['page', 'post'], 'side');
}, 99);



function RemoveAddMediaButtons(){
 
        remove_action( 'media_buttons', 'media_buttons' );
    
}
add_action('admin_head', 'RemoveAddMediaButtons');


function cleanStringForClass($string) {
    if(!$string) return
    
    $string = preg_replace('/[^A-Za-z\-]/', '', $string); // Removes special chars.
    $string = trim(str_replace(' ', '-', $string)); // Replaces all spaces with hyphens.
	$string = strtolower($string);//make all lowecase;
	$string = 'f_'.$string;
  //  $html = 'id="' . $string .'"';

    return $string;

}




 
	?>