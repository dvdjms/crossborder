<?php

/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */
function add_custom_taxonomies() {
  // Add new "Locations" taxonomy to Informations
  
  
  
  
  // Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name' 				=> _x( 'Department categories', 'taxonomy general name', 'xborder' ),
	      'singular_name' 		=> _x( 'Department category', 'taxonomy singular name', 'xborder' ),
	      'search_items' 		=> __( 'Search Department categories', 'xborder' ),
	      'all_items' 			=> __( 'All Department categories', 'xborder' ),
	      'parent_item' 		=> __( 'Parent Department category', 'xborder' ),
	      'parent_item_colon' 	=> __( 'Parent Department category:', 'xborder' ),
	      'edit_item' 			=> __( 'Edit Department category', 'xborder' ),
	      'update_item' 		=> __( 'Update Department category', 'xborder' ),
	      'add_new_item' 		=> __( 'Add New Department category', 'xborder' ),
	      'new_item_name' 		=> __( 'New Department category Name', 'xborder' ),
	      'menu_name' 			=> __( 'Department categories', 'xborder' ),

	);
	
	$rewrite = array(
	   'slug' => 'departments', // This controls the base slug that will display before each term
      'with_front' => true, // Don't display the category base before "/locations/"
      'hierarchical' =>  false // This will allow URL's like "/locations/boston/cambridge/"
		
	);
	
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite,
	);

	register_taxonomy( 'department-category', array( 'job' ), $args );

    // Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name' 				=> _x( 'Location categories', 'taxonomy general name', 'xborder' ),
	      'singular_name' 		=> _x( 'Location category', 'taxonomy singular name', 'xborder' ),
	      'search_items' 		=> __( 'Search Location categories', 'xborder' ),
	      'all_items' 			=> __( 'All Location categories', 'xborder' ),
	      'parent_item' 		=> __( 'Parent Location category', 'xborder' ),
	      'parent_item_colon' 	=> __( 'Parent Location category:', 'xborder' ),
	      'edit_item' 			=> __( 'Edit Location category', 'xborder' ),
	      'update_item' 		=> __( 'Update Location category', 'xborder' ),
	      'add_new_item' 		=> __( 'Add New Location category', 'xborder' ),
	      'new_item_name' 		=> __( 'New Location category Name', 'xborder' ),
	      'menu_name' 			=> __( 'Location categories', 'xborder' ),

	);
	
	$rewrite = array(
	   'slug' => 'location-category', // This controls the base slug that will display before each term
      'with_front' => true, // Don't display the category base before "/locations/"
      'hierarchical' =>  false // This will allow URL's like "/locations/boston/cambridge/"
		
	);
	
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite,
	);

	register_taxonomy( 'location-category', array( 'job' ), $args );
	
	    // Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name' 				=> _x( 'Reward categories', 'taxonomy general name', 'xborder' ),
	      'singular_name' 		=> _x( 'Reward category', 'taxonomy singular name', 'xborder' ),
	      'search_items' 		=> __( 'Search Reward categories', 'xborder' ),
	      'all_items' 			=> __( 'All Reward categories', 'xborder' ),
	      'parent_item' 		=> __( 'Parent Reward category', 'xborder' ),
	      'parent_item_colon' 	=> __( 'Parent Reward category:', 'xborder' ),
	      'edit_item' 			=> __( 'Edit Reward category', 'xborder' ),
	      'update_item' 		=> __( 'Update Reward category', 'xborder' ),
	      'add_new_item' 		=> __( 'Add New Reward category', 'xborder' ),
	      'new_item_name' 		=> __( 'New Reward category Name', 'xborder' ),
	      'menu_name' 			=> __( 'Reward categories', 'xborder' ),

	);
	
	$rewrite = array(
	   'slug' => 'reward-category', // This controls the base slug that will display before each term
      'with_front' => true, // Don't display the category base before "/locations/"
      'hierarchical' =>  false // This will allow URL's like "/locations/boston/cambridge/"
		
	);
	
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite,
		
	);

	register_taxonomy( 'reward-category', array( 'reward' ), $args );


	// WHITE PAPER
	$labels = array(
		'name' 				=> _x( 'White Paper categories', 'taxonomy general name', 'xborder' ),
			'singular_name' 		=> _x( 'White Paper category', 'taxonomy singular name', 'xborder' ),
			'search_items' 		=> __( 'Search White Paper categories', 'xborder' ),
			'all_items' 			=> __( 'All White Paper categories', 'xborder' ),
			'parent_item' 		=> __( 'Parent White Paper category', 'xborder' ),
			'parent_item_colon' 	=> __( 'Parent White Paper category:', 'xborder' ),
			'edit_item' 			=> __( 'Edit White Paper category', 'xborder' ),
			'update_item' 		=> __( 'Update White Paper category', 'xborder' ),
			'add_new_item' 		=> __( 'Add New White Paper category', 'xborder' ),
			'new_item_name' 		=> __( 'New White Paper category Name', 'xborder' ),
			'menu_name' 			=> __( 'White Paper categories', 'xborder' ),

	);
	
	$rewrite = array(
		'slug' => 'white-paper-category', // This controls the base slug that will display before each term
		'with_front' => true, // Don't display the category base before "/locations/"
		'hierarchical' =>  false // This will allow URL's like "/locations/boston/cambridge/"
		
	);
	
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite,
		
	);
		
	register_taxonomy( 'white-paper-category', array( 'white-paper' ), $args );

	// ARTICLES PAPER
	$labels = array(
			'name' 				=> _x( 'Article categories', 'taxonomy general name', 'xborder' ),
			'singular_name' 		=> _x( 'Article category', 'taxonomy singular name', 'xborder' ),
			'search_items' 		=> __( 'Search Article categories', 'xborder' ),
			'all_items' 			=> __( 'All Article categories', 'xborder' ),
			'parent_item' 		=> __( 'Parent Article category', 'xborder' ),
			'parent_item_colon' 	=> __( 'Parent Article category:', 'xborder' ),
			'edit_item' 			=> __( 'Edit Article category', 'xborder' ),
			'update_item' 		=> __( 'Update Article category', 'xborder' ),
			'add_new_item' 		=> __( 'Add New Article category', 'xborder' ),
			'new_item_name' 		=> __( 'New Article category Name', 'xborder' ),
			'menu_name' 			=> __( 'Article categories', 'xborder' ),

	);
	
	$rewrite = array(
		'slug' => 'article-category', // This controls the base slug that will display before each term
		'with_front' => true, // Don't display the category base before "/locations/"
		'hierarchical' =>  false // This will allow URL's like "/locations/boston/cambridge/"
		
	);
	
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite,
		
	);
		
	register_taxonomy( 'article-category', array( 'article' ), $args );
	
	
	// INTERVIEW
		$labels = array(
			'name' 				=> _x( 'Interview categories', 'taxonomy general name', 'xborder' ),
			'singular_name' 		=> _x( 'Interview category', 'taxonomy singular name', 'xborder' ),
			'search_items' 		=> __( 'Search Interview categories', 'xborder' ),
			'all_items' 			=> __( 'All Interview categories', 'xborder' ),
			'parent_item' 		=> __( 'Parent Interview category', 'xborder' ),
			'parent_item_colon' 	=> __( 'Parent Interview category:', 'xborder' ),
			'edit_item' 			=> __( 'Edit Interview category', 'xborder' ),
			'update_item' 		=> __( 'Update Interview category', 'xborder' ),
			'add_new_item' 		=> __( 'Add New Interview category', 'xborder' ),
			'new_item_name' 		=> __( 'New Interview category Name', 'xborder' ),
			'menu_name' 			=> __( 'Interview categories', 'xborder' ),

	);
	
	$rewrite = array(
		'slug' => 'interview-category', // This controls the base slug that will display before each term
		'with_front' => true, // Don't display the category base before "/locations/"
		'hierarchical' =>  false // This will allow URL's like "/locations/boston/cambridge/"
		
	);
	
	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => $rewrite,
		
	);
		
	register_taxonomy( 'interview-category', array( 'interview' ), $args );
       

}
add_action( 'init', 'add_custom_taxonomies', 0 );

?>