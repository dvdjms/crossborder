<?php
/**
 * The template for displaying all single rewards.

 */

wp_redirect( get_permalink( get_page_by_path( 'Rewards' ) ));

get_header(); ?>

<!--single reward -->
			<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part( 'template-parts/content', 'single' ); ?>

		<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
