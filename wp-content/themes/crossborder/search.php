<?php
/**
 * The template for displaying search results pages.
 *

 */

get_header(); ?>


<?php include(get_template_directory().'/template-parts/content-header.php'); ?>				

<?php if ( have_posts() && get_search_query()!= ' ') : ?>


	<section class="results-search">
	<div class="content" id="results">

	<h2><?php printf( esc_html__( 'You searched for %s', 'xborder' ), get_search_query() ); ?></h2>

	
	<?php $n = $wp_query->found_posts; ?>
	<p><?php echo $n ;?> results found</p>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); 
				
				get_template_part( 'template-parts/content', 'search' );
//
			 endwhile;			
				//include(get_template_directory().'/template-parts/pagination.php'); ?>	
				 <div class="paginate">
				<?php  echo paginate_links( array('prev_text'          => __('« '), 'next_text'          => __('»')) ); ?>
				 </div>


	</div>
	</section>		
<?php else: ?>
		<section class="results-search">
		<div class="content" id="results">
		<?php  get_template_part( 'template-parts/content', 'none' );?>
		
		</div>
		</section>
<?php endif; ?>

<?php get_footer(); ?>
