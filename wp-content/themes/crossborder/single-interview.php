<?php
/**
 * The template for displaying all single interview posts.

 */

get_header(); ?>

<!--single interview -->
			<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part( 'template-parts/content', 'single-interview' ); ?>

		<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
