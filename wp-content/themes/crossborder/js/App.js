import * as Fades from './imports/Fades';
import * as Lazy from './imports/Lazy';
import * as Videos from './imports/Video';
import * as Carousel from './imports/Carousel';
//
import * as People from './imports/Modal';
import * as Locations from './imports/Locations';

import * as Scroller from './imports/Scroller';
import * as Select from './imports/Leverselect'; //was Multiselect
import * as Rewards from './imports/Rewards'; 
import * as Categories from './imports/Categories'; 
//import * as Select from './vendor/slimselect';
import * as PageScroll from './vendor/smooth-scroll';

const App = (  ) => {
	let i;
 	let body = 				document.querySelector('body');
  	let menu_button = 		document.querySelector('.menu-button');
	let header = 			document.querySelector('.header--site');
	let updatecount = 		0;
	let languageLink = 		document.querySelectorAll('[hreflang]');
	let scrollPos = 		0;
	let formLoaded = 		false;
	
	let navWithsubs =		document.querySelectorAll('.menu-item-has-children > a');
	let modalLink =			false;
	let dropdownLink =		document.querySelectorAll('a[href="#getstarted"]');	
	let closeLink =			document.querySelector('.hide-modal');	
	let mobile = 			false;
	let mobileCarousel = 	null;
	let element;
   //touch
   
   document.addEventListener("touchstart", function() {},false);

	//
	var isTouchDevice = 'ontouchstart' in document.documentElement;
	if( isTouchDevice ) {
		body.classList.add('touch');
	} else {
		body.classList.add('no-touch');
	}

   
    //menu click
    if(menu_button) menu_button.onclick = () =>  {
	     body.classList.toggle('menu-visible');
	      if(body.classList.contains('sub-nav-showing')) body.classList.remove('sub-nav-showing');

	     
	     let subs = document.querySelectorAll('.show-sub-nav');
		     for ( i = 0; i < subs.length; i++) {
			    subs[i].classList.remove('show-sub-nav');
			    }
	     
	     }
	//
	if(navWithsubs.length > 0) {
			
			
			for ( i = 0; i < navWithsubs.length; i++) {
			
			let navItem = navWithsubs[i];
			
				navItem.onclick = (e) => {
				
				e.preventDefault();
				let parentNode = navItem.parentNode
				let activeMenus = document.querySelectorAll('.menu-item-has-children.active');
				//
					if(parentNode.classList.contains('active')){
						//if active, then close the menu, remove height from the header and the active clasee
						let sub =  parentNode.querySelector('.sub-menu');
						parentNode.classList.remove('active');
						header.removeAttribute('style');
						
						sub.removeAttribute('style');
					} else {
						//not active, close other active menus  
						for ( i = 0; i < activeMenus.length; i++) {
							activeMenus[i].classList.remove('active');
						}
						///add active to selected navItem
						parentNode.classList.add('active');
						///get heights	   
						let sub =  parentNode.querySelector('.sub-menu');
						let height = sub.scrollHeight;
						//
						header.style.height = height + 90 + 'px';
						sub.style.height = height + 20 + 'px';
					}	    
				}
			
			}
		
		}
	//FORM	
	if(document.querySelector('.show-bio') != null) {
		let bio_link = document.querySelector('.show-bio');
		bio_link.onclick = (e) =>  body.classList.add('show-bio');
	} 
	if(document.querySelector('.close-bio')!= null) {
		let bio_close= document.querySelectorAll('.close-bio');
		
		[...bio_close].map((item, index)=>{
			
			item.onclick = (e) =>  body.classList.remove('show-bio');
		  
		});
	} 


	if(modalLink!=null)  	for ( i = 0; i < modalLink.length; i++) modalLink[i].onclick = (e) =>  showModal(e);


	if(dropdownLink!=null) 	{
		for ( i = 0; i < dropdownLink.length; i++) {

			dropdownLink[i].onclick = (e) =>  {
				console.log('dd')
				e.preventDefault();
				e.currentTarget.classList.toggle('show-dropdown');
				element = e.currentTarget;
				document.addEventListener('click', outsideClickListener);
			};
		}

		const outsideClickListener = (event) => {
			console.log('dd click' )
		
			if (!element.contains(event.target)) removeClickListener(element);
		}
		
		const removeClickListener = (element) => {
			console.log('dd rmeove')
			if(element != undefined && element.classList.contains('show-dropdown')) element.classList.remove('show-dropdown');
			document.removeEventListener('click', outsideClickListener);
		}
		
	}

	///
	if(closeLink!=null) 	closeLink.onclick = (e) =>  hideModal(e);
	//		
	const showModal = (e) => {
		e.preventDefault();
		if(! body.classList.contains('show-modal')) body.classList.add('show-modal');
	}
	//
	const hideModal = (e) => {
		e.preventDefault();
		if(body.classList.contains('show-modal')) body.classList.remove('show-modal');
	}
	
    ///Fades
    Fades.SetUpFades('fades', 200);
    //Lazy
	Lazy.SetUpLazy();
    ///Video
	if(document.getElementsByClassName('video-holder') != null )  Videos.SetUpVideos();
     ////
	if(document.getElementsByClassName('carousel').length >  0)  Carousel.SetUpCarousel('carousel');
	//
	if(document.getElementsByClassName('people').length >  0) People.SetUpPeopleModal();
	//
	if(document.getElementsByClassName('locations').length >  0) Locations.SetUpLocations();
	//
	if(document.getElementsByClassName('multiple-select').length >  0) Select.SetUpSelects('multiple-select');
	//
	if(document.getElementsByClassName('section--rewards').length >  0) Rewards.SetUpRewards('section--rewards', Lazy);
	//
	Categories.SetUpCategories(Lazy);
		
	new SmoothScroll('a[href*="#"]', {easing: 'easeOutQuad', speed:300});
	
	if(document.getElementById('country-scroller') != null) {
		Scroller.SetUpScroller('country-scroller');	
		window.addEventListener('customScroll', e => Lazy.isImageVisible());
	}
		
		
	////
	body.classList.add('loaded');
	///
	window.addEventListener('scroll', function(){
	
		if(body.classList.contains('loaded')) body.classList.remove('loaded');
	});	 
	
	///rRESIZE
	window.addEventListener('resize', function(){
		fResponsive();
	});  
	

	 
	const fResponsive = () =>{
		let w = viewportSize('Width');
		if(w < 900){
			//console.log('mob');
			if(mobile == false) {
				if(document.getElementsByClassName('mobile-carousel').length >=  1){
					 mobileCarousel = Carousel.SetUpMobileCarousel('mobile-carousel');
					 body.classList.add('mobile-carousel-show');
				}
			}
			mobile = true;
		} else{
			if(mobile == true) {
				if(mobileCarousel!=null) mobileCarousel.destroy(true);
				if(body.classList.contains('mobile-carousel-show'))body.classList.remove('mobile-carousel-show');
			}
			mobile = false;
		}
		if(document.getElementById('load-holder') != null){
			if(document.getElementById('load-holder').hasAttribute('style')) document.getElementById('load-holder').removeAttribute('style');
		}
	}
	
	const viewportSize = ($d) => {
    

    
    var e = window, a = 'inner';
    if (!('innerWidth' in window )) {
        a = 'client';
        e = document.documentElement || document.body;
    }
		return e[ a+$d ];
   	}
    ///Ticker
    const fupdate = () => {
		

		updatecount++;
		if(updatecount > 10) {
			//console.log('update ');
			Fades.CheckFades();
			updatecount = 0;
		}
		if(formLoaded ==false){
			let ninja = document.querySelector('.nf-form-cont input');
			
			if(ninja != null) {
				formLoaded = true;
				
				fNinjaForms();
			}
		}
		ticker = requestAnimationFrame(fupdate);


	}

    let ticker = requestAnimationFrame(fupdate);

	///stacks{
	let stacks = document.querySelectorAll('.stack');
	

	
      for (let i = 0; i < stacks.length; i++) {
	       let stack = stacks[i];
		   let images = stack.querySelectorAll('img');
		   let loaded = 0;
		   for (let i = 0; i < images.length; i++) {
			
			   images[i].onload = function(){
				   loaded++;
				   if(loaded == images.length) {
					   stack.classList.add('stack-loaded');
					   stack.classList.add('show-stack');
					   }
			   }
			   
			 }
	   }
	
	
	
	const fNinjaForms = () =>{

		///Form label inputs
		let  form = document.getElementById('form-holder');
		let ninja = form.querySelector('.nf-form-cont');
		
	
		if(form != null) {
			var  inputs = ninja.querySelectorAll('input');
			var selects = form.getElementsByTagName('select');
			
			for ( i = 0; i < inputs.length; i++) {
				
				inputs[i].onfocus = (e) => {
					//	console.log('ninja focus' + inputs[i]);
					let input = e.currentTarget;
					let id = input.getAttribute('id');
					let selector = 'label[for="'+id+'"]';
					
					let label = document.querySelector(selector);
					if(!label.classList.contains('focused')) label.classList.add('focused');
				}
				inputs[i].onblur = (e) => {
					let input = e.currentTarget;
					let id = input.getAttribute('id');
					let selector = 'label[for="'+id+'"]';
					let label = document.querySelector(selector);
					if(label.classList.contains('focused') && !input.value) label.classList.remove('focused');
				}
			}
			
			for ( i = 0; i < selects.length; i++) {
				
				
				selects[i].onfocus = (e) => {
					let input = e.currentTarget;
					let id = input.getAttribute('id');
					let selector = 'label[for="'+id+'"]';
					
					let label = document.querySelector(selector);
					if(!label.classList.contains('focused')) label.classList.add('focused');
				}
				selects[i].onblur = (e) => {
					let input = e.currentTarget;
					let id = input.getAttribute('id');
					let selector = 'label[for="'+id+'"]';
					let label = document.querySelector(selector);
					if(label.classList.contains('focused') && !input.value) label.classList.remove('focused');
				}
			}
	
		}
	}
	///
	fResponsive();

    return;
};


window.onload = App;

