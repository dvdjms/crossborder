window.leverJobsOptions = {accountName: 'xbs', includeCss: false};


window.loadLeverJobs = function (options) {

  //Checking for potential Lever source or origin parameters
  var pageUrl = window.location.href;
  var leverParameter = '';
  var trackingPrefix = '?lever-';
  options.accountName = options.accountName.toLowerCase();
  // Define the container where we will put the content (or put in the body)
  var jobsContainer = document.getElementById("lever-jobs-container") || document.body;
  var jobsList = document.querySelector('.listing--jobs');

  if( pageUrl.indexOf(trackingPrefix) >= 0){
    // Found Lever parameter
    var pageUrlSplit = pageUrl.split(trackingPrefix);
    leverParameter = '?lever-'+pageUrlSplit[1];
  }

  var htmlTagsToReplace = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;'
  };

  function replaceTag(tag) {
      return htmlTagsToReplace[tag] || tag;
  }

  //For displaying titles that contain brackets in the 'append' function
  function sanitizeForHTML(str) {
      if (typeof str == 'undefined' ) {
        return '';
      }
      return str.replace(/[&<>]/g, replaceTag);
  }

  //Functions for checking if the variable is unspecified and removing script tags + null check
  //For making class names from department and team names
  function sanitizeAttribute(string) {
    if (string == '' || typeof string == 'undefined' ) {
      return 'uncategorized';
    }
    string = sanitizeForHTML(string);
    return string.replace(/\s+/ig, "");
  }

  // Adding the account name to the API URL
  var url = 'https://api.lever.co/v0/postings/' + options.accountName + '?group=team&mode=json';

  //Create an object ordered by department and team
  function createJobs(responseData) {
    if (!responseData) return;

    //Older versions of IE might not interpret the data as a JSON object
    if(typeof responseData === "string") {
      responseData = JSON.parse(responseData);
    }

    var content = "";
    var groupedPostings = [];

    for(var i = 0; i < responseData.length; i++) {
      if (!responseData[i]) continue;
      if (!responseData[i].postings) continue;
      if (!(responseData[i].postings.length > 0)) continue;

      var title = sanitizeForHTML(responseData[i].title || 'Uncategorized');
      var titlesanitizeAttribute = sanitizeAttribute(title);


      for (j = 0; j < responseData[i].postings.length; j ++) {
        var posting = responseData[i].postings[j];
        var team = (posting.categories.team || 'Uncategorized' );
        var teamsanitizeAttribute = sanitizeAttribute(team);
        var department = (posting.categories.department || 'Uncategorized' );
        var departmentsanitizeAttribute = sanitizeAttribute(department);
        var link = posting.hostedUrl+leverParameter;
		
        function findDepartment(element) {
          return element.department == departmentsanitizeAttribute;
        }

        if (groupedPostings.findIndex(findDepartment) === -1) {

          newDepartmentToAdd = {department : departmentsanitizeAttribute, departmentTitle: department, teams : [ {team: teamsanitizeAttribute, teamTitle: team, postings : []} ] };
          groupedPostings.push(newDepartmentToAdd);
        }
        else {

          departmentIndex = groupedPostings.findIndex(findDepartment);
          newTeamToAdd = {team: teamsanitizeAttribute, teamTitle: team, postings : []};

          if (groupedPostings[departmentIndex].teams.map(function(o) { return o.team; }).indexOf(teamsanitizeAttribute) === -1) {
            groupedPostings[departmentIndex].teams.push(newTeamToAdd);
          }
        }
        function findTeam(element) {
          return element.team == teamsanitizeAttribute;
        }
        departmentIndex = groupedPostings.findIndex(findDepartment);
        teamIndex = groupedPostings[departmentIndex].teams.findIndex(findTeam);
        groupedPostings[departmentIndex].teams[teamIndex].postings.push(posting);

      }

    }

    // Sort by department
    groupedPostings.sort(function(a, b) {
      var departmentA=a.department.toLowerCase(), departmentB=b.department.toLowerCase()
      if (departmentA < departmentB)
          return -1
      if (departmentA > departmentB)
          return 1
      return 0
    });

    for(var i = 0; i < groupedPostings.length; i++) {

      // If there are no departments used, there is only one "unspecified" department, and we don't have to render that.
      if (groupedPostings.length >= 2) {
        var haveDepartments = true;
      };
	  /////HTML
	  if (haveDepartments) {
        content += '<section class="lever-department" data-department="' + groupedPostings[i].departmentTitle + '"><h3 class="lever-department-title">' + sanitizeForHTML(groupedPostings[i].departmentTitle) + '</h3>';
      };
      for (j = 0; j < groupedPostings[i].teams.length; j ++) {



        for (var k = 0; k < groupedPostings[i].teams[j].postings.length; k ++) {
	        var dept = groupedPostings[i].departmentTitle;
	        var id = groupedPostings[i].teams[j].postings[k].id;
	        var loc =  groupedPostings[i].teams[j].postings[k].categories.location;
	        var team = groupedPostings[i].teams[j].postings[k].categories.team;
	        var type = groupedPostings[i].teams[j].postings[k].categories.commitment;
	      //  var url =  groupedPostings[i].teams[j].postings[k].hostedUrl;
	          var url =  '/cross-border/job-listing?id=' + id;
	        var title = groupedPostings[i].teams[j].postings[k].text;
	        var input = groupedPostings[i].teams[j].postings[k].descriptionPlain;    //declaring input
	          
	          var input = input.replace("Role Overview", "");
   			//var desc = jQuery.trim(input).substring(0, 120).replace('\n', ' ') + "...";
	       	 input = jQuery.trim(input);
         	var desc  = subStringToWord(input,120);
        
         	
         	 content += '<article class="item driver entry--job lever-job" data-department="' + dept +'" data-team="' + team + '" data-location="' + loc + '"data-work-type="' + type + '">';
		 	 content +=  '<a class="mobile-cta" href="' + url + '"></a><span class="mobile-touch"></span>';
		 	 content +=  '<div class="title"><h3>' + sanitizeForHTML(title) + '</h3>';
		 	 content +=  '<div class="categories">' + (sanitizeForHTML(loc) || '') +'</div></div>';
		 	 content += '<div class="copy"><p>'+sanitizeForHTML(desc)+ '&hellip;</p></div>';
		 	 content += '</article>';
        }

       

      }
      if (haveDepartments) {
        content += '</section>';
      };

      /////
    }

    //content += '</ul>';
    jobsContainer.innerHTML = content;
    window.dispatchEvent(new Event('leverJobsRendered'));
    
    

  }

  var request = new XMLHttpRequest();
  request.open('GET', url, true);
  request.responseType = "json";

  request.onload = function() {
	

    if (request.status == 200) {
	 
      createJobs(request.response);
      setUpMobileJobs();
 
   
    } else {
      console.log("Error fetching jobs.");
      jobsContainer.innerHTML = "<p class='lever-error'>Error fetching jobs.</p>";
    }
  };

  request.onerror = function() {
    console.log("Error fetching jobs.");
    jobsContainer.innerHTML = "<p class='lever-error'>Error fetching jobs.</p>";
  };

  request.send();

  
  function strpos (haystack, needle, offset) {
		var i = (haystack+'').indexOf(needle, (offset || 0));
			return i === -1 ? false : i;
	}
  
  function subStringToWord(string, num){
	  
	  if(string.length > num){
		var pos = strpos(string, ' ', num - 10);
		if (pos === false) {
			 pos = strpos(string, ' ', num - 20);
		}
		newString = string.substring(0, pos);
		//string = substr(string,0,pos ).$more; 
	} else {
		newString = string;
	}
	  
	  return newString;
	  
	 }
  
};

/*
function subString($s, $n, $more='&hellip;'){
	
	$s = strip_tags ($s);
	if(strlen($s) > $n){
		$pos = strpos($s, ' ', $n - 10);
		if ($pos === false) {
			 $pos = strpos($s, ' ', $n - 20);
		}
		
		$string = substr($s,0,$pos ).$more; 
	} else {
		$string = $s;
	}
	$string = '<p>'.$string.'</p>';
	
	return $string;
	
}
*/





window.loadLeverJobs(window.leverJobsOptions);

// IE polyfill for findIndex - found at https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex

if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return k.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return k;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return -1.
      return -1;
    },
    configurable: true,
    writable: true
  });
}

// IE Polyfill for New Event

(function () {

  if ( typeof window.CustomEvent === "function" ) return false;

  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
   }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;
})();

const setUpMobileJobs = () =>{
	
		if(document.getElementsByClassName('mobile-touch').length >  0){
			
			setUpMobileJobHeights();
			
			let jobs = document.getElementsByClassName('mobile-touch');
			
			for (let i = 0; i < jobs.length; i++) {
				let job = jobs[i].parentNode;
				job.onclick = (event) => {
					let tgt = event.target;
					if(!tgt.classList.contains('mobile-cta')){
						job.classList.toggle('job-open');
					}
					
					
				}
			}
		
		}
}

const setUpMobileJobHeights = () => {
	
		let jobs = document.getElementsByClassName('mobile-touch');
		for (let i = 0; i < jobs.length; i++) {
			
			let job = jobs[i].parentNode;
			let copy = job.querySelector('.copy');
			let h = copy.scrollHeight + 'px';
			job.setAttribute('style', '--job-height:' + h);
			
		}
	}

window.addEventListener("resize", requestFrame)

function requestFrame () {

      window.requestAnimationFrame(() => setUpMobileJobHeights())
   
    
  }

window.addEventListener('leverJobsRendered', function() {
    
       jQuery(".lever-job").clone().appendTo("#new-list .listing--jobs");

       var options = {
         valueNames: [
           'lever-job-title',
           { data: ['location'] },
           { data: ['department'] },
          // { data: ['team'] },
          // { data: ['work-type'] },
           { data: ['description']}
         ]
       };

       var jobList = new List('new-list', options);

       
        var locations = [];
        var departments = [];
//
        for (var i = 0; i < jobList.items.length; i++) {
          var item = jobList.items[i]._values;
          var location = item.location;
          if(jQuery.inArray(location, locations) == -1) {
            locations.push(location);
          }
          var department = item.department;
          if(jQuery.inArray(department, departments) == -1) {
            departments.push(department);
          }
/*
          var team = item.team;
          if(jQuery.inArray(team, teams) == -1) {
            teams.push(team);
          }
          var workType = item["work-type"];
          if(jQuery.inArray(workType, workTypes) == -1) {
            workTypes.push(workType);
          }
*/
        }

      locations.sort();
      departments.sort();

     ///
     	
	  for (var j = 0; j < locations.length; j++ ) {
		  var html = '<label class="label-checkbox"><input type="checkbox" value="'+ locations[j] + '" name ="location"><span class="psuedo-checkbox">' + locations[j] + '</span></label>';
          jQuery( "#lever-jobs-filter .lever-jobs-filter-locations .checks").append(html);
      }
      for (var j = 0; j < departments.length; j++ ) {
	      var html = '<label class="label-checkbox"><input type="checkbox" value="'+ departments[j] + '" name ="department"><span class="psuedo-checkbox">' + departments[j] + '</span></label>';
          jQuery( "#lever-jobs-filter .lever-jobs-filter-departments .checks").append(html);
      }


      function showFilterResults() {
        jQuery('#new-list .list').show();
        jQuery('#lever-jobs-container').hide();
         
         var cont = jQuery('.listing--jobs');
     (jobList.visibleItems.length < 3 ) ? cont.removeClass('show-arm') : cont.addClass('show-arm') ;
		 if(cont.hasClass('fade-in'))  cont.removeClass('fade-in').css('opacity',0);
		// if(cont)
         setTimeout(function(){
		      jQuery('.listing--jobs').addClass('fade-in');
	      }, 300)
      }
      function hideFilterResults() {
        jQuery('#new-list .list').hide();
        jQuery('#lever-jobs-container').show();
      }
      

     showFilterResults();
      

    jQuery('#lever-jobs-filter .label-checkbox').click(function(){
      
    
	
     var selectedFilters = {
  
        department: jQuery('#lever-jobs-filter .lever-jobs-filter-departments legend').text(),
         location: jQuery('#lever-jobs-filter .lever-jobs-filter-locations legend').text()
        
        
  
      }
	 
      //Filter the list
      jobList.filter(function(item) {
        var itemValue = item.values();
        // Check the itemValue against all filter properties (location, team, work-type).
        for (var filterProperty in selectedFilters) {
          var selectedFilterValue = selectedFilters[filterProperty];
          // For a <select> that has no option selected, JQuery's val() will return null.
          // We only want to compare properties where the user has selected a filter option,
          // which is when selectedFilterValue is not null.
          if(selectedFilterValue=='All departments' || selectedFilterValue=='All locations') selectedFilterValue = null;
          if (selectedFilterValue !== null) {
            if (itemValue[filterProperty] !== selectedFilterValue) {
              // Found mismatch with a selected filter, can immediately exclude this item.
              return false;
            }
          }
        }
        // This item passes all selected filters, include this item.
        
        
        return true;
      });

      //Show the 'no results' message if there are no matching results
      if (jobList.visibleItems.length >= 1) {
        jQuery('#lever-no-results').hide();
      }
      else {
        jQuery('#lever-no-results').show().addClass('fade-in');
        
      }

      //console.log("filtered?", jobList.filtered);


    //  jQuery('#lever-clear-filters').show();

      //Show the list with filtered results
      showFilterResults();

    });




  })
