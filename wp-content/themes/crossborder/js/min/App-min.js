'use strict';

var fadeArr = [];
var threshold = 100;

var SetUpFades = function SetUpFades($class) {
  //	let threshold = $threshold;
  var fadeItems = document.getElementsByClassName($class);

  for (var i = 0; i < fadeItems.length; i++) {
    fadeItems[i].classList.add('el__hidden');
    fadeArr.push(fadeItems[i]);
  }
};

var CheckFades = function CheckFades() {
  fadeArr.forEach(function (object, index, arr) {
    //var onScreen = Visible.isVisible(fadeArr);
    var rect = object.getBoundingClientRect();
    var limit = window.innerHeight - threshold; //console.log(threshold )

    if (rect.top < limit) {
      if (object.classList.contains('el__hidden')) {
        object.classList.remove('el__hidden');
        object.classList.add('el__show');
      }

      arr.splice(index, 1);
    }
  });
};

var lazyArray = []; // feature detection
// https://github.com/Modernizr/Modernizr/blob/master/feature-detects/img/srcset.js

var srcset = document.body.classList.contains('srcset') || 'srcset' in document.createElement('img'); // device pixel ratio
// not supported in IE10 - https://msdn.microsoft.com/en-us/library/dn265030(v=vs.85).aspx

var dpr = window.devicePixelRatio || window.screen.deviceXDPI / window.screen.logicalXDPI;
var settings = {
  normal: 'data-normal',
  retina: 'data-retina',
  srcset: 'data-srcset',
  threshold: 0
};

var SetUpLazy = function SetUpLazy() {
  var $threshold = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  lazyArray.length = 0;
  var imageItems = document.querySelectorAll('[data-normal]');
  settings.threshold = $threshold;

  for (var i = 0; i < imageItems.length; i++) {
    lazyArray.push(imageItems[i]);
  }

  SetUpEvents();
  requestFrame();
};

var SetUpEvents = function SetUpEvents() {
  ['resize', 'scroll', 'load'].forEach(function (evt) {
    return window.addEventListener(evt, requestFrame, false);
  });
};

function requestFrame() {
  window.requestAnimationFrame(function () {
    return isImageVisible();
  });
}

var isVisible = function isVisible($element) {
  var rect = $element.getBoundingClientRect();
  return rect.top <= (window.innerHeight || document.documentElement.clientHeight);
};

var isImageVisible = function isImageVisible() {
  var spliceArray = [];

  if (lazyArray.length > 0) {
    lazyArray.forEach(function (object, index, arr) {
      var onScreen = isVisible(object);

      if (onScreen) {
        //arr.splice(index, 1);
        spliceArray.push(index);
        setSource(object);
        setTimeout(function () {
          object.classList.add('lazy__show');
        }, 100);
      }
    });

    for (var i = spliceArray.length - 1; i >= 0; i--) {
      lazyArray.splice(spliceArray[i], 1);
    }
  }
};

function setSource(node) {
  //instance.emit('src:before', node)
  // prefer srcset, fallback to pixel density
  if (srcset && node.hasAttribute(settings.srcset)) {
    node.setAttribute('srcset', node.getAttribute(settings.srcset));
  } else {
    var retina = dpr > 1 && node.getAttribute(settings.retina);
    if (retina || node.getAttribute(settings.normal)) node.setAttribute('src', retina || node.getAttribute(settings.normal));
  } // instance.emit('src:after', node)
  [settings.normal, settings.retina, settings.srcset].forEach(function (attr) {
    return node.removeAttribute(attr);
  }); // update()
}

var SetUpFaqs = function SetUpFaqs() {
  var section = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'faqs';
  var container = document.getElementById(section);
  var questions = container.getElementsByTagName('h3');

  for (var i = 0; i < questions.length; i++) {
    questions[i].onclick = function (event) {
      var target = event.target;
      var article = target.parentNode;
      var answer = article.querySelector('.answer');
      var closedHeight = target.scrollHeight + 'px';
      article.style.height = article.scrollHeight + 'px';
      article.classList.toggle('open');

      if (article.classList.contains('open')) {
        // total of question + answer
        var totalHeight = target.scrollHeight + answer.scrollHeight + 'px';
        article.style.height = totalHeight;
      } else {
        // 			// make it the question height only
        article.style.height = closedHeight;
      }

      console.log('tgt: ' + target + 'pt: ' + answer + ' pth' + answer.scrollHeight);
    };
  }
};

var isVisible$1 = function isVisible($element) {
  var rect = $element.getBoundingClientRect();
  return rect.bottom >= 0 && rect.right >= 0 && rect.top <= (window.innerHeight || document.documentElement.clientHeight) && rect.left <= (window.innerWidth || document.documentElement.clientWidth);
};

function _typeof(obj) {
  if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") {
    _typeof = function (obj) {
      return typeof obj;
    };
  } else {
    _typeof = function (obj) {
      return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
    };
  }

  return _typeof(obj);
}

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

/*! @vimeo/player v2.8.2 | (c) 2019 Vimeo | MIT License | https://github.com/vimeo/player.js */
(function (global, factory) {
  (typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object' && typeof module !== 'undefined' ? module.exports = factory() : typeof define === 'function' && define.amd ? define(factory) : (global = global || self, (global.Vimeo = global.Vimeo || {}, global.Vimeo.Player = factory()));
})(undefined, function () {

  function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
      throw new TypeError("Cannot call a class as a function");
    }
  }

  function _defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  }

  function _createClass(Constructor, protoProps, staticProps) {
    if (protoProps) _defineProperties(Constructor.prototype, protoProps);
    if (staticProps) _defineProperties(Constructor, staticProps);
    return Constructor;
  }
  /**
   * @module lib/functions
   */

  /**
   * Check to see this is a node environment.
   * @type {Boolean}
   */

  /* global global */


  var isNode = typeof global !== 'undefined' && {}.toString.call(global) === '[object global]';
  /**
   * Get the name of the method for a given getter or setter.
   *
   * @param {string} prop The name of the property.
   * @param {string} type Either “get” or “set”.
   * @return {string}
   */

  function getMethodName(prop, type) {
    if (prop.indexOf(type.toLowerCase()) === 0) {
      return prop;
    }

    return "".concat(type.toLowerCase()).concat(prop.substr(0, 1).toUpperCase()).concat(prop.substr(1));
  }
  /**
   * Check to see if the object is a DOM Element.
   *
   * @param {*} element The object to check.
   * @return {boolean}
   */


  function isDomElement(element) {
    return Boolean(element && element.nodeType === 1 && 'nodeName' in element && element.ownerDocument && element.ownerDocument.defaultView);
  }
  /**
   * Check to see whether the value is a number.
   *
   * @see http://dl.dropboxusercontent.com/u/35146/js/tests/isNumber.html
   * @param {*} value The value to check.
   * @param {boolean} integer Check if the value is an integer.
   * @return {boolean}
   */


  function isInteger(value) {
    // eslint-disable-next-line eqeqeq
    return !isNaN(parseFloat(value)) && isFinite(value) && Math.floor(value) == value;
  }
  /**
   * Check to see if the URL is a Vimeo url.
   *
   * @param {string} url The url string.
   * @return {boolean}
   */


  function isVimeoUrl(url) {
    return /^(https?:)?\/\/((player|www)\.)?vimeo\.com(?=$|\/)/.test(url);
  }
  /**
   * Get the Vimeo URL from an element.
   * The element must have either a data-vimeo-id or data-vimeo-url attribute.
   *
   * @param {object} oEmbedParameters The oEmbed parameters.
   * @return {string}
   */


  function getVimeoUrl() {
    var oEmbedParameters = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    var id = oEmbedParameters.id;
    var url = oEmbedParameters.url;
    var idOrUrl = id || url;

    if (!idOrUrl) {
      throw new Error('An id or url must be passed, either in an options object or as a data-vimeo-id or data-vimeo-url attribute.');
    }

    if (isInteger(idOrUrl)) {
      return "https://vimeo.com/".concat(idOrUrl);
    }

    if (isVimeoUrl(idOrUrl)) {
      return idOrUrl.replace('http:', 'https:');
    }

    if (id) {
      throw new TypeError("\u201C".concat(id, "\u201D is not a valid video id."));
    }

    throw new TypeError("\u201C".concat(idOrUrl, "\u201D is not a vimeo.com url."));
  }

  var arrayIndexOfSupport = typeof Array.prototype.indexOf !== 'undefined';
  var postMessageSupport = typeof window !== 'undefined' && typeof window.postMessage !== 'undefined';

  if (!isNode && (!arrayIndexOfSupport || !postMessageSupport)) {
    throw new Error('Sorry, the Vimeo Player API is not available in this browser.');
  }

  var commonjsGlobal = typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

  function createCommonjsModule(fn, module) {
    return module = {
      exports: {}
    }, fn(module, module.exports), module.exports;
  }
  /*!
   * weakmap-polyfill v2.0.0 - ECMAScript6 WeakMap polyfill
   * https://github.com/polygonplanet/weakmap-polyfill
   * Copyright (c) 2015-2016 polygon planet <polygon.planet.aqua@gmail.com>
   * @license MIT
   */


  (function (self) {
    if (self.WeakMap) {
      return;
    }

    var hasOwnProperty = Object.prototype.hasOwnProperty;

    var defineProperty = function defineProperty(object, name, value) {
      if (Object.defineProperty) {
        Object.defineProperty(object, name, {
          configurable: true,
          writable: true,
          value: value
        });
      } else {
        object[name] = value;
      }
    };

    self.WeakMap = function () {
      // ECMA-262 23.3 WeakMap Objects
      function WeakMap() {
        if (this === void 0) {
          throw new TypeError("Constructor WeakMap requires 'new'");
        }

        defineProperty(this, '_id', genId('_WeakMap')); // ECMA-262 23.3.1.1 WeakMap([iterable])

        if (arguments.length > 0) {
          // Currently, WeakMap `iterable` argument is not supported
          throw new TypeError('WeakMap iterable is not supported');
        }
      } // ECMA-262 23.3.3.2 WeakMap.prototype.delete(key)


      defineProperty(WeakMap.prototype, 'delete', function (key) {
        checkInstance(this, 'delete');

        if (!isObject(key)) {
          return false;
        }

        var entry = key[this._id];

        if (entry && entry[0] === key) {
          delete key[this._id];
          return true;
        }

        return false;
      }); // ECMA-262 23.3.3.3 WeakMap.prototype.get(key)

      defineProperty(WeakMap.prototype, 'get', function (key) {
        checkInstance(this, 'get');

        if (!isObject(key)) {
          return void 0;
        }

        var entry = key[this._id];

        if (entry && entry[0] === key) {
          return entry[1];
        }

        return void 0;
      }); // ECMA-262 23.3.3.4 WeakMap.prototype.has(key)

      defineProperty(WeakMap.prototype, 'has', function (key) {
        checkInstance(this, 'has');

        if (!isObject(key)) {
          return false;
        }

        var entry = key[this._id];

        if (entry && entry[0] === key) {
          return true;
        }

        return false;
      }); // ECMA-262 23.3.3.5 WeakMap.prototype.set(key, value)

      defineProperty(WeakMap.prototype, 'set', function (key, value) {
        checkInstance(this, 'set');

        if (!isObject(key)) {
          throw new TypeError('Invalid value used as weak map key');
        }

        var entry = key[this._id];

        if (entry && entry[0] === key) {
          entry[1] = value;
          return this;
        }

        defineProperty(key, this._id, [key, value]);
        return this;
      });

      function checkInstance(x, methodName) {
        if (!isObject(x) || !hasOwnProperty.call(x, '_id')) {
          throw new TypeError(methodName + ' method called on incompatible receiver ' + _typeof(x));
        }
      }

      function genId(prefix) {
        return prefix + '_' + rand() + '.' + rand();
      }

      function rand() {
        return Math.random().toString().substring(2);
      }

      defineProperty(WeakMap, '_polyfill', true);
      return WeakMap;
    }();

    function isObject(x) {
      return Object(x) === x;
    }
  })(typeof self !== 'undefined' ? self : typeof window !== 'undefined' ? window : typeof commonjsGlobal !== 'undefined' ? commonjsGlobal : commonjsGlobal);

  var npo_src = createCommonjsModule(function (module) {
    /*! Native Promise Only
        v0.8.1 (c) Kyle Simpson
        MIT License: http://getify.mit-license.org
    */
    (function UMD(name, context, definition) {
      // special form of UMD for polyfilling across evironments
      context[name] = context[name] || definition();

      if (module.exports) {
        module.exports = context[name];
      }
    })("Promise", typeof commonjsGlobal != "undefined" ? commonjsGlobal : commonjsGlobal, function DEF() {
      var builtInProp,
          cycle,
          scheduling_queue,
          ToString = Object.prototype.toString,
          timer = typeof setImmediate != "undefined" ? function timer(fn) {
        return setImmediate(fn);
      } : setTimeout; // dammit, IE8.

      try {
        Object.defineProperty({}, "x", {});

        builtInProp = function builtInProp(obj, name, val, config) {
          return Object.defineProperty(obj, name, {
            value: val,
            writable: true,
            configurable: config !== false
          });
        };
      } catch (err) {
        builtInProp = function builtInProp(obj, name, val) {
          obj[name] = val;
          return obj;
        };
      } // Note: using a queue instead of array for efficiency


      scheduling_queue = function Queue() {
        var first, last, item;

        function Item(fn, self) {
          this.fn = fn;
          this.self = self;
          this.next = void 0;
        }

        return {
          add: function add(fn, self) {
            item = new Item(fn, self);

            if (last) {
              last.next = item;
            } else {
              first = item;
            }

            last = item;
            item = void 0;
          },
          drain: function drain() {
            var f = first;
            first = last = cycle = void 0;

            while (f) {
              f.fn.call(f.self);
              f = f.next;
            }
          }
        };
      }();

      function schedule(fn, self) {
        scheduling_queue.add(fn, self);

        if (!cycle) {
          cycle = timer(scheduling_queue.drain);
        }
      } // promise duck typing


      function isThenable(o) {
        var _then,
            o_type = _typeof(o);

        if (o != null && (o_type == "object" || o_type == "function")) {
          _then = o.then;
        }

        return typeof _then == "function" ? _then : false;
      }

      function notify() {
        for (var i = 0; i < this.chain.length; i++) {
          notifyIsolated(this, this.state === 1 ? this.chain[i].success : this.chain[i].failure, this.chain[i]);
        }

        this.chain.length = 0;
      } // NOTE: This is a separate function to isolate
      // the `try..catch` so that other code can be
      // optimized better


      function notifyIsolated(self, cb, chain) {
        var ret, _then;

        try {
          if (cb === false) {
            chain.reject(self.msg);
          } else {
            if (cb === true) {
              ret = self.msg;
            } else {
              ret = cb.call(void 0, self.msg);
            }

            if (ret === chain.promise) {
              chain.reject(TypeError("Promise-chain cycle"));
            } else if (_then = isThenable(ret)) {
              _then.call(ret, chain.resolve, chain.reject);
            } else {
              chain.resolve(ret);
            }
          }
        } catch (err) {
          chain.reject(err);
        }
      }

      function resolve(msg) {
        var _then,
            self = this; // already triggered?


        if (self.triggered) {
          return;
        }

        self.triggered = true; // unwrap

        if (self.def) {
          self = self.def;
        }

        try {
          if (_then = isThenable(msg)) {
            schedule(function () {
              var def_wrapper = new MakeDefWrapper(self);

              try {
                _then.call(msg, function $resolve$() {
                  resolve.apply(def_wrapper, arguments);
                }, function $reject$() {
                  reject.apply(def_wrapper, arguments);
                });
              } catch (err) {
                reject.call(def_wrapper, err);
              }
            });
          } else {
            self.msg = msg;
            self.state = 1;

            if (self.chain.length > 0) {
              schedule(notify, self);
            }
          }
        } catch (err) {
          reject.call(new MakeDefWrapper(self), err);
        }
      }

      function reject(msg) {
        var self = this; // already triggered?

        if (self.triggered) {
          return;
        }

        self.triggered = true; // unwrap

        if (self.def) {
          self = self.def;
        }

        self.msg = msg;
        self.state = 2;

        if (self.chain.length > 0) {
          schedule(notify, self);
        }
      }

      function iteratePromises(Constructor, arr, resolver, rejecter) {
        for (var idx = 0; idx < arr.length; idx++) {
          (function IIFE(idx) {
            Constructor.resolve(arr[idx]).then(function $resolver$(msg) {
              resolver(idx, msg);
            }, rejecter);
          })(idx);
        }
      }

      function MakeDefWrapper(self) {
        this.def = self;
        this.triggered = false;
      }

      function MakeDef(self) {
        this.promise = self;
        this.state = 0;
        this.triggered = false;
        this.chain = [];
        this.msg = void 0;
      }

      function Promise(executor) {
        if (typeof executor != "function") {
          throw TypeError("Not a function");
        }

        if (this.__NPO__ !== 0) {
          throw TypeError("Not a promise");
        } // instance shadowing the inherited "brand"
        // to signal an already "initialized" promise


        this.__NPO__ = 1;
        var def = new MakeDef(this);

        this["then"] = function then(success, failure) {
          var o = {
            success: typeof success == "function" ? success : true,
            failure: typeof failure == "function" ? failure : false
          }; // Note: `then(..)` itself can be borrowed to be used against
          // a different promise constructor for making the chained promise,
          // by substituting a different `this` binding.

          o.promise = new this.constructor(function extractChain(resolve, reject) {
            if (typeof resolve != "function" || typeof reject != "function") {
              throw TypeError("Not a function");
            }

            o.resolve = resolve;
            o.reject = reject;
          });
          def.chain.push(o);

          if (def.state !== 0) {
            schedule(notify, def);
          }

          return o.promise;
        };

        this["catch"] = function $catch$(failure) {
          return this.then(void 0, failure);
        };

        try {
          executor.call(void 0, function publicResolve(msg) {
            resolve.call(def, msg);
          }, function publicReject(msg) {
            reject.call(def, msg);
          });
        } catch (err) {
          reject.call(def, err);
        }
      }

      var PromisePrototype = builtInProp({}, "constructor", Promise,
      /*configurable=*/
      false); // Note: Android 4 cannot use `Object.defineProperty(..)` here

      Promise.prototype = PromisePrototype; // built-in "brand" to signal an "uninitialized" promise

      builtInProp(PromisePrototype, "__NPO__", 0,
      /*configurable=*/
      false);
      builtInProp(Promise, "resolve", function Promise$resolve(msg) {
        var Constructor = this; // spec mandated checks
        // note: best "isPromise" check that's practical for now

        if (msg && _typeof(msg) == "object" && msg.__NPO__ === 1) {
          return msg;
        }

        return new Constructor(function executor(resolve, reject) {
          if (typeof resolve != "function" || typeof reject != "function") {
            throw TypeError("Not a function");
          }

          resolve(msg);
        });
      });
      builtInProp(Promise, "reject", function Promise$reject(msg) {
        return new this(function executor(resolve, reject) {
          if (typeof resolve != "function" || typeof reject != "function") {
            throw TypeError("Not a function");
          }

          reject(msg);
        });
      });
      builtInProp(Promise, "all", function Promise$all(arr) {
        var Constructor = this; // spec mandated checks

        if (ToString.call(arr) != "[object Array]") {
          return Constructor.reject(TypeError("Not an array"));
        }

        if (arr.length === 0) {
          return Constructor.resolve([]);
        }

        return new Constructor(function executor(resolve, reject) {
          if (typeof resolve != "function" || typeof reject != "function") {
            throw TypeError("Not a function");
          }

          var len = arr.length,
              msgs = Array(len),
              count = 0;
          iteratePromises(Constructor, arr, function resolver(idx, msg) {
            msgs[idx] = msg;

            if (++count === len) {
              resolve(msgs);
            }
          }, reject);
        });
      });
      builtInProp(Promise, "race", function Promise$race(arr) {
        var Constructor = this; // spec mandated checks

        if (ToString.call(arr) != "[object Array]") {
          return Constructor.reject(TypeError("Not an array"));
        }

        return new Constructor(function executor(resolve, reject) {
          if (typeof resolve != "function" || typeof reject != "function") {
            throw TypeError("Not a function");
          }

          iteratePromises(Constructor, arr, function resolver(idx, msg) {
            resolve(msg);
          }, reject);
        });
      });
      return Promise;
    });
  });
  /**
   * @module lib/callbacks
   */

  var callbackMap = new WeakMap();
  /**
   * Store a callback for a method or event for a player.
   *
   * @param {Player} player The player object.
   * @param {string} name The method or event name.
   * @param {(function(this:Player, *): void|{resolve: function, reject: function})} callback
   *        The callback to call or an object with resolve and reject functions for a promise.
   * @return {void}
   */

  function storeCallback(player, name, callback) {
    var playerCallbacks = callbackMap.get(player.element) || {};

    if (!(name in playerCallbacks)) {
      playerCallbacks[name] = [];
    }

    playerCallbacks[name].push(callback);
    callbackMap.set(player.element, playerCallbacks);
  }
  /**
   * Get the callbacks for a player and event or method.
   *
   * @param {Player} player The player object.
   * @param {string} name The method or event name
   * @return {function[]}
   */


  function getCallbacks(player, name) {
    var playerCallbacks = callbackMap.get(player.element) || {};
    return playerCallbacks[name] || [];
  }
  /**
   * Remove a stored callback for a method or event for a player.
   *
   * @param {Player} player The player object.
   * @param {string} name The method or event name
   * @param {function} [callback] The specific callback to remove.
   * @return {boolean} Was this the last callback?
   */


  function removeCallback(player, name, callback) {
    var playerCallbacks = callbackMap.get(player.element) || {};

    if (!playerCallbacks[name]) {
      return true;
    } // If no callback is passed, remove all callbacks for the event


    if (!callback) {
      playerCallbacks[name] = [];
      callbackMap.set(player.element, playerCallbacks);
      return true;
    }

    var index = playerCallbacks[name].indexOf(callback);

    if (index !== -1) {
      playerCallbacks[name].splice(index, 1);
    }

    callbackMap.set(player.element, playerCallbacks);
    return playerCallbacks[name] && playerCallbacks[name].length === 0;
  }
  /**
   * Return the first stored callback for a player and event or method.
   *
   * @param {Player} player The player object.
   * @param {string} name The method or event name.
   * @return {function} The callback, or false if there were none
   */


  function shiftCallbacks(player, name) {
    var playerCallbacks = getCallbacks(player, name);

    if (playerCallbacks.length < 1) {
      return false;
    }

    var callback = playerCallbacks.shift();
    removeCallback(player, name, callback);
    return callback;
  }
  /**
   * Move callbacks associated with an element to another element.
   *
   * @param {HTMLElement} oldElement The old element.
   * @param {HTMLElement} newElement The new element.
   * @return {void}
   */


  function swapCallbacks(oldElement, newElement) {
    var playerCallbacks = callbackMap.get(oldElement);
    callbackMap.set(newElement, playerCallbacks);
    callbackMap.delete(oldElement);
  }
  /**
   * @module lib/embed
   */


  var oEmbedParameters = ['autopause', 'autoplay', 'background', 'byline', 'color', 'dnt', 'height', 'id', 'loop', 'maxheight', 'maxwidth', 'muted', 'playsinline', 'portrait', 'responsive', 'speed', 'title', 'transparent', 'url', 'width'];
  /**
   * Get the 'data-vimeo'-prefixed attributes from an element as an object.
   *
   * @param {HTMLElement} element The element.
   * @param {Object} [defaults={}] The default values to use.
   * @return {Object<string, string>}
   */

  function getOEmbedParameters(element) {
    var defaults = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    return oEmbedParameters.reduce(function (params, param) {
      var value = element.getAttribute("data-vimeo-".concat(param));

      if (value || value === '') {
        params[param] = value === '' ? 1 : value;
      }

      return params;
    }, defaults);
  }
  /**
   * Create an embed from oEmbed data inside an element.
   *
   * @param {object} data The oEmbed data.
   * @param {HTMLElement} element The element to put the iframe in.
   * @return {HTMLIFrameElement} The iframe embed.
   */


  function createEmbed(_ref, element) {
    var html = _ref.html;

    if (!element) {
      throw new TypeError('An element must be provided');
    }

    if (element.getAttribute('data-vimeo-initialized') !== null) {
      return element.querySelector('iframe');
    }

    var div = document.createElement('div');
    div.innerHTML = html;
    element.appendChild(div.firstChild);
    element.setAttribute('data-vimeo-initialized', 'true');
    return element.querySelector('iframe');
  }
  /**
   * Make an oEmbed call for the specified URL.
   *
   * @param {string} videoUrl The vimeo.com url for the video.
   * @param {Object} [params] Parameters to pass to oEmbed.
   * @param {HTMLElement} element The element.
   * @return {Promise}
   */


  function getOEmbedData(videoUrl) {
    var params = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
    var element = arguments.length > 2 ? arguments[2] : undefined;
    return new Promise(function (resolve, reject) {
      if (!isVimeoUrl(videoUrl)) {
        throw new TypeError("\u201C".concat(videoUrl, "\u201D is not a vimeo.com url."));
      }

      var url = "https://vimeo.com/api/oembed.json?url=".concat(encodeURIComponent(videoUrl), "&domain=").concat(window.location.hostname);

      for (var param in params) {
        if (params.hasOwnProperty(param)) {
          url += "&".concat(param, "=").concat(encodeURIComponent(params[param]));
        }
      }

      var xhr = 'XDomainRequest' in window ? new XDomainRequest() : new XMLHttpRequest();
      xhr.open('GET', url, true);

      xhr.onload = function () {
        if (xhr.status === 404) {
          reject(new Error("\u201C".concat(videoUrl, "\u201D was not found.")));
          return;
        }

        if (xhr.status === 403) {
          reject(new Error("\u201C".concat(videoUrl, "\u201D is not embeddable.")));
          return;
        }

        try {
          var json = JSON.parse(xhr.responseText); // Check api response for 403 on oembed

          if (json.domain_status_code === 403) {
            // We still want to create the embed to give users visual feedback
            createEmbed(json, element);
            reject(new Error("\u201C".concat(videoUrl, "\u201D is not embeddable.")));
            return;
          }

          resolve(json);
        } catch (error) {
          reject(error);
        }
      };

      xhr.onerror = function () {
        var status = xhr.status ? " (".concat(xhr.status, ")") : '';
        reject(new Error("There was an error fetching the embed code from Vimeo".concat(status, ".")));
      };

      xhr.send();
    });
  }
  /**
   * Initialize all embeds within a specific element
   *
   * @param {HTMLElement} [parent=document] The parent element.
   * @return {void}
   */


  function initializeEmbeds() {
    var parent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document;
    var elements = [].slice.call(parent.querySelectorAll('[data-vimeo-id], [data-vimeo-url]'));

    var handleError = function handleError(error) {
      if ('console' in window && console.error) {
        console.error("There was an error creating an embed: ".concat(error));
      }
    };

    elements.forEach(function (element) {
      try {
        // Skip any that have data-vimeo-defer
        if (element.getAttribute('data-vimeo-defer') !== null) {
          return;
        }

        var params = getOEmbedParameters(element);
        var url = getVimeoUrl(params);
        getOEmbedData(url, params, element).then(function (data) {
          return createEmbed(data, element);
        }).catch(handleError);
      } catch (error) {
        handleError(error);
      }
    });
  }
  /**
   * Resize embeds when messaged by the player.
   *
   * @param {HTMLElement} [parent=document] The parent element.
   * @return {void}
   */


  function resizeEmbeds() {
    var parent = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : document; // Prevent execution if users include the player.js script multiple times.

    if (window.VimeoPlayerResizeEmbeds_) {
      return;
    }

    window.VimeoPlayerResizeEmbeds_ = true;

    var onMessage = function onMessage(event) {
      if (!isVimeoUrl(event.origin)) {
        return;
      } // 'spacechange' is fired only on embeds with cards


      if (!event.data || event.data.event !== 'spacechange') {
        return;
      }

      var iframes = parent.querySelectorAll('iframe');

      for (var i = 0; i < iframes.length; i++) {
        if (iframes[i].contentWindow !== event.source) {
          continue;
        } // Change padding-bottom of the enclosing div to accommodate
        // card carousel without distorting aspect ratio


        var space = iframes[i].parentElement;
        space.style.paddingBottom = "".concat(event.data.data[0].bottom, "px");
        break;
      }
    };

    if (window.addEventListener) {
      window.addEventListener('message', onMessage, false);
    } else if (window.attachEvent) {
      window.attachEvent('onmessage', onMessage);
    }
  }
  /**
   * @module lib/postmessage
   */

  /**
   * Parse a message received from postMessage.
   *
   * @param {*} data The data received from postMessage.
   * @return {object}
   */


  function parseMessageData(data) {
    if (typeof data === 'string') {
      try {
        data = JSON.parse(data);
      } catch (error) {
        // If the message cannot be parsed, throw the error as a warning
        console.warn(error);
        return {};
      }
    }

    return data;
  }
  /**
   * Post a message to the specified target.
   *
   * @param {Player} player The player object to use.
   * @param {string} method The API method to call.
   * @param {object} params The parameters to send to the player.
   * @return {void}
   */


  function postMessage(player, method, params) {
    if (!player.element.contentWindow || !player.element.contentWindow.postMessage) {
      return;
    }

    var message = {
      method: method
    };

    if (params !== undefined) {
      message.value = params;
    } // IE 8 and 9 do not support passing messages, so stringify them


    var ieVersion = parseFloat(navigator.userAgent.toLowerCase().replace(/^.*msie (\d+).*$/, '$1'));

    if (ieVersion >= 8 && ieVersion < 10) {
      message = JSON.stringify(message);
    }

    player.element.contentWindow.postMessage(message, player.origin);
  }
  /**
   * Parse the data received from a message event.
   *
   * @param {Player} player The player that received the message.
   * @param {(Object|string)} data The message data. Strings will be parsed into JSON.
   * @return {void}
   */


  function processData(player, data) {
    data = parseMessageData(data);
    var callbacks = [];
    var param;

    if (data.event) {
      if (data.event === 'error') {
        var promises = getCallbacks(player, data.data.method);
        promises.forEach(function (promise) {
          var error = new Error(data.data.message);
          error.name = data.data.name;
          promise.reject(error);
          removeCallback(player, data.data.method, promise);
        });
      }

      callbacks = getCallbacks(player, "event:".concat(data.event));
      param = data.data;
    } else if (data.method) {
      var callback = shiftCallbacks(player, data.method);

      if (callback) {
        callbacks.push(callback);
        param = data.value;
      }
    }

    callbacks.forEach(function (callback) {
      try {
        if (typeof callback === 'function') {
          callback.call(player, param);
          return;
        }

        callback.resolve(param);
      } catch (e) {// empty
      }
    });
  }

  var playerMap = new WeakMap();
  var readyMap = new WeakMap();

  var Player =
  /*#__PURE__*/
  function () {
    /**
     * Create a Player.
     *
     * @param {(HTMLIFrameElement|HTMLElement|string|jQuery)} element A reference to the Vimeo
     *        player iframe, and id, or a jQuery object.
     * @param {object} [options] oEmbed parameters to use when creating an embed in the element.
     * @return {Player}
     */
    function Player(element) {
      var _this = this;

      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      _classCallCheck(this, Player);
      /* global jQuery */


      if (window.jQuery && element instanceof jQuery) {
        if (element.length > 1 && window.console && console.warn) {
          console.warn('A jQuery object with multiple elements was passed, using the first element.');
        }

        element = element[0];
      } // Find an element by ID


      if (typeof document !== 'undefined' && typeof element === 'string') {
        element = document.getElementById(element);
      } // Not an element!


      if (!isDomElement(element)) {
        throw new TypeError('You must pass either a valid element or a valid id.');
      }

      var win = element.ownerDocument.defaultView; // Already initialized an embed in this div, so grab the iframe

      if (element.nodeName !== 'IFRAME') {
        var iframe = element.querySelector('iframe');

        if (iframe) {
          element = iframe;
        }
      } // iframe url is not a Vimeo url


      if (element.nodeName === 'IFRAME' && !isVimeoUrl(element.getAttribute('src') || '')) {
        throw new Error('The player element passed isn’t a Vimeo embed.');
      } // If there is already a player object in the map, return that


      if (playerMap.has(element)) {
        return playerMap.get(element);
      }

      this.element = element;
      this.origin = '*';
      var readyPromise = new npo_src(function (resolve, reject) {
        var onMessage = function onMessage(event) {
          if (!isVimeoUrl(event.origin) || _this.element.contentWindow !== event.source) {
            return;
          }

          if (_this.origin === '*') {
            _this.origin = event.origin;
          }

          var data = parseMessageData(event.data);
          var isError = data && data.event === 'error';
          var isReadyError = isError && data.data && data.data.method === 'ready';

          if (isReadyError) {
            var error = new Error(data.data.message);
            error.name = data.data.name;
            reject(error);
            return;
          }

          var isReadyEvent = data && data.event === 'ready';
          var isPingResponse = data && data.method === 'ping';

          if (isReadyEvent || isPingResponse) {
            _this.element.setAttribute('data-ready', 'true');

            resolve();
            return;
          }

          processData(_this, data);
        };

        if (win.addEventListener) {
          win.addEventListener('message', onMessage, false);
        } else if (win.attachEvent) {
          win.attachEvent('onmessage', onMessage);
        }

        if (_this.element.nodeName !== 'IFRAME') {
          var params = getOEmbedParameters(element, options);
          var url = getVimeoUrl(params);
          getOEmbedData(url, params, element).then(function (data) {
            var iframe = createEmbed(data, element); // Overwrite element with the new iframe,
            // but store reference to the original element

            _this.element = iframe;
            _this._originalElement = element;
            swapCallbacks(element, iframe);
            playerMap.set(_this.element, _this);
            return data;
          }).catch(reject);
        }
      }); // Store a copy of this Player in the map

      readyMap.set(this, readyPromise);
      playerMap.set(this.element, this); // Send a ping to the iframe so the ready promise will be resolved if
      // the player is already ready.

      if (this.element.nodeName === 'IFRAME') {
        postMessage(this, 'ping');
      }

      return this;
    }
    /**
     * Get a promise for a method.
     *
     * @param {string} name The API method to call.
     * @param {Object} [args={}] Arguments to send via postMessage.
     * @return {Promise}
     */


    _createClass(Player, [{
      key: "callMethod",
      value: function callMethod(name) {
        var _this2 = this;

        var args = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return new npo_src(function (resolve, reject) {
          // We are storing the resolve/reject handlers to call later, so we
          // can’t return here.
          // eslint-disable-next-line promise/always-return
          return _this2.ready().then(function () {
            storeCallback(_this2, name, {
              resolve: resolve,
              reject: reject
            });
            postMessage(_this2, name, args);
          }).catch(reject);
        });
      }
      /**
       * Get a promise for the value of a player property.
       *
       * @param {string} name The property name
       * @return {Promise}
       */

    }, {
      key: "get",
      value: function get(name) {
        var _this3 = this;

        return new npo_src(function (resolve, reject) {
          name = getMethodName(name, 'get'); // We are storing the resolve/reject handlers to call later, so we
          // can’t return here.
          // eslint-disable-next-line promise/always-return

          return _this3.ready().then(function () {
            storeCallback(_this3, name, {
              resolve: resolve,
              reject: reject
            });
            postMessage(_this3, name);
          }).catch(reject);
        });
      }
      /**
       * Get a promise for setting the value of a player property.
       *
       * @param {string} name The API method to call.
       * @param {mixed} value The value to set.
       * @return {Promise}
       */

    }, {
      key: "set",
      value: function set(name, value) {
        var _this4 = this;

        return new npo_src(function (resolve, reject) {
          name = getMethodName(name, 'set');

          if (value === undefined || value === null) {
            throw new TypeError('There must be a value to set.');
          } // We are storing the resolve/reject handlers to call later, so we
          // can’t return here.
          // eslint-disable-next-line promise/always-return


          return _this4.ready().then(function () {
            storeCallback(_this4, name, {
              resolve: resolve,
              reject: reject
            });
            postMessage(_this4, name, value);
          }).catch(reject);
        });
      }
      /**
       * Add an event listener for the specified event. Will call the
       * callback with a single parameter, `data`, that contains the data for
       * that event.
       *
       * @param {string} eventName The name of the event.
       * @param {function(*)} callback The function to call when the event fires.
       * @return {void}
       */

    }, {
      key: "on",
      value: function on(eventName, callback) {
        if (!eventName) {
          throw new TypeError('You must pass an event name.');
        }

        if (!callback) {
          throw new TypeError('You must pass a callback function.');
        }

        if (typeof callback !== 'function') {
          throw new TypeError('The callback must be a function.');
        }

        var callbacks = getCallbacks(this, "event:".concat(eventName));

        if (callbacks.length === 0) {
          this.callMethod('addEventListener', eventName).catch(function () {// Ignore the error. There will be an error event fired that
            // will trigger the error callback if they are listening.
          });
        }

        storeCallback(this, "event:".concat(eventName), callback);
      }
      /**
       * Remove an event listener for the specified event. Will remove all
       * listeners for that event if a `callback` isn’t passed, or only that
       * specific callback if it is passed.
       *
       * @param {string} eventName The name of the event.
       * @param {function} [callback] The specific callback to remove.
       * @return {void}
       */

    }, {
      key: "off",
      value: function off(eventName, callback) {
        if (!eventName) {
          throw new TypeError('You must pass an event name.');
        }

        if (callback && typeof callback !== 'function') {
          throw new TypeError('The callback must be a function.');
        }

        var lastCallback = removeCallback(this, "event:".concat(eventName), callback); // If there are no callbacks left, remove the listener

        if (lastCallback) {
          this.callMethod('removeEventListener', eventName).catch(function (e) {// Ignore the error. There will be an error event fired that
            // will trigger the error callback if they are listening.
          });
        }
      }
      /**
       * A promise to load a new video.
       *
       * @promise LoadVideoPromise
       * @fulfill {number} The video with this id successfully loaded.
       * @reject {TypeError} The id was not a number.
       */

      /**
       * Load a new video into this embed. The promise will be resolved if
       * the video is successfully loaded, or it will be rejected if it could
       * not be loaded.
       *
       * @param {number|object} options The id of the video or an object with embed options.
       * @return {LoadVideoPromise}
       */

    }, {
      key: "loadVideo",
      value: function loadVideo(options) {
        return this.callMethod('loadVideo', options);
      }
      /**
       * A promise to perform an action when the Player is ready.
       *
       * @todo document errors
       * @promise LoadVideoPromise
       * @fulfill {void}
       */

      /**
       * Trigger a function when the player iframe has initialized. You do not
       * need to wait for `ready` to trigger to begin adding event listeners
       * or calling other methods.
       *
       * @return {ReadyPromise}
       */

    }, {
      key: "ready",
      value: function ready() {
        var readyPromise = readyMap.get(this) || new npo_src(function (resolve, reject) {
          reject(new Error('Unknown player. Probably unloaded.'));
        });
        return npo_src.resolve(readyPromise);
      }
      /**
       * A promise to add a cue point to the player.
       *
       * @promise AddCuePointPromise
       * @fulfill {string} The id of the cue point to use for removeCuePoint.
       * @reject {RangeError} the time was less than 0 or greater than the
       *         video’s duration.
       * @reject {UnsupportedError} Cue points are not supported with the current
       *         player or browser.
       */

      /**
       * Add a cue point to the player.
       *
       * @param {number} time The time for the cue point.
       * @param {object} [data] Arbitrary data to be returned with the cue point.
       * @return {AddCuePointPromise}
       */

    }, {
      key: "addCuePoint",
      value: function addCuePoint(time) {
        var data = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
        return this.callMethod('addCuePoint', {
          time: time,
          data: data
        });
      }
      /**
       * A promise to remove a cue point from the player.
       *
       * @promise AddCuePointPromise
       * @fulfill {string} The id of the cue point that was removed.
       * @reject {InvalidCuePoint} The cue point with the specified id was not
       *         found.
       * @reject {UnsupportedError} Cue points are not supported with the current
       *         player or browser.
       */

      /**
       * Remove a cue point from the video.
       *
       * @param {string} id The id of the cue point to remove.
       * @return {RemoveCuePointPromise}
       */

    }, {
      key: "removeCuePoint",
      value: function removeCuePoint(id) {
        return this.callMethod('removeCuePoint', id);
      }
      /**
       * A representation of a text track on a video.
       *
       * @typedef {Object} VimeoTextTrack
       * @property {string} language The ISO language code.
       * @property {string} kind The kind of track it is (captions or subtitles).
       * @property {string} label The human‐readable label for the track.
       */

      /**
       * A promise to enable a text track.
       *
       * @promise EnableTextTrackPromise
       * @fulfill {VimeoTextTrack} The text track that was enabled.
       * @reject {InvalidTrackLanguageError} No track was available with the
       *         specified language.
       * @reject {InvalidTrackError} No track was available with the specified
       *         language and kind.
       */

      /**
       * Enable the text track with the specified language, and optionally the
       * specified kind (captions or subtitles).
       *
       * When set via the API, the track language will not change the viewer’s
       * stored preference.
       *
       * @param {string} language The two‐letter language code.
       * @param {string} [kind] The kind of track to enable (captions or subtitles).
       * @return {EnableTextTrackPromise}
       */

    }, {
      key: "enableTextTrack",
      value: function enableTextTrack(language, kind) {
        if (!language) {
          throw new TypeError('You must pass a language.');
        }

        return this.callMethod('enableTextTrack', {
          language: language,
          kind: kind
        });
      }
      /**
       * A promise to disable the active text track.
       *
       * @promise DisableTextTrackPromise
       * @fulfill {void} The track was disabled.
       */

      /**
       * Disable the currently-active text track.
       *
       * @return {DisableTextTrackPromise}
       */

    }, {
      key: "disableTextTrack",
      value: function disableTextTrack() {
        return this.callMethod('disableTextTrack');
      }
      /**
       * A promise to pause the video.
       *
       * @promise PausePromise
       * @fulfill {void} The video was paused.
       */

      /**
       * Pause the video if it’s playing.
       *
       * @return {PausePromise}
       */

    }, {
      key: "pause",
      value: function pause() {
        return this.callMethod('pause');
      }
      /**
       * A promise to play the video.
       *
       * @promise PlayPromise
       * @fulfill {void} The video was played.
       */

      /**
       * Play the video if it’s paused. **Note:** on iOS and some other
       * mobile devices, you cannot programmatically trigger play. Once the
       * viewer has tapped on the play button in the player, however, you
       * will be able to use this function.
       *
       * @return {PlayPromise}
       */

    }, {
      key: "play",
      value: function play() {
        return this.callMethod('play');
      }
      /**
       * A promise to unload the video.
       *
       * @promise UnloadPromise
       * @fulfill {void} The video was unloaded.
       */

      /**
       * Return the player to its initial state.
       *
       * @return {UnloadPromise}
       */

    }, {
      key: "unload",
      value: function unload() {
        return this.callMethod('unload');
      }
      /**
       * Cleanup the player and remove it from the DOM
       *
       * It won't be usable and a new one should be constructed
       *  in order to do any operations.
       *
       * @return {Promise}
       */

    }, {
      key: "destroy",
      value: function destroy() {
        var _this5 = this;

        return new npo_src(function (resolve) {
          readyMap.delete(_this5);
          playerMap.delete(_this5.element);

          if (_this5._originalElement) {
            playerMap.delete(_this5._originalElement);

            _this5._originalElement.removeAttribute('data-vimeo-initialized');
          }

          if (_this5.element && _this5.element.nodeName === 'IFRAME' && _this5.element.parentNode) {
            _this5.element.parentNode.removeChild(_this5.element);
          }

          resolve();
        });
      }
      /**
       * A promise to get the autopause behavior of the video.
       *
       * @promise GetAutopausePromise
       * @fulfill {boolean} Whether autopause is turned on or off.
       * @reject {UnsupportedError} Autopause is not supported with the current
       *         player or browser.
       */

      /**
       * Get the autopause behavior for this player.
       *
       * @return {GetAutopausePromise}
       */

    }, {
      key: "getAutopause",
      value: function getAutopause() {
        return this.get('autopause');
      }
      /**
       * A promise to set the autopause behavior of the video.
       *
       * @promise SetAutopausePromise
       * @fulfill {boolean} Whether autopause is turned on or off.
       * @reject {UnsupportedError} Autopause is not supported with the current
       *         player or browser.
       */

      /**
       * Enable or disable the autopause behavior of this player.
       *
       * By default, when another video is played in the same browser, this
       * player will automatically pause. Unless you have a specific reason
       * for doing so, we recommend that you leave autopause set to the
       * default (`true`).
       *
       * @param {boolean} autopause
       * @return {SetAutopausePromise}
       */

    }, {
      key: "setAutopause",
      value: function setAutopause(autopause) {
        return this.set('autopause', autopause);
      }
      /**
       * A promise to get the buffered property of the video.
       *
       * @promise GetBufferedPromise
       * @fulfill {Array} Buffered Timeranges converted to an Array.
       */

      /**
       * Get the buffered property of the video.
       *
       * @return {GetBufferedPromise}
       */

    }, {
      key: "getBuffered",
      value: function getBuffered() {
        return this.get('buffered');
      }
      /**
       * A promise to get the color of the player.
       *
       * @promise GetColorPromise
       * @fulfill {string} The hex color of the player.
       */

      /**
       * Get the color for this player.
       *
       * @return {GetColorPromise}
       */

    }, {
      key: "getColor",
      value: function getColor() {
        return this.get('color');
      }
      /**
       * A promise to set the color of the player.
       *
       * @promise SetColorPromise
       * @fulfill {string} The color was successfully set.
       * @reject {TypeError} The string was not a valid hex or rgb color.
       * @reject {ContrastError} The color was set, but the contrast is
       *         outside of the acceptable range.
       * @reject {EmbedSettingsError} The owner of the player has chosen to
       *         use a specific color.
       */

      /**
       * Set the color of this player to a hex or rgb string. Setting the
       * color may fail if the owner of the video has set their embed
       * preferences to force a specific color.
       *
       * @param {string} color The hex or rgb color string to set.
       * @return {SetColorPromise}
       */

    }, {
      key: "setColor",
      value: function setColor(color) {
        return this.set('color', color);
      }
      /**
       * A representation of a cue point.
       *
       * @typedef {Object} VimeoCuePoint
       * @property {number} time The time of the cue point.
       * @property {object} data The data passed when adding the cue point.
       * @property {string} id The unique id for use with removeCuePoint.
       */

      /**
       * A promise to get the cue points of a video.
       *
       * @promise GetCuePointsPromise
       * @fulfill {VimeoCuePoint[]} The cue points added to the video.
       * @reject {UnsupportedError} Cue points are not supported with the current
       *         player or browser.
       */

      /**
       * Get an array of the cue points added to the video.
       *
       * @return {GetCuePointsPromise}
       */

    }, {
      key: "getCuePoints",
      value: function getCuePoints() {
        return this.get('cuePoints');
      }
      /**
       * A promise to get the current time of the video.
       *
       * @promise GetCurrentTimePromise
       * @fulfill {number} The current time in seconds.
       */

      /**
       * Get the current playback position in seconds.
       *
       * @return {GetCurrentTimePromise}
       */

    }, {
      key: "getCurrentTime",
      value: function getCurrentTime() {
        return this.get('currentTime');
      }
      /**
       * A promise to set the current time of the video.
       *
       * @promise SetCurrentTimePromise
       * @fulfill {number} The actual current time that was set.
       * @reject {RangeError} the time was less than 0 or greater than the
       *         video’s duration.
       */

      /**
       * Set the current playback position in seconds. If the player was
       * paused, it will remain paused. Likewise, if the player was playing,
       * it will resume playing once the video has buffered.
       *
       * You can provide an accurate time and the player will attempt to seek
       * to as close to that time as possible. The exact time will be the
       * fulfilled value of the promise.
       *
       * @param {number} currentTime
       * @return {SetCurrentTimePromise}
       */

    }, {
      key: "setCurrentTime",
      value: function setCurrentTime(currentTime) {
        return this.set('currentTime', currentTime);
      }
      /**
       * A promise to get the duration of the video.
       *
       * @promise GetDurationPromise
       * @fulfill {number} The duration in seconds.
       */

      /**
       * Get the duration of the video in seconds. It will be rounded to the
       * nearest second before playback begins, and to the nearest thousandth
       * of a second after playback begins.
       *
       * @return {GetDurationPromise}
       */

    }, {
      key: "getDuration",
      value: function getDuration() {
        return this.get('duration');
      }
      /**
       * A promise to get the ended state of the video.
       *
       * @promise GetEndedPromise
       * @fulfill {boolean} Whether or not the video has ended.
       */

      /**
       * Get the ended state of the video. The video has ended if
       * `currentTime === duration`.
       *
       * @return {GetEndedPromise}
       */

    }, {
      key: "getEnded",
      value: function getEnded() {
        return this.get('ended');
      }
      /**
       * A promise to get the loop state of the player.
       *
       * @promise GetLoopPromise
       * @fulfill {boolean} Whether or not the player is set to loop.
       */

      /**
       * Get the loop state of the player.
       *
       * @return {GetLoopPromise}
       */

    }, {
      key: "getLoop",
      value: function getLoop() {
        return this.get('loop');
      }
      /**
       * A promise to set the loop state of the player.
       *
       * @promise SetLoopPromise
       * @fulfill {boolean} The loop state that was set.
       */

      /**
       * Set the loop state of the player. When set to `true`, the player
       * will start over immediately once playback ends.
       *
       * @param {boolean} loop
       * @return {SetLoopPromise}
       */

    }, {
      key: "setLoop",
      value: function setLoop(loop) {
        return this.set('loop', loop);
      }
      /**
       * A promise to get the paused state of the player.
       *
       * @promise GetLoopPromise
       * @fulfill {boolean} Whether or not the video is paused.
       */

      /**
       * Get the paused state of the player.
       *
       * @return {GetLoopPromise}
       */

    }, {
      key: "getPaused",
      value: function getPaused() {
        return this.get('paused');
      }
      /**
       * A promise to get the playback rate of the player.
       *
       * @promise GetPlaybackRatePromise
       * @fulfill {number} The playback rate of the player on a scale from 0.5 to 2.
       */

      /**
       * Get the playback rate of the player on a scale from `0.5` to `2`.
       *
       * @return {GetPlaybackRatePromise}
       */

    }, {
      key: "getPlaybackRate",
      value: function getPlaybackRate() {
        return this.get('playbackRate');
      }
      /**
       * A promise to set the playbackrate of the player.
       *
       * @promise SetPlaybackRatePromise
       * @fulfill {number} The playback rate was set.
       * @reject {RangeError} The playback rate was less than 0.5 or greater than 2.
       */

      /**
       * Set the playback rate of the player on a scale from `0.5` to `2`. When set
       * via the API, the playback rate will not be synchronized to other
       * players or stored as the viewer's preference.
       *
       * @param {number} playbackRate
       * @return {SetPlaybackRatePromise}
       */

    }, {
      key: "setPlaybackRate",
      value: function setPlaybackRate(playbackRate) {
        return this.set('playbackRate', playbackRate);
      }
      /**
       * A promise to get the played property of the video.
       *
       * @promise GetPlayedPromise
       * @fulfill {Array} Played Timeranges converted to an Array.
       */

      /**
       * Get the played property of the video.
       *
       * @return {GetPlayedPromise}
       */

    }, {
      key: "getPlayed",
      value: function getPlayed() {
        return this.get('played');
      }
      /**
       * A promise to get the seekable property of the video.
       *
       * @promise GetSeekablePromise
       * @fulfill {Array} Seekable Timeranges converted to an Array.
       */

      /**
       * Get the seekable property of the video.
       *
       * @return {GetSeekablePromise}
       */

    }, {
      key: "getSeekable",
      value: function getSeekable() {
        return this.get('seekable');
      }
      /**
       * A promise to get the seeking property of the player.
       *
       * @promise GetSeekingPromise
       * @fulfill {boolean} Whether or not the player is currently seeking.
       */

      /**
       * Get if the player is currently seeking.
       *
       * @return {GetSeekingPromise}
       */

    }, {
      key: "getSeeking",
      value: function getSeeking() {
        return this.get('seeking');
      }
      /**
       * A promise to get the text tracks of a video.
       *
       * @promise GetTextTracksPromise
       * @fulfill {VimeoTextTrack[]} The text tracks associated with the video.
       */

      /**
       * Get an array of the text tracks that exist for the video.
       *
       * @return {GetTextTracksPromise}
       */

    }, {
      key: "getTextTracks",
      value: function getTextTracks() {
        return this.get('textTracks');
      }
      /**
       * A promise to get the embed code for the video.
       *
       * @promise GetVideoEmbedCodePromise
       * @fulfill {string} The `<iframe>` embed code for the video.
       */

      /**
       * Get the `<iframe>` embed code for the video.
       *
       * @return {GetVideoEmbedCodePromise}
       */

    }, {
      key: "getVideoEmbedCode",
      value: function getVideoEmbedCode() {
        return this.get('videoEmbedCode');
      }
      /**
       * A promise to get the id of the video.
       *
       * @promise GetVideoIdPromise
       * @fulfill {number} The id of the video.
       */

      /**
       * Get the id of the video.
       *
       * @return {GetVideoIdPromise}
       */

    }, {
      key: "getVideoId",
      value: function getVideoId() {
        return this.get('videoId');
      }
      /**
       * A promise to get the title of the video.
       *
       * @promise GetVideoTitlePromise
       * @fulfill {number} The title of the video.
       */

      /**
       * Get the title of the video.
       *
       * @return {GetVideoTitlePromise}
       */

    }, {
      key: "getVideoTitle",
      value: function getVideoTitle() {
        return this.get('videoTitle');
      }
      /**
       * A promise to get the native width of the video.
       *
       * @promise GetVideoWidthPromise
       * @fulfill {number} The native width of the video.
       */

      /**
       * Get the native width of the currently‐playing video. The width of
       * the highest‐resolution available will be used before playback begins.
       *
       * @return {GetVideoWidthPromise}
       */

    }, {
      key: "getVideoWidth",
      value: function getVideoWidth() {
        return this.get('videoWidth');
      }
      /**
       * A promise to get the native height of the video.
       *
       * @promise GetVideoHeightPromise
       * @fulfill {number} The native height of the video.
       */

      /**
       * Get the native height of the currently‐playing video. The height of
       * the highest‐resolution available will be used before playback begins.
       *
       * @return {GetVideoHeightPromise}
       */

    }, {
      key: "getVideoHeight",
      value: function getVideoHeight() {
        return this.get('videoHeight');
      }
      /**
       * A promise to get the vimeo.com url for the video.
       *
       * @promise GetVideoUrlPromise
       * @fulfill {number} The vimeo.com url for the video.
       * @reject {PrivacyError} The url isn’t available because of the video’s privacy setting.
       */

      /**
       * Get the vimeo.com url for the video.
       *
       * @return {GetVideoUrlPromise}
       */

    }, {
      key: "getVideoUrl",
      value: function getVideoUrl() {
        return this.get('videoUrl');
      }
      /**
       * A promise to get the volume level of the player.
       *
       * @promise GetVolumePromise
       * @fulfill {number} The volume level of the player on a scale from 0 to 1.
       */

      /**
       * Get the current volume level of the player on a scale from `0` to `1`.
       *
       * Most mobile devices do not support an independent volume from the
       * system volume. In those cases, this method will always return `1`.
       *
       * @return {GetVolumePromise}
       */

    }, {
      key: "getVolume",
      value: function getVolume() {
        return this.get('volume');
      }
      /**
       * A promise to set the volume level of the player.
       *
       * @promise SetVolumePromise
       * @fulfill {number} The volume was set.
       * @reject {RangeError} The volume was less than 0 or greater than 1.
       */

      /**
       * Set the volume of the player on a scale from `0` to `1`. When set
       * via the API, the volume level will not be synchronized to other
       * players or stored as the viewer’s preference.
       *
       * Most mobile devices do not support setting the volume. An error will
       * *not* be triggered in that situation.
       *
       * @param {number} volume
       * @return {SetVolumePromise}
       */

    }, {
      key: "setVolume",
      value: function setVolume(volume) {
        return this.set('volume', volume);
      }
    }]);

    return Player;
  }(); // Setup embed only if this is not a node environment


  if (!isNode) {
    initializeEmbeds();
    resizeEmbeds();
  }

  return Player;
});

var vidArray = [];

var SetUpVideos = function SetUpVideos() {
  var videoItems = document.getElementsByClassName('video-header');
  var videoItemsDriver = document.getElementsByClassName('video-driver');
  var videoItemsClick = document.getElementsByClassName('video-holder-click');
  var videoItemsSlider = document.getElementsByClassName('video-holder-slider');

  for (var i = 0; i < videoItems.length; i++) {
    fInitVid(videoItems[i]);
  } ///criver


  for (var _i = 0; _i < videoItemsDriver.length; _i++) {
    videoItemsDriver[_i].setAttribute('id', 'video-driver' + _i);

    fInitVid(videoItemsDriver[_i]);
  } ///Video


  var _loop = function _loop(_i2) {
    var $vid = videoItemsClick[_i2];
    $vid.setAttribute('id', 'video-holder' + _i2);

    $vid.onclick = function (event) {
      if (!$vid.classList.contains('show-video')) {
        fInitVid($vid);
      }
    };
  };

  for (var _i2 = 0; _i2 < videoItemsClick.length; _i2++) {
    _loop(_i2);
  } ////Video slider


  for (var _i3 = 0; _i3 < videoItemsSlider.length; _i3++) {
    videoItemsSlider[_i3].setAttribute('id', 'video-slider' + _i3);

    fInitVid(videoItemsSlider[_i3]);
  }
};

window.addEventListener('scroll', function (event) {
  isVideoVisible();
});
window.addEventListener('resize', function (event) {
  isVideoVisible();
});

var fInitVid = function fInitVid($vid) {
  var id = $vid.getAttribute("data-id");
  var autoplay = $vid.getAttribute("data-autoplay");
  var muted = $vid.getAttribute("data-muted");
  var node_id = $vid.getAttribute("id");
  var iframe;
  var close = $vid.getElementsByClassName('close-video');
  var playpause = $vid.getElementsByClassName('play-video')[0];
  var ismute = false;
  if (muted == "true") ismute = true;
  var player;
  $vid.classList.add('show-video');
  var options = {
    id: id,
    byline: false,
    portrait: false,
    title: false,
    autoplay: false,
    loop: true,
    controls: false,
    app_id: 122963,
    muted: ismute
  };
  player = new Vimeo.Player(node_id, options);
  player.ready().then(function () {
    iframe = $vid.getElementsByTagName('iframe')[0]; //console.log ('vimeo ready' + player + ' ap: '+autoplay + ' '+playpause);

    $vid.classList.add('video-ready'); ///if data-autoplay = autoplay

    if (autoplay == "autoplay") fPlayPause(null, player, playpause); //

    vidArray.push({
      iframe: iframe,
      player: player,
      playpause: playpause,
      id: id,
      $vid: $vid
    });
  });
  player.on('bufferstart', function (data) {});
  player.on('play', function (data) {//console.log ('vMeo on play' + iframe + ' ');
  }); //
  ////Play pause

  if (playpause) playpause.onclick = function () {
    return fPlayPause(event, player, playpause);
  };
};

var fPlayPause = function fPlayPause(e, player, playpause) {
  player.getPaused().then(function (paused) {
    // paused = whether or not the player is paused
    if (paused) {
      if (playpause != undefined) {
        playpause.setAttribute('data-copy', 'Pause video');
        if (!playpause.classList.contains('is-playing')) playpause.classList.add('is-playing');
      }

      vimPlay(player);
    } else {
      if (playpause != undefined) {
        playpause.setAttribute('data-copy', 'Play video');
        if (playpause.classList.contains('is-playing')) playpause.classList.remove('is-playing');
      }

      vimPause(player);
    }
  }).catch(function (error) {// an error occurred
  });
};

var vimPause = function vimPause(player) {
  player.pause().then(function () {// the video was paused
  }).catch(function (error) {
    switch (error.name) {
      case 'PasswordError':
        // the video is password-protected and the viewer needs to enter the
        // password first
        break;

      case 'PrivacyError':
        break;

      default:
        // some other error occurred
        break;
    }
  });
};

var vimPlay = function vimPlay(player) {
  //$vid.classList.add('video-ready');
  player.play().then(function () {// the video was played
  }).catch(function (error) {
    switch (error.name) {
      case 'PasswordError':
        // the video is password-protected and the viewer needs to enter the
        // password first
        break;

      case 'PrivacyError':
        // the video is private
        break;

      default:
        // some other error occurred
        break;
    }
  });
};

var isVideoVisible = function isVideoVisible() {
  var spliceArray = [];
  vidArray.forEach(function (arr, index, object) {
    ///sliders remeve the element so we should destry the player
    var originalElement = arr.player._originalElement;
    if (originalElement == undefined) spliceArray.push(index);
    if (arr.iframe != undefined) var onScreen = isVisible$1(arr.iframe);

    if (!onScreen) {
      var hasAutopause = arr.$vid.getAttribute('data-autopause');

      if (hasAutopause == null || hasAutopause != "false") {
        vimPause(arr.player);
        var playpause = arr.playpause;

        if (playpause != undefined) {
          playpause.setAttribute('data-copy', 'Play video');
          if (playpause.classList.contains('is-playing')) playpause.classList.remove('is-playing');
        } ///


        spliceArray.push(index);
      }
    }
  });

  for (var i = spliceArray.length - 1; i >= 0; i--) {
    vidArray.splice(spliceArray[i], 1);
  }
};

var destroyPlayer = function destroyPlayer(player) {
  var arrayIndex = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : null;
  var $vid = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
  var $replace = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : null;
  player.destroy().then(function () {
    // the player was destroyed
    ///remove the classes added
    if ($vid != null) $vid.classList.remove('show-video', 'video-ready'); ////if we're replacing add new object here

    if ($replace != null) fInitVid($replace); ///remove fro the array

    if (arrayIndex != null) vidArray.splice(arrayIndex, 1);
  }).catch(function (error) {// an error occurred
  });
};

var SetUpArchives = function SetUpArchives(section) {
  var containers = document.getElementsByClassName(section);

  var _loop2 = function _loop2(i) {
    var container = containers[i];
    var tabs = container.querySelectorAll('.tabs li');
    var thumbs = container.querySelectorAll('.driver-video');

    var _loop3 = function _loop3(_i4) {
      var tab = tabs[_i4];
      var n = tab.getAttribute('data-tab');

      tab.onclick = function (event) {
        return animate(event, n, container);
      };
    };

    for (var _i4 = 0; _i4 < tabs.length; _i4++) {
      _loop3(_i4);
    }

    var _loop4 = function _loop4(_i5) {
      var thumb = thumbs[_i5];
      var code = thumb.getAttribute('data-code');

      thumb.onclick = function (event) {
        return newFilm(event, code, container);
      };
    };

    for (var _i5 = 0; _i5 < thumbs.length; _i5++) {
      _loop4(_i5);
    }
  };

  for (var i = 0; i < containers.length; i++) {
    _loop2(i);
  }
};

var animate = function animate(event, id, $container) {
  var active = $container.querySelectorAll('.films-holder.active');
  var tabActive = event.currentTarget;
  var tab = $container.querySelectorAll('.tabs .active');
  var item = $container.querySelectorAll('div[data-item="' + id + '"]');
  active[0].classList.remove('active');
  item[0].classList.add('active');
  tab[0].classList.remove('active');
  tabActive.classList.add('active');
};

var newFilm = function newFilm(event, $code, $container) {
  var current = $container.querySelectorAll('.active .video-holder');
  var $vid = current[0];
  var currentId = $vid.getAttribute('data-id');
  var inArray = false;
  $vid.setAttribute('data-id', $code);
  $vid.removeAttribute('style');
  vidArray.forEach(function (arr, index, object) {
    if (arr.id == currentId) {
      destroyPlayer(arr.player, index, arr.$vid, $vid);
      inArray = true;
    }
  });

  if (inArray === false) {
    fInitVid($vid);
  }
};

var parallaxArray = [];

var SetUpParallax = function SetUpParallax() {
  parallaxArray.length = 0;
  var imageItems = document.querySelectorAll('.img-parallax'); //settings.threshold = $threshold;            

  for (var i = 0; i < imageItems.length; i++) {
    parallaxArray.push(imageItems[i]);
  }

  SetUpEvents$1();
  requestFrame$1();
};

var SetUpEvents$1 = function SetUpEvents() {
  ['resize', 'scroll', 'load'].forEach(function (evt) {
    return window.addEventListener(evt, requestFrame$1, false);
  });
};

function requestFrame$1() {
  window.requestAnimationFrame(function () {
    return fCheckParallax();
  });
}

var fCheckParallax = function fCheckParallax() {
  for (var i = 0; i < parallaxArray.length; i++) {
    var container = parallaxArray[i];
    var img = container.querySelector('img'); ///check it's got height, if not remove parallax

    var h = container.scrollHeight;
    if (h <= 0 || h == undefined) container.classList.remove('img-parallax'); //

    var rect = container.getBoundingClientRect();

    if (rect.top <= (window.innerHeight || document.documentElement.clientHeight)) {
      var moves = container.scrollHeight + window.innerHeight;
      var rb = rect.bottom;
      var ratio = (rb / moves).toFixed(2); // 

      var overlap = 20; //20% set in css (height = 120%)

      var y = overlap * ratio;
      var coords = 'translate3d(-50%, -' + y + '%, 0)';
      img.setAttribute("style", "transform:" + coords);
    }
  }
};

/**
 * Hi :-) This is a class representing a Siema.
 */
var Siema =
/*#__PURE__*/
function () {
  /**
   * Create a Siema.
   * @param {Object} options - Optional settings object.
   */
  function Siema(options) {
    var _this = this;

    _classCallCheck(this, Siema);

    // Merge defaults with user's settings
    this.config = Siema.mergeSettings(options); // Resolve selector's type

    this.selector = typeof this.config.selector === 'string' ? document.querySelector(this.config.selector) : this.config.selector; // Early throw if selector doesn't exists

    if (this.selector === null) {
      throw new Error('Something wrong with your selector 😭');
    } // update perPage number dependable of user value


    this.resolveSlidesNumber(); // Create global references

    this.selectorWidth = this.selector.offsetWidth;
    this.innerElements = [].slice.call(this.selector.children);
    this.currentSlide = this.config.loop ? this.config.startIndex % this.innerElements.length : Math.max(0, Math.min(this.config.startIndex, this.innerElements.length - this.perPage));
    this.transformProperty = Siema.webkitOrNot(); // Bind all event handlers for referencability

    ['resizeHandler', 'touchstartHandler', 'touchendHandler', 'touchmoveHandler', 'mousedownHandler', 'mouseupHandler', 'mouseleaveHandler', 'mousemoveHandler', 'clickHandler'].forEach(function (method) {
      _this[method] = _this[method].bind(_this);
    }); // Build markup and apply required styling to elements

    this.init();
  }
  /**
   * Overrides default settings with custom ones.
   * @param {Object} options - Optional settings object.
   * @returns {Object} - Custom Siema settings.
   */


  _createClass(Siema, [{
    key: "attachEvents",

    /**
     * Attaches listeners to required events.
     */
    value: function attachEvents() {
      // Resize element on window resize
      window.addEventListener('resize', this.resizeHandler); // If element is draggable / swipable, add event handlers

      if (this.config.draggable) {
        // Keep track pointer hold and dragging distance
        this.pointerDown = false;
        this.drag = {
          startX: 0,
          endX: 0,
          startY: 0,
          letItGo: null,
          preventClick: false
        }; // Touch events

        this.selector.addEventListener('touchstart', this.touchstartHandler);
        this.selector.addEventListener('touchend', this.touchendHandler);
        this.selector.addEventListener('touchmove', this.touchmoveHandler); // Mouse events

        this.selector.addEventListener('mousedown', this.mousedownHandler);
        this.selector.addEventListener('mouseup', this.mouseupHandler);
        this.selector.addEventListener('mouseleave', this.mouseleaveHandler);
        this.selector.addEventListener('mousemove', this.mousemoveHandler); // Click

        this.selector.addEventListener('click', this.clickHandler);
      }
    }
    /**
     * Detaches listeners from required events.
     */

  }, {
    key: "detachEvents",
    value: function detachEvents() {
      window.removeEventListener('resize', this.resizeHandler);
      this.selector.removeEventListener('touchstart', this.touchstartHandler);
      this.selector.removeEventListener('touchend', this.touchendHandler);
      this.selector.removeEventListener('touchmove', this.touchmoveHandler);
      this.selector.removeEventListener('mousedown', this.mousedownHandler);
      this.selector.removeEventListener('mouseup', this.mouseupHandler);
      this.selector.removeEventListener('mouseleave', this.mouseleaveHandler);
      this.selector.removeEventListener('mousemove', this.mousemoveHandler);
      this.selector.removeEventListener('click', this.clickHandler);
    }
    /**
     * Builds the markup and attaches listeners to required events.
     */

  }, {
    key: "init",
    value: function init() {
      this.attachEvents(); // hide everything out of selector's boundaries

      this.selector.style.overflow = 'hidden'; // rtl or ltr

      this.selector.style.direction = this.config.rtl ? 'rtl' : 'ltr'; // build a frame and slide to a currentSlide

      this.buildSliderFrame();
      this.config.onInit.call(this);
    }
    /**
     * Build a sliderFrame and slide to a current item.
     */

  }, {
    key: "buildSliderFrame",
    value: function buildSliderFrame() {
      var widthItem = this.selectorWidth / this.perPage;
      var itemsToBuild = this.config.loop ? this.innerElements.length + 2 * this.perPage : this.innerElements.length; // Create frame and apply styling

      this.sliderFrame = document.createElement('div');
      this.sliderFrame.style.width = "".concat(widthItem * itemsToBuild, "px");
      this.enableTransition();

      if (this.config.draggable) {
        this.selector.style.cursor = '-webkit-grab';
      } // Create a document fragment to put slides into it


      var docFragment = document.createDocumentFragment(); // Loop through the slides, add styling and add them to document fragment

      if (this.config.loop) {
        for (var i = this.innerElements.length - this.perPage; i < this.innerElements.length; i++) {
          var element = this.buildSliderFrameItem(this.innerElements[i].cloneNode(true));
          docFragment.appendChild(element);
        }
      }

      for (var _i = 0; _i < this.innerElements.length; _i++) {
        var _element = this.buildSliderFrameItem(this.innerElements[_i]);

        docFragment.appendChild(_element);
      }

      if (this.config.loop) {
        for (var _i2 = 0; _i2 < this.perPage; _i2++) {
          var _element2 = this.buildSliderFrameItem(this.innerElements[_i2].cloneNode(true));

          docFragment.appendChild(_element2);
        }
      } // Add fragment to the frame


      this.sliderFrame.appendChild(docFragment); // Clear selector (just in case something is there) and insert a frame

      this.selector.innerHTML = '';
      this.selector.appendChild(this.sliderFrame); // Go to currently active slide after initial build

      this.slideToCurrent();
    }
  }, {
    key: "buildSliderFrameItem",
    value: function buildSliderFrameItem(elm) {
      var elementContainer = document.createElement('div');
      elementContainer.style.cssFloat = this.config.rtl ? 'right' : 'left';
      elementContainer.style.float = this.config.rtl ? 'right' : 'left';
      elementContainer.style.width = "".concat(this.config.loop ? 100 / (this.innerElements.length + this.perPage * 2) : 100 / this.innerElements.length, "%");
      elementContainer.appendChild(elm);
      return elementContainer;
    }
    /**
     * Determinates slides number accordingly to clients viewport.
     */

  }, {
    key: "resolveSlidesNumber",
    value: function resolveSlidesNumber() {
      if (typeof this.config.perPage === 'number') {
        this.perPage = this.config.perPage;
      } else if (_typeof(this.config.perPage) === 'object') {
        this.perPage = 1;

        for (var viewport in this.config.perPage) {
          if (window.innerWidth >= viewport) {
            this.perPage = this.config.perPage[viewport];
          }
        }
      }
    }
    /**
     * Go to previous slide.
     * @param {number} [howManySlides=1] - How many items to slide backward.
     * @param {function} callback - Optional callback function.
     */

  }, {
    key: "prev",
    value: function prev() {
      var howManySlides = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var callback = arguments.length > 1 ? arguments[1] : undefined;

      // early return when there is nothing to slide
      if (this.innerElements.length <= this.perPage) {
        return;
      }

      var beforeChange = this.currentSlide;

      if (this.config.loop) {
        var isNewIndexClone = this.currentSlide - howManySlides < 0;

        if (isNewIndexClone) {
          this.disableTransition();
          var mirrorSlideIndex = this.currentSlide + this.innerElements.length;
          var mirrorSlideIndexOffset = this.perPage;
          var moveTo = mirrorSlideIndex + mirrorSlideIndexOffset;
          var offset = (this.config.rtl ? 1 : -1) * moveTo * (this.selectorWidth / this.perPage);
          var dragDistance = this.config.draggable ? this.drag.endX - this.drag.startX : 0;
          this.sliderFrame.style[this.transformProperty] = "translate3d(".concat(offset + dragDistance, "px, 0, 0)");
          this.currentSlide = mirrorSlideIndex - howManySlides;
        } else {
          this.currentSlide = this.currentSlide - howManySlides;
        }
      } else {
        this.currentSlide = Math.max(this.currentSlide - howManySlides, 0);
      }

      if (beforeChange !== this.currentSlide) {
        this.slideToCurrent(this.config.loop);
        this.config.onChange.call(this);

        if (callback) {
          callback.call(this);
        }
      }
    }
    /**
     * Go to next slide.
     * @param {number} [howManySlides=1] - How many items to slide forward.
     * @param {function} callback - Optional callback function.
     */

  }, {
    key: "next",
    value: function next() {
      var howManySlides = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 1;
      var callback = arguments.length > 1 ? arguments[1] : undefined;

      // early return when there is nothing to slide
      if (this.innerElements.length <= this.perPage) {
        return;
      }

      var beforeChange = this.currentSlide;

      if (this.config.loop) {
        var isNewIndexClone = this.currentSlide + howManySlides > this.innerElements.length - this.perPage;

        if (isNewIndexClone) {
          this.disableTransition();
          var mirrorSlideIndex = this.currentSlide - this.innerElements.length;
          var mirrorSlideIndexOffset = this.perPage;
          var moveTo = mirrorSlideIndex + mirrorSlideIndexOffset;
          var offset = (this.config.rtl ? 1 : -1) * moveTo * (this.selectorWidth / this.perPage);
          var dragDistance = this.config.draggable ? this.drag.endX - this.drag.startX : 0;
          this.sliderFrame.style[this.transformProperty] = "translate3d(".concat(offset + dragDistance, "px, 0, 0)");
          this.currentSlide = mirrorSlideIndex + howManySlides;
        } else {
          this.currentSlide = this.currentSlide + howManySlides;
        }
      } else {
        this.currentSlide = Math.min(this.currentSlide + howManySlides, this.innerElements.length - this.perPage);
      }

      if (beforeChange !== this.currentSlide) {
        this.slideToCurrent(this.config.loop);
        this.config.onChange.call(this);

        if (callback) {
          callback.call(this);
        }
      }
    }
    /**
     * Disable transition on sliderFrame.
     */

  }, {
    key: "disableTransition",
    value: function disableTransition() {
      this.sliderFrame.style.webkitTransition = "all 0ms ".concat(this.config.easing);
      this.sliderFrame.style.transition = "all 0ms ".concat(this.config.easing);
    }
    /**
     * Enable transition on sliderFrame.
     */

  }, {
    key: "enableTransition",
    value: function enableTransition() {
      this.sliderFrame.style.webkitTransition = "all ".concat(this.config.duration, "ms ").concat(this.config.easing);
      this.sliderFrame.style.transition = "all ".concat(this.config.duration, "ms ").concat(this.config.easing);
    }
    /**
     * Go to slide with particular index
     * @param {number} index - Item index to slide to.
     * @param {function} callback - Optional callback function.
     */

  }, {
    key: "goTo",
    value: function goTo(index, callback) {
      if (this.innerElements.length <= this.perPage) {
        return;
      }

      var beforeChange = this.currentSlide;
      this.currentSlide = this.config.loop ? index % this.innerElements.length : Math.min(Math.max(index, 0), this.innerElements.length - this.perPage);

      if (beforeChange !== this.currentSlide) {
        this.slideToCurrent();
        this.config.onChange.call(this);

        if (callback) {
          callback.call(this);
        }
      }
    }
    /**
     * Moves sliders frame to position of currently active slide
     */

  }, {
    key: "slideToCurrent",
    value: function slideToCurrent(enableTransition) {
      var _this2 = this;

      var currentSlide = this.config.loop ? this.currentSlide + this.perPage : this.currentSlide;
      var offset = (this.config.rtl ? 1 : -1) * currentSlide * (this.selectorWidth / this.perPage);

      if (enableTransition) {
        // This one is tricky, I know but this is a perfect explanation:
        // https://youtu.be/cCOL7MC4Pl0
        requestAnimationFrame(function () {
          requestAnimationFrame(function () {
            _this2.enableTransition();

            _this2.sliderFrame.style[_this2.transformProperty] = "translate3d(".concat(offset, "px, 0, 0)");
          });
        });
      } else {
        this.sliderFrame.style[this.transformProperty] = "translate3d(".concat(offset, "px, 0, 0)");
      }
    }
    /**
     * Recalculate drag /swipe event and reposition the frame of a slider
     */

  }, {
    key: "updateAfterDrag",
    value: function updateAfterDrag() {
      var movement = (this.config.rtl ? -1 : 1) * (this.drag.endX - this.drag.startX);
      var movementDistance = Math.abs(movement);
      var howManySliderToSlide = this.config.multipleDrag ? Math.ceil(movementDistance / (this.selectorWidth / this.perPage)) : 1;
      var slideToNegativeClone = movement > 0 && this.currentSlide - howManySliderToSlide < 0;
      var slideToPositiveClone = movement < 0 && this.currentSlide + howManySliderToSlide > this.innerElements.length - this.perPage;

      if (movement > 0 && movementDistance > this.config.threshold && this.innerElements.length > this.perPage) {
        this.prev(howManySliderToSlide);
      } else if (movement < 0 && movementDistance > this.config.threshold && this.innerElements.length > this.perPage) {
        this.next(howManySliderToSlide);
      }

      this.slideToCurrent(slideToNegativeClone || slideToPositiveClone);
    }
    /**
     * When window resizes, resize slider components as well
     */

  }, {
    key: "resizeHandler",
    value: function resizeHandler() {
      // update perPage number dependable of user value
      this.resolveSlidesNumber(); // relcalculate currentSlide
      // prevent hiding items when browser width increases

      if (this.currentSlide + this.perPage > this.innerElements.length) {
        this.currentSlide = this.innerElements.length <= this.perPage ? 0 : this.innerElements.length - this.perPage;
      }

      this.selectorWidth = this.selector.offsetWidth;
      this.buildSliderFrame();
    }
    /**
     * Clear drag after touchend and mouseup event
     */

  }, {
    key: "clearDrag",
    value: function clearDrag() {
      this.drag = {
        startX: 0,
        endX: 0,
        startY: 0,
        letItGo: null,
        preventClick: this.drag.preventClick
      };
    }
    /**
     * touchstart event handler
     */

  }, {
    key: "touchstartHandler",
    value: function touchstartHandler(e) {
      // Prevent dragging / swiping on inputs, selects and textareas
      var ignoreSiema = ['TEXTAREA', 'OPTION', 'INPUT', 'SELECT'].indexOf(e.target.nodeName) !== -1;

      if (ignoreSiema) {
        return;
      }

      e.stopPropagation();
      this.pointerDown = true;
      this.drag.startX = e.touches[0].pageX;
      this.drag.startY = e.touches[0].pageY;
    }
    /**
     * touchend event handler
     */

  }, {
    key: "touchendHandler",
    value: function touchendHandler(e) {
      e.stopPropagation();
      this.pointerDown = false;
      this.enableTransition();

      if (this.drag.endX) {
        this.updateAfterDrag();
      }

      this.clearDrag();
    }
    /**
     * touchmove event handler
     */

  }, {
    key: "touchmoveHandler",
    value: function touchmoveHandler(e) {
      e.stopPropagation();

      if (this.drag.letItGo === null) {
        this.drag.letItGo = Math.abs(this.drag.startY - e.touches[0].pageY) < Math.abs(this.drag.startX - e.touches[0].pageX);
      }

      if (this.pointerDown && this.drag.letItGo) {
        e.preventDefault();
        this.drag.endX = e.touches[0].pageX;
        this.sliderFrame.style.webkitTransition = "all 0ms ".concat(this.config.easing);
        this.sliderFrame.style.transition = "all 0ms ".concat(this.config.easing);
        var currentSlide = this.config.loop ? this.currentSlide + this.perPage : this.currentSlide;
        var currentOffset = currentSlide * (this.selectorWidth / this.perPage);
        var dragOffset = this.drag.endX - this.drag.startX;
        var offset = this.config.rtl ? currentOffset + dragOffset : currentOffset - dragOffset;
        this.sliderFrame.style[this.transformProperty] = "translate3d(".concat((this.config.rtl ? 1 : -1) * offset, "px, 0, 0)");
      }
    }
    /**
     * mousedown event handler
     */

  }, {
    key: "mousedownHandler",
    value: function mousedownHandler(e) {
      // Prevent dragging / swiping on inputs, selects and textareas
      var ignoreSiema = ['TEXTAREA', 'OPTION', 'INPUT', 'SELECT'].indexOf(e.target.nodeName) !== -1;

      if (ignoreSiema) {
        return;
      }

      e.preventDefault();
      e.stopPropagation();
      this.pointerDown = true;
      this.drag.startX = e.pageX;
    }
    /**
     * mouseup event handler
     */

  }, {
    key: "mouseupHandler",
    value: function mouseupHandler(e) {
      e.stopPropagation();
      this.pointerDown = false;
      this.selector.style.cursor = '-webkit-grab';
      this.enableTransition();

      if (this.drag.endX) {
        this.updateAfterDrag();
      }

      this.clearDrag();
    }
    /**
     * mousemove event handler
     */

  }, {
    key: "mousemoveHandler",
    value: function mousemoveHandler(e) {
      e.preventDefault();

      if (this.pointerDown) {
        // if dragged element is a link
        // mark preventClick prop as a true
        // to detemine about browser redirection later on
        if (e.target.nodeName === 'A') {
          this.drag.preventClick = true;
        }

        this.drag.endX = e.pageX;
        this.selector.style.cursor = '-webkit-grabbing';
        this.sliderFrame.style.webkitTransition = "all 0ms ".concat(this.config.easing);
        this.sliderFrame.style.transition = "all 0ms ".concat(this.config.easing);
        var currentSlide = this.config.loop ? this.currentSlide + this.perPage : this.currentSlide;
        var currentOffset = currentSlide * (this.selectorWidth / this.perPage);
        var dragOffset = this.drag.endX - this.drag.startX;
        var offset = this.config.rtl ? currentOffset + dragOffset : currentOffset - dragOffset;
        this.sliderFrame.style[this.transformProperty] = "translate3d(".concat((this.config.rtl ? 1 : -1) * offset, "px, 0, 0)");
      }
    }
    /**
     * mouseleave event handler
     */

  }, {
    key: "mouseleaveHandler",
    value: function mouseleaveHandler(e) {
      if (this.pointerDown) {
        this.pointerDown = false;
        this.selector.style.cursor = '-webkit-grab';
        this.drag.endX = e.pageX;
        this.drag.preventClick = false;
        this.enableTransition();
        this.updateAfterDrag();
        this.clearDrag();
      }
    }
    /**
     * click event handler
     */

  }, {
    key: "clickHandler",
    value: function clickHandler(e) {
      // if the dragged element is a link
      // prevent browsers from folowing the link
      if (this.drag.preventClick) {
        e.preventDefault();
      }

      this.drag.preventClick = false;
    }
    /**
     * Remove item from carousel.
     * @param {number} index - Item index to remove.
     * @param {function} callback - Optional callback to call after remove.
     */

  }, {
    key: "remove",
    value: function remove(index, callback) {
      if (index < 0 || index >= this.innerElements.length) {
        throw new Error('Item to remove doesn\'t exist 😭');
      } // Shift sliderFrame back by one item when:
      // 1. Item with lower index than currenSlide is removed.
      // 2. Last item is removed.


      var lowerIndex = index < this.currentSlide;
      var lastItem = this.currentSlide + this.perPage - 1 === index;

      if (lowerIndex || lastItem) {
        this.currentSlide--;
      }

      this.innerElements.splice(index, 1); // build a frame and slide to a currentSlide

      this.buildSliderFrame();

      if (callback) {
        callback.call(this);
      }
    }
    /**
     * Insert item to carousel at particular index.
     * @param {HTMLElement} item - Item to insert.
     * @param {number} index - Index of new new item insertion.
     * @param {function} callback - Optional callback to call after insert.
     */

  }, {
    key: "insert",
    value: function insert(item, index, callback) {
      if (index < 0 || index > this.innerElements.length + 1) {
        throw new Error('Unable to inset it at this index 😭');
      }

      if (this.innerElements.indexOf(item) !== -1) {
        throw new Error('The same item in a carousel? Really? Nope 😭');
      } // Avoid shifting content


      var shouldItShift = index <= this.currentSlide > 0 && this.innerElements.length;
      this.currentSlide = shouldItShift ? this.currentSlide + 1 : this.currentSlide;
      this.innerElements.splice(index, 0, item); // build a frame and slide to a currentSlide

      this.buildSliderFrame();

      if (callback) {
        callback.call(this);
      }
    }
    /**
     * Prepernd item to carousel.
     * @param {HTMLElement} item - Item to prepend.
     * @param {function} callback - Optional callback to call after prepend.
     */

  }, {
    key: "prepend",
    value: function prepend(item, callback) {
      this.insert(item, 0);

      if (callback) {
        callback.call(this);
      }
    }
    /**
     * Append item to carousel.
     * @param {HTMLElement} item - Item to append.
     * @param {function} callback - Optional callback to call after append.
     */

  }, {
    key: "append",
    value: function append(item, callback) {
      this.insert(item, this.innerElements.length + 1);

      if (callback) {
        callback.call(this);
      }
    }
    /**
     * Removes listeners and optionally restores to initial markup
     * @param {boolean} restoreMarkup - Determinants about restoring an initial markup.
     * @param {function} callback - Optional callback function.
     */

  }, {
    key: "destroy",
    value: function destroy() {
      var restoreMarkup = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var callback = arguments.length > 1 ? arguments[1] : undefined;
      this.detachEvents();
      this.selector.style.cursor = 'auto';

      if (restoreMarkup) {
        var slides = document.createDocumentFragment();

        for (var i = 0; i < this.innerElements.length; i++) {
          slides.appendChild(this.innerElements[i]);
        }

        this.selector.innerHTML = '';
        this.selector.appendChild(slides);
        this.selector.removeAttribute('style');
      }

      if (callback) {
        callback.call(this);
      }
    }
  }], [{
    key: "mergeSettings",
    value: function mergeSettings(options) {
      var settings = {
        selector: '.siema',
        duration: 200,
        easing: 'ease-out',
        perPage: 1,
        startIndex: 0,
        draggable: true,
        multipleDrag: true,
        threshold: 20,
        loop: false,
        rtl: false,
        onInit: function onInit() {},
        onChange: function onChange() {}
      };
      var userSttings = options;

      for (var attrname in userSttings) {
        settings[attrname] = userSttings[attrname];
      }

      return settings;
    }
    /**
     * Determine if browser supports unprefixed transform property.
     * Google Chrome since version 26 supports prefix-less transform
     * @returns {string} - Transform property supported by client.
     */

  }, {
    key: "webkitOrNot",
    value: function webkitOrNot() {
      var style = document.documentElement.style;

      if (typeof style.transform === 'string') {
        return 'transform';
      }

      return 'WebkitTransform';
    }
  }]);

  return Siema;
}();

var singleArgs = {
  selector: '.carousel',
  duration: 200,
  easing: 'ease-out',
  perPage: 1,
  startIndex: 0,
  draggable: true,
  multipleDrag: true,
  threshold: 20,
  loop: true,
  rtl: false,
  pagination: true,
  onInit: function onInit() {},
  onChange: fChange
};
var fourArgs = {
  selector: '.images-carousel',
  duration: 200,
  easing: 'ease-out',
  perPage: {
    320: 1,
    500: 2,
    860: 3,
    // 2 items for viewport wider than 800px
    1240: 4 // 3 items for viewport wider than 1240px

  },
  startIndex: 0,
  draggable: true,
  multipleDrag: true,
  threshold: 20,
  loop: true,
  rtl: false,
  pagination: true,
  onInit: function onInit() {},
  onChange: fChange
};

var SetUpCarousel = function SetUpCarousel() {
  var pagination = false;
  var mySiema = new Siema(singleArgs);
  var prev = mySiema.selector.parentNode.querySelector('.prev');
  var next = mySiema.selector.parentNode.querySelector('.next');
  if (prev) prev.addEventListener('click', function () {
    return mySiema.prev();
  });
  if (next) next.addEventListener('click', function () {
    return mySiema.next();
  });
  if (singleArgs['pagination'] == true) pagination = true; // Trigger pagination creator

  if (pagination) {
    mySiema.addPagination();
  }
};

var SetUpImagesCarousel = function SetUpImagesCarousel() {
  var mySiema = new Siema(fourArgs);
  var prev = mySiema.selector.parentNode.querySelector('.prev');
  var next = mySiema.selector.parentNode.querySelector('.next');
  if (prev) prev.addEventListener('click', function () {
    return mySiema.prev();
  });
  if (next) next.addEventListener('click', function () {
    return mySiema.next();
  }); // Trigger pagination creator
  // Trigger pagination on window resize
};

Siema.prototype.addPagination = function () {
  var _this = this;

  //console.log(this);
  var btns = document.querySelectorAll('.carousel-pagination button');

  var _loop = function _loop(i) {
    var btn = btns[i];
    btn.addEventListener('click', function () {
      return _this.goTo(i);
    });
    if (i === 0) btn.classList.add('active');
  };

  for (var i = 0; i < btns.length; i++) {
    _loop(i);
  }
};

function fChange() {
  var mySiema = this;
  var navItems = mySiema.selector.nextElementSibling.getElementsByTagName("button");

  if (navItems) {
    var currentButton = navItems[mySiema.currentSlide];

    for (var i = 0; i < navItems.length; i++) {
      if (navItems[i].classList.contains('active')) navItems[i].classList.remove('active');
    }

    currentButton.classList.add('active');
  }
}

var SliderList;
var Sliders;
var animating = false;
var SliderTimeout;
var animTime = 700;

var SetUpSlider = function SetUpSlider(section) {
  SliderList = document.getElementsByClassName(section);

  var _loop = function _loop(i) {
    var button = document.createElement('button'); //add content to modal

    SliderList[i].appendChild(button);
    Sliders = SliderList[i].getElementsByClassName('slider-item');
    var Slider = SliderList[i];

    button.onclick = function (event) {
      return animate$1(event, Slider);
    };
  };

  for (var i = 0; i < SliderList.length; i++) {
    _loop(i);
  }

  setWidths();
  window.addEventListener('resize', setWidths, false);
}; ///Ticker


var fupdate = function fupdate() {
  if (animating) {
    for (var _i = 0; _i < SliderList.length; _i++) {
      for (var _i = 0; _i < Sliders.length; _i++) {
        var element = Sliders[_i];
        checkpos(element);
      }
    }
  }

  ticker = requestAnimationFrame(fupdate);
};

var ticker = requestAnimationFrame(fupdate);

var setWidths = function setWidths() {
  if (animating === false) {
    for (var _i2 = 0; _i2 < SliderList.length; _i2++) {
      for (var _i2 = 0; _i2 < Sliders.length; _i2++) {
        var element = Sliders[_i2];
        var positionInfo = element.getBoundingClientRect();
        var width = positionInfo.width.toFixed(2);
        var widthpx = width + 'px';
        checkpos(element);
        element.style.width = widthpx;
        element.setAttribute('data-width', width);
      }
    }
  }
};

var checkpos = function checkpos(element) {
  var positionInfo = element.getBoundingClientRect();

  if (positionInfo.right > document.body.offsetWidth) {
    if (!element.classList.contains('off-screen')) element.classList.add('off-screen');
  } else {
    if (element.classList.contains('off-screen')) element.classList.remove('off-screen');
  }
};

var animate$1 = function animate(event, currentSlider) {
  if (!animating) {
    //
    currentSlider.classList.add('animating');

    var _Sliders = currentSlider.getElementsByClassName('slider-item');

    var slider = _Sliders[0];
    var inners;
    var type;

    if (slider.classList.contains('video')) {
      inners = slider.getElementsByClassName('video-holder');
      type = 'video';
    }

    if (slider.classList.contains('image')) {
      inners = slider.getElementsByTagName('img');
      type = 'image';
    } ///select first item


    var inner = inners[0];
    inner.style.transform = 'translateX(-' + slider.getAttribute("data-width") + 'px)';
    slider.style.width = 0; //

    animating = true; //

    duplicateNode(currentSlider, slider, type);
    SliderTimeout = setTimeout(function () {
      endAnimate(currentSlider, slider);
    }, animTime);
    var pagination = document.querySelectorAll('.slider-pagination button');
    var activateNext = 0;

    for (var i = 0; i < pagination.length; i++) {
      var btn = pagination[i];

      if (btn.classList.contains('active')) {
        btn.classList.remove('active');
        activateNext = i + 1;
      }
    }

    if (activateNext > pagination.length - 1) activateNext = 0;
    pagination[activateNext].classList.add('active');
  }
};

var duplicateNode = function duplicateNode(currentSlider, slider, type) {
  //img.removeAttribute('style');
  var dupNode = slider.cloneNode(true);
  var w = dupNode.getAttribute('data-width') + 'px';
  dupNode.removeAttribute('style');
  var inners;

  if (type == 'video') {
    inners = dupNode.getElementsByClassName('video-holder'); ////

    var id = inners[0].getAttribute('id');
    var id1 = id.substring(0, id.length - 1);
    var id2 = parseInt(id.substring(id.length - 1, id.length)) + 1;
    var newID = id1 + id2;
    inners[0].setAttribute('id', newID);
  }

  if (type == 'image') inners = dupNode.getElementsByTagName('img');
  inners[0].removeAttribute('style');
  dupNode.style.width = w;
  var newNode = currentSlider.appendChild(dupNode);

  if (type == 'video') {
    var $vid = newNode.getElementsByClassName('video-holder');
    fInitVid($vid[0]);
  }
};

var endAnimate = function endAnimate(currentSlider, image) {
  ///get top twitte
  animating = false;
  currentSlider.classList.remove('animating');
  image.remove();
  setWidths();
};

var mainHouses;

var SetUpHouses = function SetUpHouses(section) {
  var container = document.getElementById(section);
  var houses = container.querySelectorAll("div[data-type='house']");
  mainHouses = document.querySelectorAll("div[data-type='main-house']");

  var _loop = function _loop(i) {
    var house = houses[i];
    var id = house.getAttribute("id");

    house.onclick = function (event) {
      return animate$2(event, id);
    };
  };

  for (var i = 0; i < houses.length; i++) {
    _loop(i);
  }

  mainHouses[0].style.display = 'block';
  mainHouses[0].classList.add('fade-in');
};

var animate$2 = function animate(event, id) {
  for (var i = 0; i < mainHouses.length; i++) {
    var house = mainHouses[i];
    if (house.classList.contains('fade-in')) house.classList.remove('fade-in');
    house.style.display = 'none';
  }

  var mainHouse = document.getElementById('house-' + id);
  mainHouse.style.display = 'block';
  mainHouse.classList.add('fade-in');
};

var SetUpIll = function SetUpIll(section) {
  var containers = document.getElementsByClassName(section);

  var _loop = function _loop(i) {
    var container = containers[i];
    var tabs = container.querySelectorAll('.tabs li'); //items = container.querySelectorAll('.illy--item');

    var _loop2 = function _loop2(_i) {
      var tab = tabs[_i];
      var n = tab.getAttribute('data-tab'); //console.log('n: '+n);

      tab.onclick = function (event) {
        return animate$3(event, n, container);
      };
    };

    for (var _i = 0; _i < tabs.length; _i++) {
      _loop2(_i);
    } ///find the buttons


    var buttonsLeft = container.querySelectorAll('.btn-left');
    var buttonsRight = container.querySelectorAll('.btn-right');
    var buttonLeft = void 0;
    var buttonRight = void 0;

    if (buttonsLeft.length > 0) {
      buttonLeft = buttonsLeft[0];

      buttonLeft.onclick = function () {
        var n = getn(container, tabs, 'minus');
        console.log(n);
        animate$3(event, n, container);
      };
    }

    if (buttonsRight.length > 0) {
      buttonRight = buttonsRight[0];

      buttonRight.onclick = function () {
        var n = getn(container, tabs, 'plus');
        console.log(n);
        animate$3(event, n, container);
      };
    }
  };

  for (var i = 0; i < containers.length; i++) {
    _loop(i);
  } //	 mainHouses[0].style.display = 'block';
  //	 mainHouses[0].classList.add('fade-in');

};

var getn = function getn(container, tabs, action) {
  var current = container.querySelectorAll('.illy--item.active');
  var n = Number(current[0].getAttribute('data-item'));
  if (action == 'minus') n = n - 1;
  if (action == 'plus') n = n + 1;
  if (n < 0) n = tabs.length - 1;
  if (n >= tabs.length) n = 0;
  return n;
};

var animate$3 = function animate(event, id, $container) {
  var active = $container.querySelectorAll('.illy--item.active'); //let tabActive = event.currentTarget;

  var tabActive = $container.querySelectorAll('li[data-tab="' + id + '"');
  var tab = $container.querySelectorAll('.tabs .active');
  var item = $container.querySelectorAll('div[data-item="' + id + '"]'); //console.log(tabActive);

  active[0].classList.remove('active');
  item[0].classList.add('active');
  tab[0].classList.remove('active');
  tabActive[0].classList.add('active');
};

/*!
 * smooth-scroll v16.0.3
 * Animate scrolling to anchor links
 * (c) 2019 Chris Ferdinandi
 * MIT License
 * http://github.com/cferdinandi/smooth-scroll
 */
if (window.Element && !Element.prototype.closest) {
  Element.prototype.closest = function (s) {
    var matches = (this.document || this.ownerDocument).querySelectorAll(s),
        i,
        el = this;

    do {
      i = matches.length;

      while (--i >= 0 && matches.item(i) !== el) {}
    } while (i < 0 && (el = el.parentElement));

    return el;
  };
}

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define([], function () {
      return factory(root);
    });
  } else if ((typeof exports === "undefined" ? "undefined" : _typeof(exports)) === 'object') {
    module.exports = factory(root);
  } else {
    root.SmoothScroll = factory(root);
  }
})(typeof global !== 'undefined' ? global : typeof window !== 'undefined' ? window : undefined, function (window) {
  // Default settings
  //

  var defaults = {
    // Selectors
    ignore: '[data-scroll-ignore]',
    header: null,
    topOnEmptyHash: true,
    // Speed & Duration
    speed: 500,
    speedAsDuration: false,
    durationMax: null,
    durationMin: null,
    clip: true,
    offset: 0,
    // Easing
    easing: 'easeInOutCubic',
    customEasing: null,
    // History
    updateURL: true,
    popstate: true,
    // Custom Events
    emitEvents: true
  }; //
  // Utility Methods
  //

  /**
   * Check if browser supports required methods
   * @return {Boolean} Returns true if all required methods are supported
   */

  var supports = function supports() {
    return 'querySelector' in document && 'addEventListener' in window && 'requestAnimationFrame' in window && 'closest' in window.Element.prototype;
  };
  /**
   * Merge two or more objects together.
   * @param   {Object}   objects  The objects to merge together
   * @returns {Object}            Merged values of defaults and options
   */


  var extend = function extend() {
    var merged = {};
    Array.prototype.forEach.call(arguments, function (obj) {
      for (var key in obj) {
        if (!obj.hasOwnProperty(key)) return;
        merged[key] = obj[key];
      }
    });
    return merged;
  };
  /**
   * Check to see if user prefers reduced motion
   * @param  {Object} settings Script settings
   */


  var reduceMotion = function reduceMotion(settings) {
    if ('matchMedia' in window && window.matchMedia('(prefers-reduced-motion)').matches) {
      return true;
    }

    return false;
  };
  /**
   * Get the height of an element.
   * @param  {Node} elem The element to get the height of
   * @return {Number}    The element's height in pixels
   */


  var getHeight = function getHeight(elem) {
    return parseInt(window.getComputedStyle(elem).height, 10);
  };
  /**
   * Escape special characters for use with querySelector
   * @author Mathias Bynens
   * @link https://github.com/mathiasbynens/CSS.escape
   * @param {String} id The anchor ID to escape
   */


  var escapeCharacters = function escapeCharacters(id) {
    // Remove leading hash
    if (id.charAt(0) === '#') {
      id = id.substr(1);
    }

    var string = String(id);
    var length = string.length;
    var index = -1;
    var codeUnit;
    var result = '';
    var firstCodeUnit = string.charCodeAt(0);

    while (++index < length) {
      codeUnit = string.charCodeAt(index); // Note: there’s no need to special-case astral symbols, surrogate
      // pairs, or lone surrogates.
      // If the character is NULL (U+0000), then throw an
      // `InvalidCharacterError` exception and terminate these steps.

      if (codeUnit === 0x0000) {
        throw new InvalidCharacterError('Invalid character: the input contains U+0000.');
      }

      if ( // If the character is in the range [\1-\1F] (U+0001 to U+001F) or is
      // U+007F, […]
      codeUnit >= 0x0001 && codeUnit <= 0x001F || codeUnit == 0x007F || // If the character is the first character and is in the range [0-9]
      // (U+0030 to U+0039), […]
      index === 0 && codeUnit >= 0x0030 && codeUnit <= 0x0039 || // If the character is the second character and is in the range [0-9]
      // (U+0030 to U+0039) and the first character is a `-` (U+002D), […]
      index === 1 && codeUnit >= 0x0030 && codeUnit <= 0x0039 && firstCodeUnit === 0x002D) {
        // http://dev.w3.org/csswg/cssom/#escape-a-character-as-code-point
        result += '\\' + codeUnit.toString(16) + ' ';
        continue;
      } // If the character is not handled by one of the above rules and is
      // greater than or equal to U+0080, is `-` (U+002D) or `_` (U+005F), or
      // is in one of the ranges [0-9] (U+0030 to U+0039), [A-Z] (U+0041 to
      // U+005A), or [a-z] (U+0061 to U+007A), […]


      if (codeUnit >= 0x0080 || codeUnit === 0x002D || codeUnit === 0x005F || codeUnit >= 0x0030 && codeUnit <= 0x0039 || codeUnit >= 0x0041 && codeUnit <= 0x005A || codeUnit >= 0x0061 && codeUnit <= 0x007A) {
        // the character itself
        result += string.charAt(index);
        continue;
      } // Otherwise, the escaped character.
      // http://dev.w3.org/csswg/cssom/#escape-a-character


      result += '\\' + string.charAt(index);
    } // Return sanitized hash


    return '#' + result;
  };
  /**
   * Calculate the easing pattern
   * @link https://gist.github.com/gre/1650294
   * @param {String} type Easing pattern
   * @param {Number} time Time animation should take to complete
   * @returns {Number}
   */


  var easingPattern = function easingPattern(settings, time) {
    var pattern; // Default Easing Patterns

    if (settings.easing === 'easeInQuad') pattern = time * time; // accelerating from zero velocity

    if (settings.easing === 'easeOutQuad') pattern = time * (2 - time); // decelerating to zero velocity

    if (settings.easing === 'easeInOutQuad') pattern = time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time; // acceleration until halfway, then deceleration

    if (settings.easing === 'easeInCubic') pattern = time * time * time; // accelerating from zero velocity

    if (settings.easing === 'easeOutCubic') pattern = --time * time * time + 1; // decelerating to zero velocity

    if (settings.easing === 'easeInOutCubic') pattern = time < 0.5 ? 4 * time * time * time : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1; // acceleration until halfway, then deceleration

    if (settings.easing === 'easeInQuart') pattern = time * time * time * time; // accelerating from zero velocity

    if (settings.easing === 'easeOutQuart') pattern = 1 - --time * time * time * time; // decelerating to zero velocity

    if (settings.easing === 'easeInOutQuart') pattern = time < 0.5 ? 8 * time * time * time * time : 1 - 8 * --time * time * time * time; // acceleration until halfway, then deceleration

    if (settings.easing === 'easeInQuint') pattern = time * time * time * time * time; // accelerating from zero velocity

    if (settings.easing === 'easeOutQuint') pattern = 1 + --time * time * time * time * time; // decelerating to zero velocity

    if (settings.easing === 'easeInOutQuint') pattern = time < 0.5 ? 16 * time * time * time * time * time : 1 + 16 * --time * time * time * time * time; // acceleration until halfway, then deceleration
    // Custom Easing Patterns

    if (!!settings.customEasing) pattern = settings.customEasing(time);
    return pattern || time; // no easing, no acceleration
  };
  /**
   * Determine the document's height
   * @returns {Number}
   */


  var getDocumentHeight = function getDocumentHeight() {
    return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight, document.body.offsetHeight, document.documentElement.offsetHeight, document.body.clientHeight, document.documentElement.clientHeight);
  };
  /**
   * Calculate how far to scroll
   * Clip support added by robjtede - https://github.com/cferdinandi/smooth-scroll/issues/405
   * @param {Element} anchor       The anchor element to scroll to
   * @param {Number}  headerHeight Height of a fixed header, if any
   * @param {Number}  offset       Number of pixels by which to offset scroll
   * @param {Boolean} clip         If true, adjust scroll distance to prevent abrupt stops near the bottom of the page
   * @returns {Number}
   */


  var getEndLocation = function getEndLocation(anchor, headerHeight, offset, clip) {
    var location = 0;

    if (anchor.offsetParent) {
      do {
        location += anchor.offsetTop;
        anchor = anchor.offsetParent;
      } while (anchor);
    }

    location = Math.max(location - headerHeight - offset, 0);

    if (clip) {
      location = Math.min(location, getDocumentHeight() - window.innerHeight);
    }

    return location;
  };
  /**
   * Get the height of the fixed header
   * @param  {Node}   header The header
   * @return {Number}        The height of the header
   */


  var getHeaderHeight = function getHeaderHeight(header) {
    return !header ? 0 : getHeight(header) + header.offsetTop;
  };
  /**
   * Calculate the speed to use for the animation
   * @param  {Number} distance The distance to travel
   * @param  {Object} settings The plugin settings
   * @return {Number}          How fast to animate
   */


  var getSpeed = function getSpeed(distance, settings) {
    var speed = settings.speedAsDuration ? settings.speed : Math.abs(distance / 1000 * settings.speed);
    if (settings.durationMax && speed > settings.durationMax) return settings.durationMax;
    if (settings.durationMin && speed < settings.durationMin) return settings.durationMin;
    return parseInt(speed, 10);
  };

  var setHistory = function setHistory(options) {
    // Make sure this should run
    if (!history.replaceState || !options.updateURL || history.state) return; // Get the hash to use

    var hash = window.location.hash;
    hash = hash ? hash : ''; // Set a default history

    history.replaceState({
      smoothScroll: JSON.stringify(options),
      anchor: hash ? hash : window.pageYOffset
    }, document.title, hash ? hash : window.location.href);
  };
  /**
   * Update the URL
   * @param  {Node}    anchor  The anchor that was scrolled to
   * @param  {Boolean} isNum   If true, anchor is a number
   * @param  {Object}  options Settings for Smooth Scroll
   */


  var updateURL = function updateURL(anchor, isNum, options) {
    // Bail if the anchor is a number
    if (isNum) return; // Verify that pushState is supported and the updateURL option is enabled

    if (!history.pushState || !options.updateURL) return; // Update URL

    history.pushState({
      smoothScroll: JSON.stringify(options),
      anchor: anchor.id
    }, document.title, anchor === document.documentElement ? '#top' : '#' + anchor.id);
  };
  /**
   * Bring the anchored element into focus
   * @param {Node}     anchor      The anchor element
   * @param {Number}   endLocation The end location to scroll to
   * @param {Boolean}  isNum       If true, scroll is to a position rather than an element
   */


  var adjustFocus = function adjustFocus(anchor, endLocation, isNum) {
    // Is scrolling to top of page, blur
    if (anchor === 0) {
      document.body.focus();
    } // Don't run if scrolling to a number on the page


    if (isNum) return; // Otherwise, bring anchor element into focus

    anchor.focus();

    if (document.activeElement !== anchor) {
      anchor.setAttribute('tabindex', '-1');
      anchor.focus();
      anchor.style.outline = 'none';
    }

    window.scrollTo(0, endLocation);
  };
  /**
   * Emit a custom event
   * @param  {String} type    The event type
   * @param  {Object} options The settings object
   * @param  {Node}   anchor  The anchor element
   * @param  {Node}   toggle  The toggle element
   */


  var emitEvent = function emitEvent(type, options, anchor, toggle) {
    if (!options.emitEvents || typeof window.CustomEvent !== 'function') return;
    var event = new CustomEvent(type, {
      bubbles: true,
      detail: {
        anchor: anchor,
        toggle: toggle
      }
    });
    document.dispatchEvent(event);
  }; //
  // SmoothScroll Constructor
  //


  var SmoothScroll = function SmoothScroll(selector, options) {
    //
    // Variables
    //
    var smoothScroll = {}; // Object for public APIs

    var settings, toggle, fixedHeader, animationInterval; //
    // Methods
    //

    /**
     * Cancel a scroll-in-progress
     */

    smoothScroll.cancelScroll = function (noEvent) {
      cancelAnimationFrame(animationInterval);
      animationInterval = null;
      if (noEvent) return;
      emitEvent('scrollCancel', settings);
    };
    /**
     * Start/stop the scrolling animation
     * @param {Node|Number} anchor  The element or position to scroll to
     * @param {Element}     toggle  The element that toggled the scroll event
     * @param {Object}      options
     */


    smoothScroll.animateScroll = function (anchor, toggle, options) {
      // Cancel any in progress scrolls
      smoothScroll.cancelScroll(); // Local settings

      var _settings = extend(settings || defaults, options || {}); // Merge user options with defaults
      // Selectors and variables


      var isNum = Object.prototype.toString.call(anchor) === '[object Number]' ? true : false;
      var anchorElem = isNum || !anchor.tagName ? null : anchor;
      if (!isNum && !anchorElem) return;
      var startLocation = window.pageYOffset; // Current location on the page

      if (_settings.header && !fixedHeader) {
        // Get the fixed header if not already set
        fixedHeader = document.querySelector(_settings.header);
      }

      var headerHeight = getHeaderHeight(fixedHeader);
      var endLocation = isNum ? anchor : getEndLocation(anchorElem, headerHeight, parseInt(typeof _settings.offset === 'function' ? _settings.offset(anchor, toggle) : _settings.offset, 10), _settings.clip); // Location to scroll to

      var distance = endLocation - startLocation; // distance to travel

      var documentHeight = getDocumentHeight();
      var timeLapsed = 0;
      var speed = getSpeed(distance, _settings);
      var start, percentage, position;
      /**
       * Stop the scroll animation when it reaches its target (or the bottom/top of page)
       * @param {Number} position Current position on the page
       * @param {Number} endLocation Scroll to location
       * @param {Number} animationInterval How much to scroll on this loop
       */

      var stopAnimateScroll = function stopAnimateScroll(position, endLocation) {
        // Get the current location
        var currentLocation = window.pageYOffset; // Check if the end location has been reached yet (or we've hit the end of the document)

        if (position == endLocation || currentLocation == endLocation || (startLocation < endLocation && window.innerHeight + currentLocation) >= documentHeight) {
          // Clear the animation timer
          smoothScroll.cancelScroll(true); // Bring the anchored element into focus

          adjustFocus(anchor, endLocation, isNum); // Emit a custom event

          emitEvent('scrollStop', _settings, anchor, toggle); // Reset start

          start = null;
          animationInterval = null;
          return true;
        }
      };
      /**
       * Loop scrolling animation
       */


      var loopAnimateScroll = function loopAnimateScroll(timestamp) {
        if (!start) {
          start = timestamp;
        }

        timeLapsed += timestamp - start;
        percentage = speed === 0 ? 0 : timeLapsed / speed;
        percentage = percentage > 1 ? 1 : percentage;
        position = startLocation + distance * easingPattern(_settings, percentage);
        window.scrollTo(0, Math.floor(position));

        if (!stopAnimateScroll(position, endLocation)) {
          animationInterval = window.requestAnimationFrame(loopAnimateScroll);
          start = timestamp;
        }
      };
      /**
       * Reset position to fix weird iOS bug
       * @link https://github.com/cferdinandi/smooth-scroll/issues/45
       */


      if (window.pageYOffset === 0) {
        window.scrollTo(0, 0);
      } // Update the URL


      updateURL(anchor, isNum, _settings); // Emit a custom event

      emitEvent('scrollStart', _settings, anchor, toggle); // Start scrolling animation

      smoothScroll.cancelScroll(true);
      window.requestAnimationFrame(loopAnimateScroll);
    };
    /**
     * If smooth scroll element clicked, animate scroll
     */


    var clickHandler = function clickHandler(event) {
      // Don't run if the user prefers reduced motion
      if (reduceMotion(settings)) return; // Don't run if event was canceled but still bubbled up
      // By @mgreter - https://github.com/cferdinandi/smooth-scroll/pull/462/

      if (event.defaultPrevented) return; // Don't run if right-click or command/control + click or shift + click

      if (event.button !== 0 || event.metaKey || event.ctrlKey || event.shiftKey) return; // Check if event.target has closest() method
      // By @totegi - https://github.com/cferdinandi/smooth-scroll/pull/401/

      if (!('closest' in event.target)) return; // Check if a smooth scroll link was clicked

      toggle = event.target.closest(selector);
      if (!toggle || toggle.tagName.toLowerCase() !== 'a' || event.target.closest(settings.ignore)) return; // Only run if link is an anchor and points to the current page

      if (toggle.hostname !== window.location.hostname || toggle.pathname !== window.location.pathname || !/#/.test(toggle.href)) return; // Get an escaped version of the hash

      var hash = escapeCharacters(toggle.hash); // Get the anchored element

      var anchor;

      if (hash === '#') {
        if (!settings.topOnEmptyHash) return;
        anchor = document.documentElement;
      } else {
        anchor = document.querySelector(hash);
      }

      anchor = !anchor && hash === '#top' ? document.documentElement : anchor; // If anchored element exists, scroll to it

      if (!anchor) return;
      event.preventDefault();
      setHistory(settings);
      smoothScroll.animateScroll(anchor, toggle);
    };
    /**
     * Animate scroll on popstate events
     */


    var popstateHandler = function popstateHandler(event) {
      // Stop if history.state doesn't exist (ex. if clicking on a broken anchor link).
      // fixes `Cannot read property 'smoothScroll' of null` error getting thrown.
      if (history.state === null) return; // Only run if state is a popstate record for this instantiation

      if (!history.state.smoothScroll || history.state.smoothScroll !== JSON.stringify(settings)) return; // Only run if state includes an anchor
      // if (!history.state.anchor && history.state.anchor !== 0) return;
      // Get the anchor

      var anchor = history.state.anchor;

      if (typeof anchor === 'string' && anchor) {
        anchor = document.querySelector(escapeCharacters(history.state.anchor));
        if (!anchor) return;
      } // Animate scroll to anchor link


      smoothScroll.animateScroll(anchor, null, {
        updateURL: false
      });
    };
    /**
     * Destroy the current initialization.
     */


    smoothScroll.destroy = function () {
      // If plugin isn't already initialized, stop
      if (!settings) return; // Remove event listeners

      document.removeEventListener('click', clickHandler, false);
      window.removeEventListener('popstate', popstateHandler, false); // Cancel any scrolls-in-progress

      smoothScroll.cancelScroll(); // Reset variables

      settings = null;
      toggle = null;
      fixedHeader = null;
      animationInterval = null;
    };
    /**
     * Initialize Smooth Scroll
     * @param {Object} options User settings
     */


    var init = function init() {
      // feature test
      if (!supports()) {
        throw 'Smooth Scroll: This browser does not support the required JavaScript methods and browser APIs.';
        return;
      } // Destroy any existing initializations


      smoothScroll.destroy(); // Selectors and variables

      settings = extend(defaults, options || {}); // Merge user options with defaults

      fixedHeader = settings.header ? document.querySelector(settings.header) : null; // Get the fixed header
      // When a toggle is clicked, run the click handler

      document.addEventListener('click', clickHandler, false); // If updateURL and popState are enabled, listen for pop events

      if (settings.updateURL && settings.popstate) {
        window.addEventListener('popstate', popstateHandler, false);
      }
    }; //
    // Initialize plugin
    //


    init(); //
    // Public APIs
    //

    return smoothScroll;
  };

  return SmoothScroll;
});

var App = function App() {
  var i;
  var body = document.querySelector('body');
  var menu_button = document.querySelector('.menu-button');
  var header = document.querySelector('.header--site');
  var updatecount = 0;
  var languageLink = document.querySelectorAll('[hreflang]');
  var registerLinks = document.querySelectorAll('.register-modal');
  var availabilityLinks = document.querySelectorAll('.availability-modal');
  var hideform = document.querySelectorAll('.hide-form');
  var hideavail = document.querySelectorAll('.hide-availability');
  var listLinks = document.querySelectorAll('.list-toggle');
  var childNav = document.querySelectorAll('.menu-item-has-children > a');
  var mobileSubNav = document.querySelector('.mobile-sub-nav');
  var moreAvailability = document.querySelector('.more-availability');
  var moreResidences = document.querySelector('.more-residences'); ///SUB NAV

  var _loop = function _loop() {
    var child = childNav[i];
    var navBtn = document.createElement('div'); ///

    navBtn.classList.add('sub-nav-btn');
    child.appendChild(navBtn);
    var subs = document.querySelectorAll('.show-sub-nav'); //

    for (i = 0; i < subs.length; i++) {
      subs[i].classList.remove('show-sub-nav');
    } //


    navBtn.onclick = function (e) {
      e.preventDefault();
      child.classList.toggle('show-sub-nav');
      body.classList.toggle('sub-nav-showing');
    };
  };

  for (i = 0; i < childNav.length; i++) {
    _loop();
  }

  mobileSubNav.onclick = function (e) {
    e.preventDefault();
    body.classList.remove('sub-nav-showing');

    for (i = 0; i < childNav.length; i++) {
      var child = childNav[i];
      if (child.classList.contains('show-sub-nav')) child.classList.remove('show-sub-nav');
    }
  }; //menu click


  if (menu_button) menu_button.onclick = function () {
    body.classList.toggle('menu-visible');
    if (body.classList.contains('sub-nav-showing')) body.classList.remove('sub-nav-showing');
    var subs = document.querySelectorAll('.show-sub-nav');

    for (i = 0; i < subs.length; i++) {
      subs[i].classList.remove('show-sub-nav');
    }
  }; ///FORM

  for (i = 0; i < registerLinks.length; i++) {
    registerLinks[i].onclick = function (e) {
      return showRegister(e);
    };
  }

  for (i = 0; i < hideform.length; i++) {
    hideform[i].onclick = function (e) {
      return hideRegister(e);
    };
  }

  var showRegister = function showRegister(e) {
    e.preventDefault();
    if (!body.classList.contains('show-register')) body.classList.add('show-register'); //Hide menu as it sits above the modal z-index

    if (body.classList.contains('menu-visible')) body.classList.remove('menu-visible');
  };

  var hideRegister = function hideRegister(e) {
    e.preventDefault();
    if (body.classList.contains('show-register')) body.classList.remove('show-register');
  }; /////AVAILABILITY


  moreAvailability.onclick = function (e) {
    var table = document.querySelector('#availability-table');
    table.classList.add('show-extra');
  };

  for (i = 0; i < availabilityLinks.length; i++) {
    availabilityLinks[i].onclick = function (e) {
      return showAvailability(e);
    };
  }

  for (i = 0; i < hideavail.length; i++) {
    hideavail[i].onclick = function (e) {
      return hideAvailability(e);
    };
  }

  var showAvailability = function showAvailability(e) {
    e.preventDefault();
    if (!body.classList.contains('show-availability')) body.classList.add('show-availability'); //Hide menu as it sits above the modal z-index

    if (body.classList.contains('menu-visible')) body.classList.remove('menu-visible');
  };

  var hideAvailability = function hideAvailability(e) {
    e.preventDefault();
    if (body.classList.contains('show-availability')) body.classList.remove('show-availability');
    var table = document.querySelector('#availability-table');
    if (table.classList.contains('show-extra')) table.classList.remove('show-extra');
  }; ////MORE RESIDENCES


  if (moreResidences != undefined) {
    var moreDrivers = moreResidences.previousSibling;
    moreDrivers.style.height = '80px';

    moreResidences.onclick = function (e) {
      e.preventDefault();
      body.classList.toggle('show-residence-drivers');
      if (body.classList.contains('show-residence-drivers')) moreDrivers.style.height = moreDrivers.scrollHeight + 'px';
      if (!body.classList.contains('show-residence-drivers')) moreDrivers.style.height = '80px';
    };
  }
  /*
  	for ( i = 0; i < languageLink.length; i++) {
  
  		languageLink[i].classList.add('no-barba');
  	}
  	
  		
  	
  	for ( i = 0; i < listLinks.length; i++) {
  		listLinks[i].onclick = (e) =>  toggleList(e);
  	}
  	
  */
  ///Fades


  SetUpFades('fades', 200); //Layzr

  SetUpLazy(); ///Video

  if (document.getElementsByClassName('video-holder') != null) SetUpVideos(); //FAQs

  if (document.getElementById('faqs') != null) SetUpFaqs('faqs'); //image parallax

  if (document.getElementsByClassName('img-parallax') != null) SetUpParallax(); ////

  if (document.getElementsByClassName('carousel').length > 0) SetUpCarousel(); //

  if (document.getElementsByClassName('images-carousel').length > 0) SetUpImagesCarousel(); ////

  if (document.getElementsByClassName('slider') != null) SetUpSlider('slider'); ////

  if (document.getElementById('residence-list') != null) SetUpHouses('residence-list');
  if (document.getElementsByClassName('section-illy-carousel') != null) SetUpIll('section-illy-carousel');
  if (document.getElementsByClassName('section--films') != null) SetUpArchives('section--films'); ///

  window.addEventListener('scroll', function () {
    fOverlaps();
    var shield = document.querySelector('.fixed-shield');
    var reg = document.querySelector('.section--register');
    var t = document.documentElement.scrollTop || document.body.scrollTop;
    var rpos = reg.getBoundingClientRect();
    var spos = shield.getBoundingClientRect();
    var diff = rpos.top - spos.top; //

    if (diff < 0 && body.classList.contains('show-shield')) if (body.classList.contains('show-shield')) body.classList.remove('show-shield');
    if (diff >= 100 && !body.classList.contains('show-shield')) if (!body.classList.contains('show-shield')) body.classList.add('show-shield');
  }); //
  ///Logo overlapping

  var fOverlaps = function fOverlaps() {
    //console.log('foever');
    var logo = document.getElementById('site-logo');
    var logoOverlap = false;
    var menuOverlap = false;
    var darker = document.querySelectorAll('.bg-img');
    var rectLogo = logo.getBoundingClientRect();
    var rectMenu = menu_button.getBoundingClientRect();

    for (var _i = 0; _i < darker.length; _i++) {
      var rect2 = darker[_i].getBoundingClientRect();

      logoOverlap = rectLogo.top > rect2.top && rectLogo.right < rect2.right && rectLogo.top < rect2.bottom;
      menuOverlap = rectMenu.top > rect2.top && rectMenu.right < rect2.right && rectMenu.top < rect2.bottom;
    }

    if (logoOverlap) {
      if (!body.classList.contains('wo-logo')) body.classList.add('wo-logo');
    } else {
      if (body.classList.contains('wo-logo')) body.classList.remove('wo-logo');
    }

    if (menuOverlap) {
      if (!body.classList.contains('wo-menu')) body.classList.add('wo-menu');
    } else {
      if (body.classList.contains('wo-menu')) body.classList.remove('wo-menu');
    }
  }; ///Ticker


  var fupdate = function fupdate() {
    updatecount++;

    if (updatecount > 10) {
      //console.log('update ');
      CheckFades();
      updatecount = 0;
    }

    ticker = requestAnimationFrame(fupdate);
  };

  var ticker = requestAnimationFrame(fupdate); ///scroll smooth -scroll

  if (document.querySelector('.wordpress-content .cta')) {
    var linear = new SmoothScroll('a[href*="#"]', {
      easing: 'easeOutQuad',
      speed: 1000
    });
  }

  if ("ontouchstart" in document.documentElement) {
    body.classList.add("touch");
  } ///Form label inputs


  var form = document.getElementById('modal-holder-form');
  var inputs = form.getElementsByTagName('input');

  for (i = 0; i < inputs.length; i++) {
    inputs[i].onfocus = function (e) {
      var input = e.currentTarget;
      var id = input.getAttribute('id');
      var selector = 'label[for="' + id + '"]';
      var label = document.querySelector(selector);
      if (!label.classList.contains('focused')) label.classList.add('focused');
    };

    inputs[i].onblur = function (e) {
      var input = e.currentTarget;
      var id = input.getAttribute('id');
      var selector = 'label[for="' + id + '"]';
      var label = document.querySelector(selector);
      if (label.classList.contains('focused') && !input.value) label.classList.remove('focused');
    };
  } ////AJAX


  fOverlaps(); ///

  var navtools = document.querySelectorAll('.nav--site-tools a'); //

  for (i = 0; i < navtools.length; i++) {
    var navitem = navtools[i];
    var positionInfo = navitem.getBoundingClientRect();
    var width = Math.round(positionInfo.width);
    navitem.style.width = width + 'px';
  }

  return;
};

window.onload = App;
//# sourceMappingURL=App-min.js.map
