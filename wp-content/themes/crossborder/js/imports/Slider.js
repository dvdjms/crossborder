import * as Videos from './Video';
let SliderList 
let Sliders
let animating = false;
let SliderTimeout;

const animTime = 700;
const pauseTime = 8000;

const SetUpSlider = (section) => {
	
	SliderList  = document.getElementsByClassName(section);
	
	for (let i = 0; i < SliderList.length; i++) {
		let button = document.createElement('button'); 
	
	     //add content to modal
	     SliderList[i].appendChild(button);
	     Sliders = SliderList[i].getElementsByClassName('slider-item');
	     let Slider =  SliderList[i];
	     button.onclick = (event) =>  animate(event, Slider);
		     
     }

	setWidths();
  
    window.addEventListener('resize', setWidths, false);
	
 
}


   ///Ticker

const fupdate = () => {
		
	if(animating){
		for (let i = 0; i < SliderList.length; i++) {
	
				 for (var i = 0; i < Sliders.length; i++) {
				 			
				 			let element = Sliders[i];
						
				 			checkpos(element);
				 			
						
													
				}
			}
		}


		ticker = requestAnimationFrame(fupdate);

}

let ticker = requestAnimationFrame(fupdate);

const setWidths = () => {
	
	if(animating === false){
		
		for (let i = 0; i < SliderList.length; i++) {
	
				 for (var i = 0; i < Sliders.length; i++) {
				 			
				 			let element = Sliders[i];
							let positionInfo = element.getBoundingClientRect();
							let width = positionInfo.width.toFixed(2); 
							let widthpx = width  + 'px';
				 			
				 			checkpos(element);
				 			
						element.style.width = widthpx;
						element.setAttribute('data-width', width);
													
				}
			}
	}

}


const checkpos = (element) => {
	let positionInfo = element.getBoundingClientRect();	 		
	if(positionInfo.right > document.body.offsetWidth){
		
		if(!element.classList.contains('off-screen')) element.classList.add('off-screen');
		
	} else {
		if(element.classList.contains('off-screen')) element.classList.remove('off-screen');
	}
}

const animate = (event, currentSlider) =>{
	
	if(!animating){
	//
		currentSlider.classList.add('animating');
		
		let Sliders = currentSlider.getElementsByClassName('slider-item');
		
		let slider = Sliders[0];
		let inners;
		let type;
		if(slider.classList.contains('video')) {
			inners = slider.getElementsByClassName('video-holder');
			type = 'video';
		}
		if(slider.classList.contains('image')) {
			inners  = slider.getElementsByTagName('img');
			type='image';
		}
		///select first item
		let inner = inners[0];
	
		
		inner.style.transform = 'translateX(-'+ slider.getAttribute("data-width") + 'px)';
		slider.style.width = 0;
		//
		animating = true;
		//
		duplicateNode(currentSlider, slider, type);
		SliderTimeout= setTimeout(() => {endAnimate(currentSlider, slider)}, animTime);
		
		let pagination = document.querySelectorAll('.slider-pagination button');
		let activateNext = 0;
		
		for (let i = 0; i < pagination.length; i++){
			let btn = pagination[i];
			
			
			if(btn.classList.contains('active')) {
				btn.classList.remove('active');
				activateNext = i + 1;
			}
			
		}
	
		if(activateNext > (pagination.length - 1)) activateNext = 0;
		pagination[activateNext].classList.add('active')
		
	}
		
}


const duplicateNode = (currentSlider, slider, type) =>{
	//img.removeAttribute('style');
	
	let dupNode = slider.cloneNode(true);
	let w = dupNode.getAttribute('data-width') + 'px';
	
	dupNode.removeAttribute('style');
	let inners
	if(type=='video') {
		inners = dupNode.getElementsByClassName('video-holder');
		
		////
		let id = inners[0].getAttribute('id');
		
		let id1 =  id.substring(0, id.length - 1)
		let id2 =  parseInt(id.substring(id.length - 1, id.length)) + 1
		let newID = id1 + id2;
		inners[0].setAttribute('id',newID);
		
		}
	if(type=='image') inners = dupNode.getElementsByTagName('img');
	
	inners[0].removeAttribute('style')
	
	dupNode.style.width = w;

	let newNode = currentSlider.appendChild(dupNode);
	
	if(type=='video'){
	
		let $vid = newNode.getElementsByClassName('video-holder');
		Videos.fInitVid($vid[0]);
	}
}

const endAnimate = (currentSlider, image) =>{
	///get top twitte

	animating = false;
	currentSlider.classList.remove('animating');
	image.remove();
	setWidths();
}







export {SetUpSlider };
