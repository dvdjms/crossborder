import * as Odo from '../vendor/odometer'

const arr =[];
const threshold = 10;
const barArr =[];
const odArr =[];
const amountArr=[];
let barChart;
let bars

const SetUpBars = ($id) =>{



	barChart = document.getElementById($id);
	barChart.classList.add('hide-bars');
	
	bars = barChart.getElementsByClassName('bar');
	
	for (let i = 0; i < bars.length; i++) {
		
		let bar = bars[i]
		let bar_amount = bar.querySelector('.bar--amount');
		let amount = bar_amount.innerText;
		let newamount = amount - 10
		barArr[i] = bar_amount;
		amountArr[i] = amount;
		bar_amount.innerText = newamount;
		//
		let od = new Odometer({
		  	  el: bar_amount,
			  value: newamount,
			  theme: 'default'
			});	
		
		odArr[i] = od
	
	}
	
	 	
	 ['resize','scroll','load'].forEach( evt => 
		window.addEventListener(evt, requestFrame, false)
	 );

}


const CheckBars = () => {



    		//var onScreen = Visible.isVisible(fadeArr);
    		let rect = barChart.getBoundingClientRect();
    		let limit = window.innerHeight - threshold;
    		//console.log(threshold )
    		
    		
    		if(rect.bottom <  limit) {
	    	
	    		if(!barChart.classList.contains('bars-animate')) {
				
					barChart.classList.add('bars-animate');
						for (let i = 0; i < barArr.length; i++) {
			
;							 let od = odArr[i];
							 od.update(amountArr[i]);
						}
		    	
	    		
	    		
	    		}

	    	}



}

function requestFrame () {
 
      window.requestAnimationFrame(() => CheckBars()) 
  }


export {SetUpBars };
