
const SetUpFaqs = (section = 'faqs') => {

  let container = document.getElementById(section);
  let questions = container.getElementsByTagName('h3');
	  for (var i = 0; i < questions.length; i++) {

					questions[i].onclick = (event) => {

						let target = event.target
						let article = target.parentNode;
						let answer = article.querySelector('.answer');
						let closedHeight = target.scrollHeight + 'px';

						article.style.height = article.scrollHeight  + 'px';
						article.classList.toggle('open');

						if(article.classList.contains('open')) {
							// total of question + answer
								let totalHeight = target.scrollHeight + answer.scrollHeight  + 'px';
						 		article.style.height = totalHeight

						} else{
						// 			// make it the question height only
							article.style.height =  closedHeight;


						}
						console.log('tgt: ' + target + 'pt: ' + answer + ' pth' + answer.scrollHeight);

					}

		}
}

export {SetUpFaqs};
