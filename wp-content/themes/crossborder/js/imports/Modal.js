import * as Lazy from './Lazy';
let body = 	document.querySelector('body');
let urlArray = new Array();


const SetUpPeopleModal = () =>{

//	let threshold = $threshold;
	let people = document.querySelectorAll('.people a');

       for (let i = 0; i < people.length; i++) {
	       let link = people[i];
	       link.setAttribute('data-pos', i);
		   link.onclick = (e) =>  fModalLink(e,i);
		   ///
		   urlArray.push(link.getAttribute('href'));
	}
	//external to the loaded data

}

const fSetUpPaginationNav = (elem,i) =>{
	
	i =  Number(i);

	let pagination = document.createElement('div'); 
	let next = document.createElement('a');
	let prev =  document.createElement('a');
	//
	pagination.classList.add('pagination');
	next.classList.add('next-person');
	prev.classList.add('prev-person');
	//
	next.setAttribute('data-pos', i + 1);
	prev.setAttribute('data-pos', i - 1);
	//
	pagination.appendChild(next);
	pagination.appendChild(prev);
	//
	next.onclick = (e) => fPaginationLink(e, prev,next);
	prev.onclick = (e) => fPaginationLink(e, prev,next);
	//
	elem.appendChild(pagination);
	//
	fUpdatePagination(i,prev,next);
};



const fPaginationLink = (e,prev,next) => {
	
	e.preventDefault();
	let tgt = e.currentTarget;
	
	let href = tgt.getAttribute('href');
	let modal = document.querySelector('.people-modal');
	let i = tgt.getAttribute('data-pos');
	//
	if(tgt.classList.contains('prev-person')) {
			var moveOut = 'out-left';
			var moveIn 	= 'in-right';
		} else {
			var moveOut = 'out-right';
			var moveIn 	= 'in-left';	
	}
	
	if (modal.classList.contains('in-left')) modal.classList.remove('in-left');
	if (modal.classList.contains('in-right')) modal.classList.remove('in-right');
	
	modal.classList.add(moveOut);		
	fLoadContent(modal,href,moveIn);
	
	fUpdatePagination(i,prev,next);
	
}


const fUpdatePagination =(i, prev, next) =>{
	i =  Number(i);
	let nextUrl = urlArray[i + 1];
	let prevUrl = urlArray[i - 1];
	//
	next.setAttribute('data-pos', i + 1);
	prev.setAttribute('data-pos', i - 1);
	
	if(prevUrl == undefined)  {
		prev.classList.add('disabled');
	} else {
		if(prev.classList.contains('disabled')) prev.classList.remove('disabled');
		prev.setAttribute('href', prevUrl);
	}
	if(nextUrl == undefined)  {
		next.classList.add('disabled');
	} else {
		if(next.classList.contains('disabled')) next.classList.remove('disabled');
		next.setAttribute('href', nextUrl);
	}
}


const fModalLink = (e,i) => {
	
	e.preventDefault();
	let tgt = e.currentTarget;
	
	let href = tgt.getAttribute('href');
	///add modal container
	let modal = document.createElement('div'); 
	let shadow = document.createElement('div');
	shadow.classList.add('shadow');
	//
	modal.classList.add('people-modal');
	body.appendChild(modal);
	modal.appendChild(shadow);
	
	//
    fSetUpPaginationNav(modal,i);

	//set close on click
	shadow.onclick = (event) => {
	   	
	   	 modal.parentNode.removeChild(modal);
	 	 
	   	if(body.classList.contains('loading-people-modal')) body.classList.remove('loading-people-modal');
	   	if(body.classList.contains('show-people-modal')) body.classList.remove('show-people-modal');
	   	 
   	 }
	
	
	fLoadContent(modal,href);
	
}

//////

const fLoadContent = (modal,href,moveInClass = null)  => {


	//timeout for loading animation
	let timerid
	if (timerid) clearTimeout(timerid);
	//
	if(body.classList.contains('show-people-modal')) body.classList.remove('show-people-modal');
	//
	if(!body.classList.contains('loading-people-modal'))body.classList.add('loading-people-modal')
	
	
	
	let  xhr = new XMLHttpRequest();
	xhr.open('GET', href, true);
	
	
	xhr.onload = function() {
	  if (this.status >= 200 && this.status < 400) {
	    // Success!
	    
	    //remove current content if it exists
	    let oldcontent = modal.querySelector('#modal-content');
	    if(oldcontent!=null){ 
		    
		    oldcontent.parentNode.removeChild(oldcontent);
	    }
	    let resp = this.response;
		let dummy = document.createElement('html'); 
		///get the modal content for the response
		dummy.innerHTML = resp;
		let content = dummy.querySelector('#modal-content');
	     //add content to modal
	     modal.appendChild(content);
	     ///ad a a close button
	     let close = document.createElement('a'); 
		 close.classList.add('close-people-modal');
		 content.appendChild(close);
		 close.onclick = () =>{
			 	 modal.parentNode.removeChild(modal);
		 		if(body.classList.contains('loading-people-modal')) body.classList.remove('loading-people-modal');
		 		if(body.classList.contains('show-people-modal')) body.classList.remove('show-people-modal');
	   	
		 }
		 //add some pagination
	     ///update classses
	 
	     if(!body.classList.contains('show-people-modal')) body.classList.add('show-people-modal');
	     if(body.classList.contains('loading-people-modal'))  body.classList.remove('loading-people-modal');
		   //pagination
		 	if (modal.classList.contains('out-left')) modal.classList.remove('out-left');
		 	if (modal.classList.contains('out-right')) modal.classList.remove('out-right');
		 if(moveInClass) modal.classList.add(moveInClass);
		
		  ///
		 if (timerid)  clearTimeout(timerid);
	     //images
	   	 Lazy.SetUpLazy();
	   
	    
	     } else {
		     console.log('error');
	    // We reached our target server, but it returned an error
	
	  }
	};
	
	xhr.onerror = function() {
	  // There was a connection error of some sort
	};
	
	xhr.send();
	
	
	
}




export { SetUpPeopleModal };