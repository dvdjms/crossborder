let container
let speed

const SetUpBgScroll = (node, $speed = 0.3) => {
	
	
	
	container = document.getElementById(node);
	speed = $speed;
  
	  ['resize','scroll','load'].forEach( evt => 
		window.addEventListener(evt, requestFrame, false)
	 );
 
}


const bgUpdate = () => {

	let pos = window.pageYOffset;
	let height = container.scrollHeight;
	
	let r =  (  (pos * speed)).toFixed(3)  +  'px';
	var o = (1100 - pos) / 1000;
	
	//var css = 'background-position: 50% ' + r + '; opacity: ' + o + ';'
	var css = 'background-position: 50% ' + r + ';'
		
		container.style.cssText = css;

	}	
 

	
function requestFrame () {
 
      window.requestAnimationFrame(() => bgUpdate()) 
  }



export {SetUpBgScroll};

