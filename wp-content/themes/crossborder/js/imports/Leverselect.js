
let element;
let jobfilterform 
let loading = false;

const SetUpSelects= ($class) => {
	
	jobfilterform  = document.getElementById('lever-jobs-filter');
		
	let selects = jobfilterform.getElementsByClassName($class);
	//let submit = jobfilterform.querySelector('.job-filter-submit');
	let clickTimeout;
	

	for (let i = 0; i < selects.length; i++) {
	   let select = selects[i];
	   let legend = select.querySelector('legend');
	  
	   legend.onclick = () => {
	
		   	if(element != select && element != undefined && element.classList.contains('multi-select-open')) element.classList.remove('multi-select-open');
			//near bottom
			let bounding = legend.getBoundingClientRect();
			
			let checks = select.querySelector('.checks');
			let cutOff = bounding.bottom  + checks.scrollHeight;
			//console.log(cutOff);
			
			if(cutOff >= (window.innerHeight || document.documentElement.clientHeight)) {
				if(!select.classList.contains('move-up')) select.classList.add('move-up');
			} else {
				if( select.classList.contains('move-up')) select.classList.remove('move-up')
			}
			
			
			if(select.classList.contains('multi-select-open')){
			  select.classList.remove('multi-select-open');
			} else if(!select.classList.contains('multi-select-open')){
			  select.classList.add('multi-select-open');
			  element = select;
			  document.addEventListener('click', outsideClickListener);
			} 
	   }   
	   ///uncheck on click
	   let checks = select.querySelectorAll('.label-checkbox');
   	   
   	   for (let j = 0; j < checks.length; j++) {
	   		
   			let check = checks[j];
   		
   			check.onclick = (event) => {
	   		
	   			let checked = 	select.querySelectorAll('input');
	   			
	   			for(let k = 0; k < checked.length; k++){
		   			if(checked[k].checked = true) checked[k].checked = false;
	   			}
	   			
	   			check.querySelector('input').checked = true;
	   			let selector = check.querySelector('span');
	   			let selected = legend.querySelector('span');
	   			selected.innerHTML = selector.innerHTML;
	   			
	   			///close it
	   			if(element != undefined && element.classList.contains('multi-select-open')) element.classList.remove('multi-select-open');
	   			///
	   			//submitForm();
	   			//clearTimeout(clickTimeout);
	   			//clickTimeout = setTimeout(loadJobs, 300)
	   			
	   		}
	   }

	}
	
	//loadJobs();
}



const outsideClickListener = (event) => {
	if (!element.contains(event.target)) removeClickListener();
}

const removeClickListener = () => {
	if(element != undefined && element.classList.contains('multi-select-open')) element.classList.remove('multi-select-open');
	document.removeEventListener('click', outsideClickListener);
}

///


export {SetUpSelects};