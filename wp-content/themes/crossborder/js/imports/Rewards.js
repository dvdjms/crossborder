
let element;
let rewardfilterform 
let loading = false;
let timer;
let Lazy;

const SetUpRewards = ($class, $lazy) => {
	Lazy = $lazy
	rewardfilterform  = document.getElementById('reward-filter');
		
	let cats = document.querySelectorAll('.cat-item a');

	let radios = rewardfilterform.querySelectorAll('input[name="sort"]');
	let numberInput = rewardfilterform.querySelector('input[name="points"]');
	let clearInput = rewardfilterform.querySelector('.clear-input');
	let clickTimeout;
	
	///
	for(let i = 0; i < cats.length; i++){
		let cat = cats[i];
		
		cat.onclick = (e) => {
			e.preventDefault();
			if(loading === true) return;
			let href = cat.getAttribute('href');
			let activeCat = document.querySelector('.cat-item a.active');
			let clickedCat = e.currentTarget;
			activeCat.classList.remove('active');
			clickedCat.classList.add('active');
			//update action so it's in place for filtering;
			rewardfilterform.setAttribute('action', href);
			loadRewards();
		}
	}
	///radios
	for(let i = 0; i < radios.length; i++){
		radios[i].onclick = (e) => {
		//
			loadRewards();
		}
	}
	///Nunber
	numberInput.addEventListener("input", fInput);
	clearInput.onclick = (e) => {
		e.preventDefault();
		numberInput.value = '';
		numberInput.classList.add('cleared');
		loadRewards();
	}
	
	
	loadRewards();
	//
}

const fInput = (e) => {
	let numberInput = e.currentTarget;
	let iv = numberInput.value.length;
	if(numberInput.classList.contains('cleared')) numberInput.classList.remove('cleared');
		
	if(iv > 0) {
		//loadRewards();
		clearTimeout(timer);
      timer = setTimeout(loadRewards, 500);	
		//console.log('fire');	
		//loadRewards()
	} 
}

const fSetUpPagination = ($content) =>{
	let pagination = $content.querySelector('.load-more a');

	if(pagination){
		let href = pagination.getAttribute('href'); 
			
		pagination.onclick = (e) =>{
			e.preventDefault();
			let link = e.currentTarget;
			loadRewards(href, true);
			link.parentNode.remove();
		}
	}
	
}

const loadRewards = ($href = null, $pag = false) => {
	
		//if(loading === true) return;
		
		 loading = true;
		
			let formData = new FormData(rewardfilterform);
		
			let href;
			//console.log(formData);
			//console.log(formData.get('points'))
			$href ? href = $href : href = rewardfilterform.getAttribute('action');
			//href = $href; 
			let holder = document.getElementById('load-holder');
		
			if($pag){
				holder.classList.add('loading-more');
			} else {
				holder.innerHTML = "";
				holder.classList.add('loading-content');
			}
			let h = holder.clientHeight;
			holder.style.height = h + 'px';
		

		///get page
		let  xhr = new XMLHttpRequest();
		xhr.open('POST', href, true);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.onload = function() {
		  if (this.status >= 200 && this.status < 400) {
			    // Success!
			 	loading = false;
			    let resp = this.response;
				let dummy = document.createElement('html'); 
				//console.log(resp.substring(0, 100));
				///get the modal content for the response
				dummy.innerHTML = resp;
				let content = dummy.querySelector('#load-holder .load-content');
			     //add content to modal
			    holder.appendChild(content);
			    if(holder.classList.contains('loading-content')) holder.classList.remove('loading-content');
			    if(holder.classList.contains('loading-more')) holder.classList.remove('loading-more');
				//
				let nh; 
				if($pag){
					// the 100 is the height of the pagination that we get rid of
					nh = content.clientHeight  + h - 100;	
				} else  {
			    	nh = content.clientHeight;
			    }
				holder.style.height = nh + 'px';
				content.classList.add('fade-in');
				fSetUpPagination(content);
				//setUpMobileJobs();
				Lazy.SetUpLazy();
				
		     } else {
		    //
		
		  }
		};
		
		xhr.onerror = function() {
		  // There was a connection error of some sort
		};
		
		xhr.send(formData);
	}


	///




export {SetUpRewards};