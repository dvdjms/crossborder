import * as Visible from './Visible';


const vidArray = [];

const SetUpVideos = () => {

	let videoItems = document.getElementsByClassName('video-holder');

	for (let i = 0; i < videoItems.length; i++) {

   		 fInitVid(videoItems[i]);

    }
}




window.addEventListener('scroll', event => {
       isVideoVisible();
});

window.addEventListener('resize', event => {
       isVideoVisible();
});


const fInitVid = ($vid) => {



	let close =  $vid.querySelector('.close-video');
	
	let video = $vid.getElementsByTagName('video');  
	
	let src;
	
	
	close.onclick = (event) => {
		
		event.stopPropagation();
			video[0].currentTime = 0;
			if($vid.classList.contains('show-video')){
				$vid.classList.remove('show-video');
				video[0].pause();
				video[0].currentTime = 0;
				
				}
		}
	
	$vid.onclick = (event) => {
		if($vid.classList.contains('show-video')){
   			return;
   		} else {
	   		src = $vid.getAttribute('data-src'); 
	   		$vid.classList.add('show-video');
	   		if(src){
		   		
		   			$vid.removeAttribute('data-src'); 
			   		
			   		 let type = $vid.getAttribute('data-type');
			   		 let source = document.createElement('source');
			   		 
			   		 source.setAttribute('src', src);
			   		 source.setAttribute('type', type);
			   		 
			   		 video[0].appendChild(source);
	   		 
		   		} else {
			   		
			   		video[0].currentTime = 0;
			   		video[0].play();
			   		
		   		}
	   		 //vidArray.push({iframe, player});
	   	
   		}
	}
	
	

}



const vimPause = (player) => {
	console.log('pause');
	player.pause().then(function() {
    // the video was paused
	}).catch(function(error) {
    switch (error.name) {

        case 'PasswordError':
            // the video is password-protected and the viewer needs to enter the
            // password first
            break;

        case 'PrivacyError':

            break;

        default:
            // some other error occurred
            break;
    }
});
}

const vimPlay = (player) => {
	player.play().then(function() {
    // the video was played
	}).catch(function(error) {

    switch (error.name) {
        case 'PasswordError':
            // the video is password-protected and the viewer needs to enter the
            // password first
            break;

        case 'PrivacyError':
            // the video is private
            break;

        default:
            // some other error occurred
            break;
    }
});
}

const isVideoVisible = () => {


	vidArray.forEach((arr,index, object) => {

    		var onScreen = Visible.isVisible(arr.iframe);

    		if(!onScreen) {
	    		vimPause(arr.player);
	    		object.splice(index, 1);
	    	}

	});




}



export {SetUpVideos};
