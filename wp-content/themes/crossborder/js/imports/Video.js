import * as Visible from './Visible';
//import * as Vimeo from './vendor/player';
//import * as Vimeo from '@vimeo/player';
import Player from '../vendor/player';

const vidArray = [];

const SetUpVideos = () => {

	let videoItems = document.getElementsByClassName('video-header');
	let videoItemsDriver = document.getElementsByClassName('video-driver');
	let videoItemsClick = document.getElementsByClassName('video-holder-click');
	let videoItemsSlider = document.getElementsByClassName('video-holder-slider');

	for (let i = 0; i < videoItems.length; i++) {
		
   		 fInitVid(videoItems[i]);
   		
    }
    ///criver
    for (let i = 0; i < videoItemsDriver.length; i++) {
		videoItemsDriver[i].setAttribute('id', 'video-driver'+i );
   		 fInitVid(videoItemsDriver[i]);
   		
    }
    ///Video
    for (let i = 0; i < videoItemsClick.length; i++) {
		
		let $vid = videoItemsClick[i];
		$vid.setAttribute('id', 'video-holder'+i );
		$vid.onclick = (event) => {
		
			if(!$vid.classList.contains('show-video')) {
				fInitVid($vid);
				
			}
		}

    }
    
    ////Video slider
     for (let i = 0; i < videoItemsSlider.length; i++) {
		videoItemsSlider[i].setAttribute('id', 'video-slider'+i );
		fInitVid(videoItemsSlider[i]);

    }
    
}




window.addEventListener('scroll', event => {
       isVideoVisible();
});

window.addEventListener('resize', event => {
       isVideoVisible();
});


const fInitVid = ($vid) => {

	const id = $vid.getAttribute("data-id");
	const autoplay = $vid.getAttribute("data-autoplay");
	
	const muted =  $vid.getAttribute("data-muted");
	const node_id = $vid.getAttribute("id");

	let iframe;
	let close =  $vid.getElementsByClassName('close-video');
	let playpause =  $vid.getElementsByClassName('play-video')[0];
	
	
	let ismute = false;
	if(muted=="true") ismute = true;
	
	let player 
	
	$vid.classList.add('show-video');
	
	var options = {
        id: id,
        byline: false,
        portrait:false,
        title: false,
		autoplay: false,
		loop: true,
		controls: false,
		app_id: 122963,
		muted: ismute
		};
	
   		player = new Vimeo.Player(node_id, options);
   		
   		player.ready().then(function() {
   		 	
			iframe = $vid.getElementsByTagName('iframe')[0];
			
			//console.log ('vimeo ready' + player + ' ap: '+autoplay + ' '+playpause);
			
			$vid.classList.add('video-ready');
			///if data-autoplay = autoplay
			if(autoplay == "autoplay") fPlayPause(null,player,playpause);
			//
			vidArray.push({iframe, player, playpause, id, $vid});
			
		});
		player.on('bufferstart', (data) => {
			
		})
		player.on('play', (data) => {
			//console.log ('vMeo on play' + iframe + ' ');
			
		});
		//
		
		////Play pause
		if(playpause) playpause.onclick = () => fPlayPause(event,player,playpause);		
}

const fPlayPause = (e,player, playpause) =>{
	
	player.getPaused().then(function(paused) {
			    // paused = whether or not the player is paused
		
			    if(paused) {
				  
				    	if(playpause != undefined) {
					    	playpause.setAttribute('data-copy', 'Pause video');
							if(!playpause.classList.contains('is-playing')) playpause.classList.add('is-playing');
						}
				     	vimPlay(player);
				     
				     } else{
					     if(playpause != undefined) {
					     	playpause.setAttribute('data-copy', 'Play video');
					     	if(playpause.classList.contains('is-playing')) playpause.classList.remove('is-playing');
					     }
					      vimPause(player);
				     }
			}).catch(function(error) {
			    // an error occurred
			});
	
}

const vimPause = (player) => {
	
	player.pause().then(function() {
    // the video was paused
	}).catch(function(error) {
    switch (error.name) {

        case 'PasswordError':
            // the video is password-protected and the viewer needs to enter the
            // password first
            break;

        case 'PrivacyError':

            break;

        default:
            // some other error occurred
            break;
    }
});
}

const vimPlay = (player) => {
	
	//$vid.classList.add('video-ready');
	player.play().then(function() {
    // the video was played
	}).catch(function(error) {

    switch (error.name) {
        case 'PasswordError':
            // the video is password-protected and the viewer needs to enter the
            // password first
            break;

        case 'PrivacyError':
            // the video is private
            break;

        default:
            // some other error occurred
            break;
    }
});
}

const isVideoVisible = () => {

	let spliceArray = [];
	
	vidArray.forEach((arr, index, object) => {
			///sliders remeve the element so we should destry the player
			let originalElement = arr.player._originalElement;
			if(originalElement == undefined) spliceArray.push(index);
    		
    		if(arr.iframe != undefined) var onScreen = Visible.isVisible(arr.iframe);
			
    		if(!onScreen) {
	    		
	    		
	    		let hasAutopause = arr.$vid.getAttribute('data-autopause');
	    		    		
	    		
	    		if(hasAutopause==null || hasAutopause!= "false")	 {  
		    		
					vimPause(arr.player);
		    		let playpause = arr.playpause
		    		 
		    		if(playpause != undefined) {
		    		 	playpause.setAttribute('data-copy', 'Play video');
					 	if(playpause.classList.contains('is-playing')) playpause.classList.remove('is-playing');
					 }
		    		///
		    		spliceArray.push(index);
	    		}
	    	
	    	}

	});


	for (let i = spliceArray.length - 1; i >=0 ; i--) {
		
		vidArray.splice(spliceArray[i], 1);
	}

}

const destroyPlayer = (player, arrayIndex=null, $vid=null, $replace=null) => {
	player.destroy().then(function() {
		    // the player was destroyed
		   
		    ///remove the classes added
		    if($vid!=null) $vid.classList.remove('show-video','video-ready');
		    ////if we're replacing add new object here
		    if($replace!=null) fInitVid($replace);
		    ///remove fro the array
			if(arrayIndex!=null) vidArray.splice(arrayIndex, 1);
		   
		}).catch(function(error) {
		    // an error occurred
		});
} 



const SetUpArchives = (section) => {
	
	let containers  = document.getElementsByClassName(section);
	

	for (let i = 0; i < containers.length; i++) {
		 let container = containers[i];
		 let tabs = container.querySelectorAll('.tabs li');
		 let thumbs = container.querySelectorAll('.driver-video');
	    
	    
	    for(let i = 0; i < tabs.length; i++){
		    let tab = tabs[i];
		    let n = tab.getAttribute('data-tab');
		 
		    tab.onclick = (event) =>  animate(event, n, container);
	    }

	    
	     for(let i = 0; i < thumbs.length; i++){
		    let thumb = thumbs[i];
		    let code = thumb.getAttribute('data-code');
		  
		    thumb.onclick = (event) =>  newFilm(event, code, container);
	    }

	 
		     
     }
	
}



const animate = (event, id, $container) =>{

		let active = $container.querySelectorAll('.films-holder.active');	
		let tabActive = event.currentTarget;
		let tab = $container.querySelectorAll('.tabs .active');		
		let item = $container.querySelectorAll('div[data-item="'+id+'"]');	
		
		active[0].classList.remove('active');
		item[0].classList.add('active');
		
		tab[0].classList.remove('active');
		tabActive.classList.add('active');

}

const newFilm = (event, $code, $container) =>{

		let current = $container.querySelectorAll('.active .video-holder');	
		let $vid = current[0];
		let currentId =  $vid.getAttribute('data-id');
		let inArray = false;
		$vid.setAttribute('data-id',$code);
		$vid.removeAttribute('style');
		
		vidArray.forEach((arr,index, object) => {
				
				if(arr.id  ==  currentId ) {
					
					destroyPlayer(arr.player, index, arr.$vid, $vid );
					inArray = true
				}
    			
	    		
	    });
	    
	    if(inArray === false){
		    fInitVid($vid);
		    
	    }


		

}




export {SetUpVideos, SetUpArchives, fInitVid, vimPlay};
