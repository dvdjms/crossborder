

const SetUpIll = (section) => {
	
	let containers  = document.getElementsByClassName(section);
	

	for (let i = 0; i < containers.length; i++) {
		 let container = containers[i];
		 let tabs = container.querySelectorAll('.tabs li');
		 //items = container.querySelectorAll('.illy--item');
	    
	    for(let i = 0; i < tabs.length; i++){
		    let tab = tabs[i];
		    let n = tab.getAttribute('data-tab');
		    //console.log('n: '+n);
		    tab.onclick = (event) =>  animate(event, n, container);
	    }
		///find the buttons
		let buttonsLeft = container.querySelectorAll('.btn-left');
		let buttonsRight = container.querySelectorAll('.btn-right');
		let buttonLeft
		let buttonRight
		if(buttonsLeft.length>0) {
			
			buttonLeft = buttonsLeft[0];
				buttonLeft.onclick = () =>  {
			 	let n = getn(container, tabs, 'minus');
			 	console.log(n);
			 	animate(event, n, container);
		} 
		}
		if(buttonsRight.length>0) {
			buttonRight = buttonsRight[0];
			buttonRight.onclick = () =>  {
			 	let n = getn(container, tabs, 'plus');
			 	console.log(n);
			 	animate(event, n, container);
			 	
		}  
			
			}	
		
	
		   
     }
	
	
//	 mainHouses[0].style.display = 'block';
//	 mainHouses[0].classList.add('fade-in');
}

const getn = (container, tabs, action) =>{
	let current = container.querySelectorAll('.illy--item.active');
			let n = Number(current[0].getAttribute('data-item'));
			if(action == 'minus') n = n - 1;
			if(action == 'plus') n = n + 1;
			if(n < 0) n = tabs.length - 1;
			if(n >= tabs.length  ) n = 0;
			
			return n;
}

const animate = (event, id, $container) =>{
		let active = $container.querySelectorAll('.illy--item.active');	
		//let tabActive = event.currentTarget;
		let tabActive = $container.querySelectorAll('li[data-tab="'+id+'"');
		let tab = $container.querySelectorAll('.tabs .active');		
		let item = $container.querySelectorAll('div[data-item="'+id+'"]');	
		//console.log(tabActive);
		
		active[0].classList.remove('active');
		item[0].classList.add('active');
		
		tab[0].classList.remove('active');
		tabActive[0].classList.add('active');
}


export {SetUpIll };
