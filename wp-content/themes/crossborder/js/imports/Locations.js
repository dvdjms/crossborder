let tabs;
let listings;
let locations;

const SetUpLocations = () =>{

//	let threshold = $threshold;
	locations = document.querySelectorAll('.locations');
	listings = document.querySelectorAll('.locations .listing');
    
    for (let i = 0; i < locations.length; i++) {
	    let loc = locations[i];
		   
		tabs = loc.querySelectorAll('button');
	    for (let i = 0; i < tabs.length; i++) {
		    tabs[i].onclick = (e) => fUpdateLocation(e);
		   }
	}
	//external to the loaded data

}


const fUpdateLocation = (e) => {
	e.preventDefault();
	let tgt = e.currentTarget
	let num = tgt.getAttribute('tab-id');
	
	//remove active
	for (let i = 0; i < locations.length; i++) {
	    let loc = locations[i];
		let actives = loc.querySelectorAll('.active');
		let stacks =  loc.querySelectorAll('.stack');
		for (let i = 0; i < actives.length; i++) {
		 	actives[i].classList.remove('active');
		 }
		 
		 for (let i = 0; i < stacks.length; i++) {
		 	stacks[i].classList.remove('show-stack');
		 }
	 }
	 //add active
	 tgt.classList.add('active');
	 //
	 for (let i = 0; i < listings.length; i++) {
	 	let listing = listings[i];
	 	if(listing.hasAttribute('tab-id') && (listing.getAttribute('tab-id') == num)){
		 	listing.classList.add('active');
		 	let stack = listing.querySelector('.stack');
		 	stack.classList.add('show-stack');
	 	}
	 }
	 
}



export { SetUpLocations };