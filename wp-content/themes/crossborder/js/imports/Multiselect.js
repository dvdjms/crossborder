
let element;
let jobfilterform 
let loading = false;

const SetUpSelects= ($class) => {
	
	jobfilterform  = document.getElementById('job-filter');
		
	let selects = jobfilterform.getElementsByClassName($class);
	let submit = jobfilterform.querySelector('.job-filter-submit');
	let clickTimeout;
	
	
	submit.onclick = (e) =>{
		
		e.preventDefault();
		loadJobs();
	}
	
	
	for (let i = 0; i < selects.length; i++) {
	   let select = selects[i];
	   let legend = select.querySelector('legend');
	  
	   legend.onclick = () => {
	
		   	if(element != select && element != undefined && element.classList.contains('multi-select-open')) element.classList.remove('multi-select-open');
			//near bottom
			let bounding = legend.getBoundingClientRect();
			
			let checks = select.querySelector('.checks');
			let cutOff = bounding.bottom  + checks.scrollHeight;
			//console.log(cutOff);
			
			if(cutOff >= (window.innerHeight || document.documentElement.clientHeight)) {
				if(!select.classList.contains('move-up')) select.classList.add('move-up');
			} else {
				if( select.classList.contains('move-up')) select.classList.remove('move-up')
			}
			
			
			if(select.classList.contains('multi-select-open')){
			  select.classList.remove('multi-select-open');
			} else if(!select.classList.contains('multi-select-open')){
			  select.classList.add('multi-select-open');
			  element = select;
			  document.addEventListener('click', outsideClickListener);
			} 
	   }   
	   ///uncheck on click
	   let checks = select.querySelectorAll('.label-checkbox');
   	   
   	   for (let j = 0; j < checks.length; j++) {
	   		
   			let check = checks[j];
   		
   			check.onclick = (event) => {
	   		
	   			let checked = 	select.querySelectorAll('input');
	   			
	   			for(let k = 0; k < checked.length; k++){
		   			if(checked[k].checked = true) checked[k].checked = false;
	   			}
	   			
	   			check.querySelector('input').checked = true;
	   			let selector = check.querySelector('span');
	   			let selected = legend.querySelector('span');
	   			selected.innerHTML = selector.innerHTML;
	   			
	   			///close it
	   			if(element != undefined && element.classList.contains('multi-select-open')) element.classList.remove('multi-select-open');
	   			///
	   			//submitForm();
	   			clearTimeout(clickTimeout);
	   			clickTimeout = setTimeout(loadJobs, 300)
	   			
	   		}
	   }

	}
	
	loadJobs();
}

/*
const submitForm = () =>
{
  document.jobFilter.submit();
}
*/

const outsideClickListener = (event) => {
	if (!element.contains(event.target)) removeClickListener();
}

const removeClickListener = () => {
	if(element != undefined && element.classList.contains('multi-select-open')) element.classList.remove('multi-select-open');
	document.removeEventListener('click', outsideClickListener);
}

const fSetUpPagination = ($content) =>{
	let pagination = $content.querySelectorAll('a.page-numbers');
	
	for(let i = 0; i < pagination.length; i++){
		
		let link = pagination[i];
		let href = link.getAttribute('href'); 
		
		pagination[i].onclick = (e) =>{
			e.preventDefault();
			loadJobs(href);
		}
	}
}

const loadJobs = ($href = null) => {
	
		if(loading === true) return;
		loading = true;
		let formData = new FormData(jobfilterform);
		let href;
		
		$href ? href = $href : href = jobfilterform.getAttribute('action');
		let holder = document.getElementById('load-holder');
	
		
		holder.innerHTML = "";
		let h = holder.clientHeight;
		holder.style.height = h + 'px';
		holder.classList.add('loading-content');

		///get page
		let  xhr = new XMLHttpRequest();
		xhr.open('POST', href, true);
		xhr.onload = function() {
		  if (this.status >= 200 && this.status < 400) {
			    // Success!
				loading = false;
			    let resp = this.response;
				let dummy = document.createElement('html'); 
				
				///get the modal content for the response
				dummy.innerHTML = resp;
				let content = dummy.querySelector('#load-holder .load-content');
			     //add content to modal
			    holder.appendChild(content);
			    holder.classList.remove('loading-content');
				//
			    let nh = content.clientHeight;
				holder.style.height = nh + 'px';
				content.classList.add('fade-in');
				fSetUpPagination(content);
				setUpMobileJobs();
		     } else {
		    //
		
		  }
		};
		
		xhr.onerror = function() {
		  // There was a connection error of some sort
		};
		
		xhr.send(formData);
	}


	///
	const setUpMobileJobs = () =>{
		if(document.getElementsByClassName('mobile-touch').length >  0){
			
			setUpMobileJobHeights();
			
			let jobs = document.getElementsByClassName('mobile-touch');
			
			for (let i = 0; i < jobs.length; i++) {
				let job = jobs[i].parentNode;
				job.onclick = (event) => {
					let tgt = event.target;
					if(!tgt.classList.contains('mobile-cta')){
						job.classList.toggle('job-open');
					}
					
					
				}
			}
		
		}
	}
	const setUpMobileJobHeights = () => {
		let jobs = document.getElementsByClassName('mobile-touch');
		for (let i = 0; i < jobs.length; i++) {
			
			let job = jobs[i].parentNode;
			let copy = job.querySelector('.copy');
			let h = copy.scrollHeight + 'px';
			job.setAttribute('style', '--job-height:' + h);
			
		}
	}
	
window.addEventListener("resize", requestFrame)



function requestFrame () {
 
      window.requestAnimationFrame(() => setUpMobileJobHeights())
   
    
  }

export {SetUpSelects};