
const myScrollTo = function(element, to, duration) {
	
	element.classList.add('auto');
	
    const
    start = element.scrollTop,
    change = to - start,
    startDate = +new Date(),
    // t = current time
    // b = start value
    // c = change in value
    // d = duration
    easeInOutQuad = function(t, b, c, d) {
        t /= d/2;
        if (t < 1) return c/2*t*t + b;
        t--;
        return -c/2 * (t*(t-2) - 1) + b;
    },
    animateScroll = function() {
        const currentDate = +new Date();
        const currentTime = currentDate - startDate;
        element.scrollTop = parseInt(easeInOutQuad(currentTime, start, change, duration));
     //console.log(parseInt(easeInOutQuad(currentTime, start, change, duration)));
     
     window.dispatchEvent(scrollEvent);
        if(currentTime < duration) {
            requestAnimationFrame(animateScroll);
        }
        else {
            element.scrollTop = to;
            element.classList.remove('auto');
            //fStopScrolling();
        }
    };
    animateScroll();
};

(function () {
  if ( typeof window.CustomEvent === "function" ) return false; //If not IE

  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
   }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;
})();


const scrollEvent =  new CustomEvent('customScroll', {
		bubbles: true,
		
  		});

const SetUpScroller = ($id) =>{


	let container = document.getElementById($id);
	let searchContainer = document.getElementById('search-countries');
	let scroller = container.querySelector('.countries');
	///alhpa links
	let alphabet = container.querySelector('.alphabet');
	let links = container.querySelectorAll('a[data-easing]');
	let topTarget =  scroller.querySelector('#top-target');
	let input = searchContainer.querySelector('input');
	let countries = scroller.querySelectorAll('dd[data-name]');
	let targets = scroller.querySelectorAll('a.target');
	let currentCountry = scroller.querySelector('.current-country');
	let closeInput = searchContainer.querySelector('.close-input');
	let timer;
	let focusTimer;
	let target = null;
	let countryPressed = false;
	
	
	/// this lets us know if a country has been clicked so we know not to trigger the focusout - not if it's current country
	for (let i = 0; i < countries.length; i++) {
		if(!countries[i].classList.contains('current-country')){
			countries[i].onmousedown = (e) =>{
				 countryPressed = true;
				console.log('countryPressed');
			}
		}
	}
	
	closeInput.onclick = (e) => {
		e.preventDefault();
		fFocusOut();
		}
	
	for (let i = 0; i < links.length; i++) {
	 	 let link = links[i];
	 	
			  ///
	 	 link.onclick = (e) =>  {
		 	let current = alphabet.querySelector('.active');
			if(current!=null) current.classList.remove('active');
		 	link.classList.add('active');
		 	e.preventDefault();
		 	 ///get the link target from the clicked link
	 		let tgt = scroller.querySelector(link.getAttribute('href'));
	 		///the top taget (for refernce)
	 		//rectangle bounds of 2 targets
	 		let tgtBounds = tgt.getBoundingClientRect();
	 		let topBounds = topTarget.getBoundingClientRect();
	 		///differnce between them determines where to move the scroller
	 		let offset   =   tgtBounds.top - topBounds.top;
	 		//scroll function
	 		myScrollTo(scroller, offset, 1000);
	     } 		
	}
	///if we are on a country page automatically scroll to the entry
	 
	if(currentCountry!=null){
		
		
		 //console.log(currentCountry);
		 
		 let tgtBounds = currentCountry.getBoundingClientRect();
	 	 let topBounds = topTarget.getBoundingClientRect();
		 let offset   =   tgtBounds.top - topBounds.top - 40;
	 		//scroll function
	 	myScrollTo(scroller, offset, 1);
		 
	}
	//
	scroller.addEventListener('scroll', function(){fScroll()});
	
	function fScroll(){
		if(scroller.classList.contains('auto')) return;
		let oldTarget
		if(target!=null) oldTarget = target;
		
		window.dispatchEvent(scrollEvent);
		
		 for (let i = 0; i < targets.length; i++){
			 let topBound = container.getBoundingClientRect().top;
			 let offset   =   targets[i].getBoundingClientRect().top - topBound;
			 
			 //if(targets[i].getAttribute('id') == 'target-b')  console.log (targets[i].getAttribute('id') + ' : ' +  offset);
			 if(offset < 1){
				 target = targets[i].getAttribute('id');
				// break;
			 } else{
				 break;
			 }
			 
			 
		 }
		
		 if(target != oldTarget){
			//  console.log (target);
			  let current = alphabet.querySelector('.active');
			  let link = alphabet.querySelector('a[href="#'+target+'"]');
			  if(current!=null) current.classList.remove('active');
			  link.classList.add('active');
			  
		 }
	 }
	///Input

	//
	input.addEventListener("focusin", fFocusIn);
	//input.addEventListener("focusout", fFoucusTimeout);
	input.addEventListener("input", fInput);
	//
	function fFocusIn(e) {
		
		//if(!container.classList.contains('focused')) container.classList.add('focused');
		let iv = input.value.length;
		if(iv > 0) container.classList.add('input');
		
		
		clearTimeout(timer);
		timer = setTimeout(function(){fSortCountries(input.value)}, 300);
		
	}
	function fFocusOut(e) {
		

		//if(container.classList.contains('focused')) container.classList.remove('focused');
		if(searchContainer.classList.contains('focused')) searchContainer.classList.remove('focused');
		
		if(container.classList.contains('input')) container.classList.remove('input');
		//
		let hidden = scroller.querySelectorAll('.country-hide');
		for (let i = 0; i < hidden.length; i++) {
			hidden[i].classList.remove('country-hide');
			}
		input.value = '';
		fScroll();
	}
	
	function fFoucusTimeout(){
		clearTimeout(focusTimer);
		if(!countryPressed) focusTimer = setTimeout(function(){fFocusOut()}, 100);
	}
	
	function fInput(e) {
		//add input class
		
		let iv = input.value.length;
		
		if(iv > 0) {
			
			if(!searchContainer.classList.contains('focused')) searchContainer.classList.add('focused');
			
		
				
		} else {
			if(searchContainer.classList.contains('focused')) searchContainer.classList.remove('focused');
		}
		
		if(!container.classList.contains('input')) container.classList.add('input');
		if(scroller.classList.contains('fade-in')) scroller.classList.remove('fade-in');
		if(scroller.classList.contains('no-results')) scroller.classList.remove('no-results');
		
		
		
		clearTimeout(timer);
		timer = setTimeout(function(){fSortCountries(input.value)}, 300);
		
		
	}
	
	function fSortCountries($val){
		let val = $val.toLowerCase();
		let results = 0;
		//
		for (let i = 0; i < countries.length; i++) {
	 	  let country = countries[i];
	 	  let name = country.getAttribute('data-name');
	 	  let n = name.search(val);
	 	  
	 	  ////
	 	  if(n===0){
		 	   
		 	  //ok we have a match
		 	  results++;
		 	  if(country.classList.contains('country-hide')) country.classList.remove('country-hide');
	 	  } else {
		 	  
		 	  if(!country.classList.contains('country-hide')) country.classList.add('country-hide');
	 	  }
		}
		if(results === 0)  scroller.classList.add('no-results');
		
		if(val.length === 0) if(container.classList.contains('input')) container.classList.remove('input');
				///
		scroller.scrollTop = 0;
		scroller.classList.add('fade-in');
	}

}



export {SetUpScroller, scrollEvent };
