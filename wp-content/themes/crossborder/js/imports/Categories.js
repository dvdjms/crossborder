let loading = false;
let Lazy;

const SetUpCategories = ($lazy) => {
	Lazy = $lazy
	let ar = document.querySelector('.section--archive');
	let wc = document.querySelectorAll('.webinar-categories');
	if(wc) SetUpWebinarCategories(wc);
	if(ar===null) return
	let cats = ar.querySelectorAll('.cat-item a');
	let type = ar.getAttribute('data-archive');
	//
	let legend  = ar.querySelector('.legend span');
	//let clickTimeout;
	
	///
	for(let i = 0; i < cats.length; i++){
		let cat = cats[i];
		
		cat.onclick = (e) => {
			e.preventDefault();
			if(loading === true) return;
			let href = cat.getAttribute('href');
			let activeCat = document.querySelector('.cat-item a.active');
			let clickedCat = e.currentTarget;
			let newLegend = cat.textContent;
			////
			activeCat.classList.remove('active');
			clickedCat.classList.add('active');
			let openlist = document.querySelector('.list-open');
			if(openlist) openlist.classList.remove('list-open');
			//update action so it's in place for filtering;
			legend.textContent = newLegend;
			loadCategories(href);
		}
	}
	
	///initial category load dynamic
	loadCategories(type);
	//
}



const fSetUpPagination = ($content) =>{
	let pagination = $content.querySelector('.load-more a');

	if(pagination){
		let href = pagination.getAttribute('href'); 
			
		pagination.onclick = (e) =>{
			e.preventDefault();
			let link = e.currentTarget;
			loadCategories(href, true);
			link.parentNode.remove();
		}
	}
	
}

const loadCategories = ($href = null, $pag = false) => {
	
		//if(loading === true) return;
		if(!$href) return
		 loading = true;
		
		
			let href = $href;
			let holder = document.getElementById('load-holder');
		
			if($pag){
				holder.classList.add('loading-more');
			} else {
				holder.innerHTML = "";
				holder.classList.add('loading-content');
			}
			let h = holder.clientHeight;
			holder.style.height = h + 'px';
		

		///get page
		let  xhr = new XMLHttpRequest();
		xhr.open('POST', href, true);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.onload = function() {
		  if (this.status >= 200 && this.status < 400) {
				// Success!
				console.log('success')
			 	loading = false;
			    let resp = this.response;
				let dummy = document.createElement('html'); 
				console.log(resp);
				///get the modal content for the response
				dummy.innerHTML = resp;
				let content = dummy.querySelector('#load-holder .load-content');
			     //add content to modal
			    holder.appendChild(content);
			    if(holder.classList.contains('loading-content')) holder.classList.remove('loading-content');
			    if(holder.classList.contains('loading-more')) holder.classList.remove('loading-more');
				//
				let nh; 
				if($pag){
					// the 100 is the height of the pagination that we get rid of
					nh = content.clientHeight  + h - 100;	
				} else  {
			    	nh = content.clientHeight;
			    }
				holder.style.height = nh + 'px';
				content.classList.add('fade-in');
				fSetUpPagination(content);
				//setUpMobileJobs();
				Lazy.SetUpLazy();
				
		     } else {
		    //
		
		  }
		};
		
		xhr.onerror = function() {
		  // There was a connection error of some sort
		};
		
		xhr.send();
	}

const SetUpWebinarCategories = ($wc) => {
	for(let i = 0; i < $wc.length; i++){
		let filter = $wc[i];
		setUpFilters(filter);
	}
	//let cats = wc.querySelectorAll('.cat-item a');
}
//
const setUpFilters = ($filter) => {
	
	let cats = $filter.querySelectorAll('.cat-item a');
	let legend  = $filter.querySelector('.legend span');
	//
	for(let i = 0; i < cats.length; i++){
		let cat = cats[i];
		
		cat.onclick = (e) => {
			e.preventDefault();
			
			let filter = cat.getAttribute('data-filter');
			let section = cat.getAttribute('data-section');
			let activeCat = document.querySelector('.cat-item a.active');
			let clickedCat = e.currentTarget;
			let newLegend = cat.textContent;
			////
			activeCat.classList.remove('active');
			clickedCat.classList.add('active');
			let openlist = document.querySelector('.list-open');
			if(openlist) openlist.classList.remove('list-open');
			//
			legend.textContent = newLegend;
			//update action so it's in place for filtering;
			filterWebinars(filter, section);
		}
	}
}

const filterWebinars = ($filter, $section) => {
	//get the section
	let section = document.querySelector('.'+$section);
	if(section===null) return
	let filtered = section.querySelectorAll('.'+$filter);
	let all = section.querySelectorAll('.entry');
	
	section.classList.add('hide-for-filtering');
	if($filter === 'all'){
	/////set all to visible
		for(let i = 0; i < all.length; i++){
			let entry = all[i];
			if(entry.classList.contains('filter-hide')) entry.classList.remove('filter-hide');
		}
		
	} else {
		//if data-filer = all
		///hide everything
		for(let i = 0; i < all.length; i++){
			let entry = all[i];
			if(!entry.classList.contains('filter-hide')) entry.classList.add('filter-hide');
		}
		//otherwise hide filtered
		for(let k = 0; k < filtered.length; k++){
			let filtered_entry = filtered[k];
			console.log('filterentry', filtered_entry)
			if(filtered_entry.classList.contains('filter-hide')) filtered_entry.classList.remove('filter-hide');
			
		}
	}
	setTimeout(() => {section.classList.remove('hide-for-filtering');}, 300)
}
	///




export {SetUpCategories};