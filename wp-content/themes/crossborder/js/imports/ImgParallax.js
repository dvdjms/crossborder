

const parallaxArray = [];



  const settings = {
    overlap: 'data-normal',
    retina: 'data-retina',
    srcset: 'data-srcset',
    threshold: 0
  }


const SetUpParallax = ($threshold = 0) => {
	

	parallaxArray .length = 0;
	
	let imageItems =  document.querySelectorAll('.img-parallax');
	  
	//settings.threshold = $threshold;            
	
	for (let i = 0; i < imageItems.length; i++) {
  
   		parallaxArray .push(imageItems[i]);
   		 
    }
  
	SetUpEvents();
	requestFrame ();
}

const SetUpEvents = () => {

 ['resize','scroll','load'].forEach( evt =>  window.addEventListener(evt, requestFrame, false) );

}

function requestFrame () {
 
      window.requestAnimationFrame(() => fCheckParallax())
   
    
  }

const fCheckParallax = () => {
	
	
	for (let i = 0; i < parallaxArray.length; i++) {
		let container = parallaxArray[i];
		let img = container.querySelector('img')

		///check it's got height, if not remove parallax
		var h =  container.scrollHeight;
		
		if(h <= 0 || h == undefined) container.classList.remove('img-parallax');
		//
		
		 let rect = container.getBoundingClientRect();
			 if  (rect.top <= (window.innerHeight || document.documentElement.clientHeight)){
				 let moves = container.scrollHeight + window.innerHeight;
				 let rb = rect.bottom;
				 let ratio = (rb / moves).toFixed(2);
				// 
				 let overlap = 20 //20% set in css (height = 120%)
				 
				
				 let y =  (overlap * ratio);
				 let coords = 'translate3d(-50%, -'+ y +'%, 0)';
				
				 img.setAttribute("style", "transform:" +  coords)
			 
		 }
		
		
		
		}
}






export {SetUpParallax};