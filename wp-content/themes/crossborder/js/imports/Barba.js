// do not import Barba like this if you load the library through the browser
import barba from '../vendor/barba';
//import * as Odo from '../vendor/odometer'

// basic default transition (with no rules and minimal hooks)

//Please note, the DOM should be ready
document.addEventListener("DOMContentLoaded", function() {
	let Barba = barba;
	 Barba.Pjax.init();
     Barba.Prefetch.init();
	
});