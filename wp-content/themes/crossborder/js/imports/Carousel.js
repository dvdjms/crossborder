import Siema from '../vendor/siema';


const singleArgs = {
		  selector: '.carousel',
		  duration: 300,
		  easing: 'ease-out',
		  perPage: 1,
		  startIndex: 0,
		  draggable: true,
		  multipleDrag: true,
		  threshold: 20,
		  loop: false,
		  rtl: false,
		  pagination:true,
		  onInit: () => {},
		  onChange: fChange
		};
		
const carouselArray = [];
let activeTimeout;
//let autoplayTimer

const SetUpCarousel = ($class = null) => {
	
	let carousels =  document.querySelectorAll('.carousel');
	
	for(let i = 0; i<carousels.length; i++){
	
		let pagination = false;
		let carousel = carousels[i];
		let id = carousel.getAttribute('id');
		let dataDuration = carousel.getAttribute('data-duration');
		let dataLoop = carousel.getAttribute('data-loop');
		let dataAuto = carousel.getAttribute('data-autoplay');
		singleArgs.selector = '#'+id;
		
		if(dataDuration) singleArgs.duration = dataDuration;
		if(dataLoop) singleArgs.loop = true;
		if(dataAuto) singleArgs.autoplay = true;
		//singleArgs.duration = $duration;
		///number of items in the carousel, if only one stop dragging		
		let num = carousel.childElementCount;

		if(num <=1) singleArgs.draggable = false;
		if(num >1) singleArgs.draggable = true;
		let mySiema = new Siema(singleArgs);

		
		let prev = mySiema.selector.parentNode.querySelector('.prev');
		let next = mySiema.selector.parentNode.querySelector('.next');
	
		if(prev) {
			prev.addEventListener('click', () => mySiema.prev());
			if(singleArgs.loop==false) prev.classList.add('disabled')
		}
		if(next) next.addEventListener('click', () => mySiema.next());	
	
		if(singleArgs['pagination'] == true) pagination = true
		
		// Trigger pagination creator
			
		if(pagination){
			mySiema.addPagination();
		}
		
		carouselArray.push(mySiema);
		
		if(dataAuto) mySiema.autoplayTimer = setTimeout(() => mySiema.next(), 3000)
	}
}

const SetUpMobileCarousel = ($class = null) => {
	
	let carousel =  document.querySelector('.'+$class);
	let pagination = false;
	let id = carousel.getAttribute('id');
	let dataDuration = carousel.getAttribute('data-duration');
	singleArgs.selector = '#'+id;
	if(dataDuration) singleArgs.duration = dataDuration;
	//singleArgs.duration = $duration;
	///number of items in the carousel, if only one stop dragging		
	let num = carousel.childElementCount;

	if(num <=1) singleArgs.draggable = false;
	if(num >1) singleArgs.draggable = true;
	let mySiema = new Siema(singleArgs);


	if(singleArgs['pagination'] == true) pagination = true
	
	// Trigger pagination creator
		
	if(pagination){
		mySiema.addPagination();
	}
	carouselArray.push(mySiema);
	return mySiema;
	

}



Siema.prototype.addPagination = function() {
 //console.log(this);
 // let btns = document.querySelectorAll('.carousel-pagination button');
 let carousel = this.selector.parentNode;
 let btns = carousel.querySelectorAll('.carousel-pagination button');

	 
	 let items = this.selector.querySelector('div').childNodes;
		////looped carousels add an item before the start so active slide is 1 not 0
	 if(this.config.loop){
		 items[1].classList.add('active-slide');
	 } else {
	 	items[0].classList.add('active-slide');
	 	}
	
  for (let i = 0; i < btns.length; i++) {
   	let btn = btns[i];
    
    btn.addEventListener('click', (e) => this.goTo(i));

    if(i === 0) {
	     btn.classList.add('active');
	 } else{
		 if(btn.classList.contains('active')) btn.classList.remove('active')
	 }
  }
 
 }
 


function fChange(){
	
	let siema = this;
	let carousel = this.selector.parentNode;
	let pagination =  carousel.querySelector('.carousel-pagination');
	let btns = carousel.querySelectorAll('.carousel-pagination button');
	
	let prev = carousel.querySelector('.prev');
	let next = carousel.querySelector('.next');
	
	fActiveSlide(siema);
		
	if(btns){
		let currentButton = btns[siema.currentSlide];
		
		let actives = carousel.querySelectorAll('.carousel-pagination .active');
		for (let i = 0; i < actives.length; i++) {
		 	actives[i].classList.remove('active')	
		}
		currentButton.classList.add('active');
	
		let cbr = currentButton.getBoundingClientRect();
		let cpr = pagination.getBoundingClientRect();
		let diff = (cpr.left - cbr.left).toFixed(2);
		let transform = "translateX("+ diff.toString() +"px)";
		pagination.style.transform = transform;
		//console.log(cbr.left + ' ' + cpr.left  + ' ' + diff + ' ' +transform);
		
	
	}
	////Prev and next disabled
	if(siema.config.loop == false && prev!=null && next!=null){
		
		if(siema.currentSlide == 0 ){
		
			prev.classList.add('disabled');
		
		} else{
			
			if(prev.classList.contains('disabled')) prev.classList.remove('disabled')
		}
		
		if(siema.currentSlide == siema.innerElements.length - 1){
			next.classList.add('disabled');
		} else{
			if(next.classList.contains('disabled')) next.classList.remove('disabled')
		}
	}
	if(siema.config.autoplay){
		clearTimeout(siema.autoplayTimer);
		siema.autoplayTimer = setTimeout(() => siema.next(), 3000);
	}

	
}



function fActiveSlide(siema){

	let items = siema.selector.querySelector('div').childNodes;
	let carousel = siema.selector.parentNode;
	
	let activeSlide = carousel.querySelector('.active-slide');
	if(activeSlide != null){
		let cs = siema.currentSlide;
		////looped carousels add an item before the start so active slide is 1 not 0
		if(siema.config.loop){
			cs = cs + 1;
		}
		items[cs].classList.add('active-slide');
		activeSlide.classList.remove('active-slide');
	}
}

function fResetActiveSlide(){

	let spliceArray = [];

	for(let i = 0; i < carouselArray.length; i++){
		//spliceArray.push(i);
		let siema = carouselArray[i];
		
		//fActiveSlide(siema);
		let items = siema.selector.querySelector('div').childNodes;
		let cs = siema.currentSlide;
		////looped carousels add an item before the start so active slide is 1 not 0
		if(siema.config.loop){
			cs = cs + 1;
		} 
		let currentItem = items[cs];
		
		if(currentItem != undefined && currentItem.childNodes.length > 0){
			currentItem.classList.add('active-slide');
		} else {
			spliceArray.push(i);
		}
			
	}
	
	for (let i = spliceArray.length - 1; i >=0 ; i--) {
			
			carouselArray.splice(spliceArray[i], 1);
	}	
}

function fCheckCarousels(){
//	console.log('----');
	//console.log('---check');
fResetActiveSlide() 
	//clearTimeout(activeTimeout);
	//activeTimeout = setTimeout(function(){ fResetActiveSlide() }, 300);
}

window.addEventListener("resize", requestFrame)



function requestFrame () {

      window.requestAnimationFrame(() => fCheckCarousels())
   
    
  }


export {SetUpCarousel, fChange, SetUpMobileCarousel};
