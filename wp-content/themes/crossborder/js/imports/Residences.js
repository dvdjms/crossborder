let mainHouses

const SetUpHouses = (section) => {
	
	let container  = document.getElementById(section);
	let houses = container.querySelectorAll("div[data-type='house']");
	mainHouses = document.querySelectorAll("div[data-type='main-house']");
	
	for (let i = 0; i < houses.length; i++) {
		 let house = houses[i];
		 let id = house.getAttribute("id");
	     house.onclick = (event) =>  animate(event,id);
		     
     }
	 mainHouses[0].style.display = 'block';
	 mainHouses[0].classList.add('fade-in');
}



const animate = (event, id) =>{

	for (let i = 0; i < mainHouses.length; i++) {
		 let house = mainHouses[i];
		 if( house.classList.contains('fade-in'))house.classList.remove('fade-in')
		house.style.display = 'none';
		
     }
	
	 	let mainHouse = document.getElementById('house-'+id);
	 	mainHouse.style.display = 'block';
	 	mainHouse.classList.add('fade-in');
	
}


export {SetUpHouses };
