<?php get_header(); ?>
<!--blog page -->

<?php include(get_template_directory().'/template-parts/content-page.php'); ?>
<section class="section--listing">
	<div class="content">

<?php while ( have_posts() ) : the_post(); ?>
	<?php include(get_template_directory().'/template-parts/entry.php'); ?>
<?php endwhile; // End of the loop. ?>
	
	 <div class="pagination">
			 
				<?php 
					previous_posts_link( 'Newer posts', $the_query->max_num_pages );
					
					next_posts_link( 'Older posts', $the_query->max_num_pages );
	
				?>
			</div><!-- END PAG-->
	
	</div>
</section>


<?php get_footer(); ?>
