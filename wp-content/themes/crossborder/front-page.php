<?php
/**
 * The template for displaying home pahge.

 */

get_header(); ?>
<!--front page -->

<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'template-parts/content', 'masthead' ); ?>
<?php get_template_part( 'template-parts/content', 'page' ); ?>
<?php get_template_part( 'template-parts/content', 'blocks' ); ?>

<?php endwhile;?>

<?php get_footer(); ?>
