<?php
/**

 * The template for displaying the categories.
 */

get_header(); ?>



<section class="listing" >
	<div class="wrapper"  id="load-holder" data-max="<?php echo $max; ?>" data-page="<?php echo $paged; ?>">
	
<div class="container">
	 
	 <?php
		$count = 0;		
		while ( have_posts() ) : the_post(); 
		get_template_part('template-parts/entry', 'post');	
		endwhile; // End of the loop. ?>		
	 	
	 <div class="pagination">
			<?php next_posts_link('More') //usage with max_num_pages
			
			//previous_posts_link( 'Previous' );// clean up after the query and pagination
		
			?>
			</div><!-- END PAG-->
		
		</div>		
		</div><!--- END WRAPPER-->
</section>
<?php get_footer(); ?>
