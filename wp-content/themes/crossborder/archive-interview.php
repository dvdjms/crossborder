<?php
/**
 * The template for displaying archive job pages.
 *
 */
if(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    wp_redirect( get_permalink( get_page_by_path( 'interviews' ) ));
    //echo $_SERVER['HTTP_X_REQUESTED_WITH'];
}

get_header(); ?>
<!---archive white paper -->
<?php include(get_template_directory().'/template-parts/content-hub-archives.php'); ?>
<?php get_footer(); ?>