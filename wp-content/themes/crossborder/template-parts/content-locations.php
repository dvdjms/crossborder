<?php
	//settings
	$settings = null;
	$stacking = null;
	


		
	?>



	
<section class="section--locations fades">

<div class="content">
	<div class="content--inner">
		
		<div class="copy">
			
				<?php echo e_acf(get_sub_field('locations-tagline'),'h4');?>
				<?php echo e_acf(get_sub_field('text'), 'h2');?>
		</div>
		<div class="copy">
				<?php echo e_acf(get_sub_field('column_text'));?>
		</div>
		
		<div class="locations">
	   <?php  if (get_sub_field('location_groups')): $count=0; ?>
	   		<div class="tabs">
	   			<?php while( the_repeater_field('location_groups') ): $count++;
	   			$n = null;
	   			$n = get_sub_field('name');?>
	   			<button class="<?php if ($count==1) echo 'active';?>" tab-id="<?php echo $count;?>"><?php echo $n;?></button>
	   			<?php 	endwhile; ?>
	   		</div>
	  <?php  endif; ?>
		
			
	 <?php  if (get_sub_field('location_groups')): $count=0; ?>		
			<div class="addresses">
				<?php while( the_repeater_field('location_groups') ): $count++; ?>
				<div class="listing<?php if ($count==1) echo ' active';?>" tab-id="<?php echo $count;?>">
					<div class="offices">
					   <?php  if (get_sub_field('locations')): ?>
					   		<?php while( the_repeater_field('locations') ):
					   			$ad = null; 
					   			$ad = get_sub_field('address');
					   			echo '<address>' . $ad . '</address>';
					   			endwhile; ?>
					  <?php  endif; ?>			
					</div>
					  <?php  if (get_sub_field('stack_images')): ?>
					   		<div class="stack">
					   		<?php while( the_repeater_field('stack_images') ):
					   			 
					   			$i = get_sub_field('image');
					   			$img = getImage($i, 'large');
					   			?>
					   			<div class="image"><?php echo $img['lazy'];?></div>
				
					   <?php 	endwhile; ?>
					   </div>
					  <?php  endif; ?>				
				</div><!-- end listing-->
				

				<?php 	endwhile; //MAIN WHILE ?>
	   		</div>
	  <?php  endif; ?>
						
			
		</div><!-- end locations-->

			
	</div>	<!-- end content inner-->





	
</div><!-- end content-->
</section>

