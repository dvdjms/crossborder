<section class="section--quotes">
	<div class="content">
	<div class="intro copy">
	<?php echo e_acf(get_sub_field('quotes-tagline'),'h4');?>
	<?php echo e_acf(get_sub_field('quotes-intro'), 'h2');?>
	
	</div>
		
		 
		 
		    <?php  if (get_sub_field('quotes')): ?>
		    		
		    		<?php while( the_repeater_field('quotes') ):
		    			 
		    			
			   			$i =  null;
			   			$img = null;
			   			$q = null;
			   		
			   			
			   			$q = get_sub_field('quote_text');
			   			$i = get_sub_field('quote_image');
			   			
			   			if($i) $img = getImage($i, 'large');?>
			   			
			   			 <div class="quote-holder">
			   						   			
			   				<?php if($img) echo '<div class="image fades">' .$img['lazy'] .'</div>';?>
		    			 
						  <div class="copy quote fades"><?php if ($q) echo $q; ?>  </div>
			   			 </div>	
		    <?php 	endwhile; ?>
		   <?php  endif; ?>
		   

		 </div>
	</div>
</section>


