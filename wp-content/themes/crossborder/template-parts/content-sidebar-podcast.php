
<?php if($spotify_link ||$itunes_link):?>
<div class="image-holder sidebar--podcast">
				
    <div class="image">
        <?php if($img):
                    echo $img['img'];
                        else:?>
                    <img src="<?php echo get_template_directory_uri();?>/placeholders/podcast.png" alt="podcast image"/>
                    <?php endif;?>

    </div>

	<?php	if($itunes_link) echo '<a href="'.$itunes_link .'" class="cta invert itunes">Apple Podcasts</a>';
			if($spotify_link ) echo  '<a href="'.$spotify_link .'" class="cta spotify">Spotify</a>';?>
					
</div>
	
 <?php endif; ?>