
<?php 
global $numCarousel;
	
	if(!is_numeric($numCarousel)) {
		$numCarousel = 1;
		} else{
		$numCarousel++;
	}
	

	
	$news =		null;
	$args = array( 'posts_per_page' => 24, 'post_type'=>'job' );
	$news = get_posts( $args );
	
	
	if ($news): ?>
		   <div class="carousel-pagination">
		   		<?php foreach( $news as $post): setup_postdata($post);
		   			$dep = get_field('department');
		   			
		   		
		   			echo '<button class=""><span>' . $dep . '</span></button>';
		   		endforeach; wp_reset_postdata();?>
		   </div>
	<?php  endif; 
	        
		if ($news): ?>
		   <div id="carousel-<?php echo $numCarousel;?>"  class="carousel">
		   		<?php foreach( $news as $post): setup_postdata($post);
			   		
			   	
			   		$tx =	null;
		   			$i = 	null;
		   			$img = 	null;
		   		
		   			$tx = 	get_sub_field('description');
		   			$i = 	get_sub_field('image');
		   			
			   		
		   		?>
		   		
		   		<article class="item driver driver--news">
						<div class="copy">
						
							
							
							<?php  echo '<h3>' . get_the_title() . '</h3>';?>
							<?php echo subString(get_the_excerpt(),120);?>
							<a href="<?php echo get_the_permalink();?>" class="cta">Find out more</a>
						</div>
						
					</article>
		   		
		   	
		   <?php endforeach; wp_reset_postdata();?>
		   </div>
		<?php  endif; ?>