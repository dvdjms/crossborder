<?php
	
$i = get_sub_field('image');
if($i) $img = getImage($i, 'large');
$align = get_sub_field('alignment');
$title = null;
$title = get_sub_field('title');
	?>



	
<section class="section--text fades align-<?php echo $align;?>">

<div class="content">
	<div class="copy">
	<?php echo '<h4>' . $title . '</h4>'; ?>
	<?php e_acf('header_text', 'h2');?>
	<?php e_acf('text');?>
	<?php echo link_acf(get_sub_field('discover_link'), 'cta-discover');?>
	</div>
	<?php if($img):?>
	<div class="image img-parallax">
		<?php echo $img['lazy'];?>
	</div>
	<?php endif;?>

</div>

</section>

