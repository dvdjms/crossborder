<?php 
				
$vc = get_sub_field('vimeo_code');
$poster = get_sub_field('image');
$thumbnail = null;

$vimeoUrl = 'https://player.vimeo.com/video/'.$vc;
$vimeoThumb = getVimeoThumbnail($vimeoUrl );

	
if($poster){
	 $img = getImage($poster, 'hero');
	 $thumbnail = $img['url'];
} else {
	$thumbnail =  $vimeoThumb;
}

?>


<?php /////https://player.vimeo.com/video/302904046?loop=1&background=1&app_id=122963 ?>
<?php if($vc): ?>	
<section class="section--video">
	<div clas="content">
	<?php e_acf('title','h2',null,null,true);?>
	</div>

		<div class="video-holder video-holder-click" data-autoplay=autoplay data-id = "<?php echo $vc; ?>"<?php if($thumbnail) echo ' style="background-image: url(' . $thumbnail . ');"'; ?>>
			<a class="play-video" data-copy="play video"></a>
		</div>
	
</section>	
	<?php endif;?>