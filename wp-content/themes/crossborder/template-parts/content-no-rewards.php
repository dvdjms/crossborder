<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 */



?>

		


<div class="content-blocks">	
<section class="section--none">
<div class="content">
	<h1 class="page-title"><?php esc_html_e( 'No rewards found', 'xborder' ); ?></h1>
		

				<p><?php esc_html_e( 'We don’t currently have what you’re looking for.', 'xborder' ); ?></p>
			
		

	
</div>
</section>
</div>