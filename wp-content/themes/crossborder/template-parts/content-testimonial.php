<?php
	
	$copy =		null;
	$person  =	null;
	$pc =		null;
	
	$copy =		get_sub_field('copy');
	$person =		get_sub_field('person');
	$pc =		get_sub_field('position_company');

	?>



	
<section class="section--testimonial fades">

<div class="content">
	<div class="copy">
	<?php if($copy) echo '<blockquote>&ldquo;' . $copy . '&rdquo;</blockquote>'; ?>
	<?php if($person) echo '<cite>' . $person . '</cite>'; ?>
	<?php if($pc) echo '<p>' . $pc . '</p>'; ?>
	</div>
	  

</div>

</section>

