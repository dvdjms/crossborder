<section class="archive-rewards">
	<div class="content">	

		
		<?php if ( have_posts() ) :  ?>
		
				<div class="wrapper"  id="load-holder">
				<div class="listing--rewards listing load-content fades">
			<?php while ( have_posts() ) : the_post();  
				
				include(get_template_directory().'/template-parts/entry-reward.php');  
			 
			 endwhile;
			 
			echo '<div class="load-more">'; 
				next_posts_link( 'Load more' );
			echo '</div>';
			?>
			</div>
			</div>

		<?php else : ?>
			<div class="wrapper"  id="load-holder">
			<div class="listing--rewards listing load-content fades">
			<?php get_template_part( 'template-parts/content', 'no-rewards' ); ?>
			</div>
			</div>
		<?php endif; ?>
		
	</div>
</section>