<?php
	$post_id = get_the_ID();
	$link = null;
	$link_target = '';
	$class = '';
	if(get_field('fill_background')) $class = ' full-bg';
	$link = get_field('link');
	if($link) $link_target = $link['target'] ? $link['target'] : '_self';
	
?>

<article class="item driver entry--reward<?php echo $class; ?>">
<?php if($link) echo '<a href="' . esc_url( $link['url'] ) . '" target=' . esc_attr( $link_target ) . '></a>';?>
	<div class="image">
	<?php if(get_field('reward_image')):
		
		$img = getImage(get_field('reward_image'), 'reward');
		echo $img['lazy'];
	 endif; ?>
	</div>
	
	<?php if(get_field('point_value')): ?>
	<div class="points">		
		<?php echo number_format(get_field('point_value')); ?> Points
	</div>
	
	<?php endif;?>

		<div class="copy">
		
		
			<?php  echo '<p>' . get_the_title() . '</p>';?>
		</div>

</article>
