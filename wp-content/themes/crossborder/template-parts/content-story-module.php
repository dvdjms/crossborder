<?php
	//settings
	$settings = null;	
	$style='';

	$settings = get_sub_field('settings');	
	
	
	$desktop = null;
	$mobile = null;

	if($settings['desktop_image']) $desktop = img_acf($settings['desktop_image'], 'hero','url');
	if($settings['mobile_image']) $mobile = img_acf($settings['mobile_image'], 'hero','url');
	

	?>



	
<section class="section--story-module fades align-bottom">

<div class="content">
	<div class="content--inner"<?php echo $style;?>>
		
		

		<div class="copy">
				<?php echo e_acf(get_sub_field('story-tagline'),'h4');?>
				<?php echo e_acf(get_sub_field('text'),'h2');?>
		</div>
		<div class="copy">
				<?php echo e_acf(get_sub_field('column_text'));?>
		</div>
		
		
	
	
	
	</div>	





	<?php if($desktop || $mobile):?>
	<picture>        
		   <source media="(min-width: 750px)" srcset="<?php echo $desktop;?>"  />
		   <source media="(max-width: 750px)" srcset="<?php echo $mobile;?>"  />
		   <img src="<?php echo $desktop;?>" alt="backup" />
   
   </picture>
	<?php endif;?>
	
</div>
</section>

