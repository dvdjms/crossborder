<div class="search-box">   
<form method="get" class="search-form" action="<?php echo esc_url( home_url() ); ?>">
				<label><span class="screen-reader-text">Search for:</span></label>
				<input type="search" class="search-field" placeholder="" value="" name="s" title="Search for:" />
				<input type="submit" class="search-submit" value="Go" />
	</form>
</div>