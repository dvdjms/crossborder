<?php


   //$terms =   (has_term( '', 'article-category' )) ? wp_get_object_terms( $post->ID,  'article-category' );
    $cpt = get_post_type();
    $taxonomy = '';
    $terms = null;
    $catlinks  = null;
  
   if (has_term( '', 'article-category' )):
        $terms = wp_get_object_terms( $post->ID,  'article-category' );
        $taxonomy = 'article-category';
   endif;
   if (has_term( '', 'white-paper-category' )):
        $terms = wp_get_object_terms( $post->ID,  'white-paper-category' );
        $taxonomy = 'white-paper-category';
    endif;
    if (has_term( '', 'interview-category' )):
        $terms = wp_get_object_terms( $post->ID,  'interview-category' );
        $taxonomy = 'interview-category';
    endif;
   
 
    if ( ! empty( $terms ) ) {
        if ( ! is_wp_error( $terms ) ) {
            $catlinks='<div class="cat-links">';
                foreach( $terms as $term ) {
                    $catlinks .=  '<span class="cat-link">' . esc_html( $term->name ) . '</span>, '; 
                }
                $catlinks = rtrim($catlinks , ', ');
                $catlinks.='</div>';
        }
    }
?>


<article class="entry hub-entry entry--<?php echo $cpt;?>">
<?php if($catlinks) echo  $catlinks; ?>
    <a href="<?php echo get_the_permalink(); ?>">
        <div class="copy">
                <?php  echo '<h1>' . get_the_title() . '</h1>';?>
                <?php echo '<p>' .get_the_excerpt() . '</p>'; ?>
        </div>
    </a>
</article>
