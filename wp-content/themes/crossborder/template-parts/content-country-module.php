<?php
	//settings
	$settings = null;
	$country = null;
	$style='';
	$img = null;
	
	$country = get_sub_field('country_image');	
	$settings = get_sub_field('settings');	

	$desktop = get_template_directory_uri() . '/placeholders/ill-arm.svg';
	$mobile =  get_template_directory_uri() . '/placeholders/ill-mobile-arm.svg';
		
	if($country) $img = img_acf($country, 'hero','lazy');
?>



	
<section class="section--module fades align-right has-bg type-text has-arm">

<div class="content">
	<div class="content--inner" style="background-color:#655dc6">

		<div class="copy">
			<?php //echo e_acf(get_sub_field('text'));
				
				$tagline = 	null;
				$title = 	null;
				$text = 	null;
				$link = 	null;
				
				$tagline = get_sub_field('hub-tagline');
				$title = get_sub_field('hub-title');
				$text = get_sub_field('text');
				$link = get_sub_field('hub-link');
			
				if($tagline) 	echo '<h4>'.$tagline.'</h4>';
				if($title) 	echo '<h2>'.$title.'</h2>';
				if($text) 	echo $text;
				if($link)	echo '<a href="'.$link['url'].'" class="cta">' . $link['title'] . '</a>'; ?>
		</div>
		<?php if($country) echo '<div class="image country-image">'.$img.'</div>'; ?>
	</div>	
		
	<picture>  
	   <source media="(min-width: 751px)" srcset="<?php echo $desktop;?>"  />
	   <source media="(max-width: 751px)" srcset="<?php echo $mobile;?>"  />
	   <img src="<?php echo $desktop;?>" alt="backup" />
   </picture>

</div>
</section>

