<?php if ( $read_more_links ) : ?>
    <div class="readmore">
        <h4>Read more</h4>
        <?php while( have_rows('read_more_links') ) : the_row(); ?>
            <?php $link = get_sub_field('link');
            if( $link ): 
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                ?>
               <span><a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a></span>
            <?php endif; ?>
        <?php endwhile; ?>
       </div>
<?php endif; ?>