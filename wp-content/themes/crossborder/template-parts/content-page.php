<?php
/**
 * The template used for displaying page content in page.php
 */
?>
			<?php //the_content(); ?>
<!-- Content page -->
<?php
	$settings = null;
	$settings = get_field('header_settings');	
	
	$style='';
	$imageClass='';
	$bgClass =' no-bg';
	if($settings['background_colour'] && $settings['background_colour'] != '#000000'){
		$style = ' style="background-color:' . $settings['background_colour'] . '"';
		$bgClass =' has-bg';
	}
	$desktop = null;
	$mobile = null;
	if($settings['desktop_image']) $desktop = img_acf($settings['desktop_image'], 'hero','url');
	if($settings['mobile_image']) $mobile = img_acf($settings['mobile_image'], 'hero','url');
	
	
	if($desktop == null && $mobile == null) $imageClass =' no-img';
	
	$tagline = 	null;
	$title = 	null;
	$text = 	null;
	$tagline = get_field('header-tagline');
	$title = get_field('header-title');
	$text = get_field('header-text');
	$link = get_field('header-link');
	
	if(get_the_content() || $text || $title):	//settings
	?>
	
<section id="post-<?php the_ID(); ?>" class="section--module wordpress-content fades module--header align-<?php echo  e_acf($settings['alignment']).$bgClass.$imageClass; ?>">
	
	<div class="content">
		<div class="content--inner"<?php echo $style;?>>
		<div class="copy">
		<?php  echo the_content(); ?>
			<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
			<?php if($title) 	echo '<h1>'.$title.'</h1>';?>
			<?php if($text) 	echo $text;?>
			<?php if($link)		echo '<a href="'.$link['url'].'" class="cta">' . $link['title'] . '</a>'; ?>
			
			
			
		</div>
		</div>
	</div><!-- .entry-content -->
	
	<?php if($desktop || $mobile):?>
	
	<picture>        
		   <source media="(min-width: 750px)" srcset="<?php echo $desktop;?>"  />
		   <source media="(max-width: 750px)" srcset="<?php echo $mobile;?>"  />
		   <img src="<?php echo $desktop;?>" alt="backup" />
   
   </picture>
	<?php endif;?>
</section><!-- #post-## -->

<?php  endif;?>
