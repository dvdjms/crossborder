<?php
	
	$items =		null;
	$items =		get_sub_field('list_items');
	$count = 		0;

 if ($items): ?>
   	<section class="section--numbered-list">
   		<div class="content listing">
   		<?php while( the_repeater_field('list_items') ): 
	   		
	   		$count ++;
   			$t 	= null;
   			$ic = null;
   			$ec = null;
   			$hn = null;

   			$t 	= get_sub_field('title');
   			$ic = get_sub_field('intro_copy');
   			$ec = get_sub_field('expanded_copy');
   			$hn = get_sub_field('hide_number');
   			
   			?>
   			
   			<article class="list-item">
				<h1>
				<?php if(!$hn) echo '<span>0' . $count . '.<br></span>';?>
				<?php echo $t; ?>
				</h1>
				
				<?php if($ic) echo '<div class="copy copy-intro">' . $ic . '</div>'; ?>
				
				<?php if($ec) echo '<div class="copy copy-expanded">' . $ec . '</div><a class="list-toggle"></a>'; ?>
				
			</article>


   <?php 	endwhile; ?>
      		</div>
   	</section>
  <?php  endif; ?>



	

