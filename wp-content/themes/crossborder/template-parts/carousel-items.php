<?php	
	global $numCarousel;
	
	if(!is_numeric($numCarousel)) {
		$numCarousel = 1;
		} else{
		$numCarousel++;
	}
	
	if(!isset($hide_item)) $hide_item = 0;
	
	$items=		null;
	$items = get_sub_field('items');
	$navcount = 0;
	$count = 0;
	
	if ($items): ?>
		   <div class="carousel-pagination">
		   		<?php while( the_repeater_field('items') ):
			   		$navcount ++;
			   		
			   		if($hide_item != $navcount):
			   			$t = null;
			   			$t = get_sub_field('title');
			   			echo '<button class=""><span>' . $t . '</span></button>';
		   			endif;  endwhile; ?>
		   </div>
		<?php  endif; 
	        
		if ($items): $itemscount = count($items);?>
		   <div id="carousel-<?php echo $numCarousel;?>"  class="carousel">
		   		<?php while( the_repeater_field('items') ):
			   		$count ++;
			   		
			   		if($hide_item != $count):
			   		
			   		$t = 	null;
			   		$tx =	null;
		   			$i = 	null;
		   			$l = 	null;
		   			$img = 	null;
		   			
		   			$t = 	get_sub_field('title');
		   			$tx = 	get_sub_field('text');
		   			$i = 	get_sub_field('image');
		   			$l = 	get_sub_field('link');
			   		
			   		
		   		?>
		   		<div class="item driver">
					<div class="copy">
						<?php if($t) echo '<h3>' . $t . '</h3>';?>
						<?php if($tx) echo  $tx;?>
						<?php if($l) echo link_acf($l, 'cta'); ?>	
					</div>
					<div class="image">
						<?php if($i):
							$img = getImage($i,'large');
							echo $img['lazy'];
							endif?>
					</div>
				</div>
				<?php  endif; ?>
		   <?php endwhile; ?>
		   </div>
		<?php  endif; ?>