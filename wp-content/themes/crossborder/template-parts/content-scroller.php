<?php
	$desktop = null;
	$mobile = null;
	$settings = get_field('images', 'options');	
	
	$tagline = 	null;
	$title = 	null;
	$text = 	null;
	
	$tagline = get_field('scroller-tagline', 'options');
	$title = get_field('scroller-title', 'options');
	$text = get_field('scroller-text', 'options');

	
	if($settings['desktop_image']) $desktop = img_acf($settings['desktop_image'], 'hero','url');
	if($settings['mobile_image']) $mobile = img_acf($settings['mobile_image'], 'hero','url');

	$noResults = 'No results match your search';
	if(get_field('no_results_text','options')) $noResults = get_field('no_results_text','options');
	
	
	$initials = array();

	?>
<section class="section--countries fades">
<div class="content">
	
	<div class="copy">
	<?php if(get_field('intro_paragraph','options')):
		echo get_field('intro_paragraph','options');
		else: ?>
		<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
		<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
		<?php if($text) 	echo $text;?>
		<?php endif;?>
		<form id="search-countries">
		
		<input type ="text" class="country-search" placeholder="Search for a country"/>
		<button class="close-input"></button>
		</form>
		
	</div>
	<?php if(($desktop || $mobile)):?>
	
	<picture>        
		   <source media="(min-width: 750px)" srcset="<?php echo $desktop;?>"  />
		   <source media="(max-width: 750px)" srcset="<?php echo $mobile;?>"  />
		   <img src="<?php echo $desktop;?>" alt="backup" />
   
   </picture>
	<?php endif;?>
	
	
	<div id="country-scroller">
		
		<div class="countries">
		 <a id="top-target">
		 <?php 
			 $initial = null;
			 $count = 0;
			 
			 
			 $posts = get_posts(array(
				 'post_type'=> 'country',
				
				 'orderby' => 'title',
				 'order' => 'ASC',
				 'numberposts' => -1
	
				));	
			

			foreach ($posts as $post): setup_postdata($post);
				 $count++;
				 
				 $title = null;
				 $img 	= null;
				 $rp 	= null;
				 $lu 	= null;
				 $icon 	= null;
				 $currentClass = '';
				 
				 $title = get_the_title();
				 $icon 	= get_field('icon');
				 $rp 	= get_field('risk_profile');
				 $lu 	= get_field('last_updated');
				 if($icon) $img = getImage($icon,'medium');
				 
				 
				 $init = $title[0];
				
				 if($initial != $init)  {
					 if($initial != null) echo '</dl>';
					 echo '<a id="target-'.strtolower($init).'" class="target"></a><dl> <dt><h2>'.strtoupper($init).'</h2></dt>';
					  $initial = $init;
					  array_push( $initials, $init);
					 
				 }
				 
				 if(isset($cName) &&  strtolower($cName) == strtolower($title)) $currentClass = ' class= "current-country"';
								 
				 echo '<dd data-name="'.strtolower($title).'"'.$currentClass.'><a class="country-link" href="'.get_the_permalink().'"><h3>'.$title.'</h3>';
				 echo '<p>Risk: '.$rp.' <br/> Deadline: '.$lu.'</p>';
				 echo '<span class="cta" href="'.get_the_permalink().'">Find out more</span>';
				 echo '<div class="image">'.$img['lazy'].'</div>';
				 echo '</a></dd>';
			 
			 
			 endforeach;
			 wp_reset_postdata();
			 echo '</dl>';

			 
/*
			 foreach($countries as $country):
				 $count++;
				 $init = $country[0];
				 $currentClass = '';
				 
				 
				 if($initial != $init)  {
					 if($initial != null) echo '</dl>';
					 echo '<a id="target-'.strtolower($init).'" class="target"></a><dl> <dt><h2>'.strtoupper($init).'</h2></dt>';
					  $initial = $init;
					  array_push( $initials, $init);
					 
				 }
				 
				 $img = get_template_directory_uri().'/placeholders/countries/australia.svg';
				 if($count % 2 == 0) $img = get_template_directory_uri().'/placeholders/countries/austria.svg';
				 if($count % 3 == 0) $img = get_template_directory_uri().'/placeholders/countries/belgium.svg';
				 
				 if(isset($cName) &&  strtolower($cName) == strtolower($country)) $currentClass = ' class= "current-country"';
				 
				 echo '<dd data-name="'.strtolower($country).'"'.$currentClass.'><h3>'.$country.'</h3>';
				 echo '<p>Risk: Medium <br/> Updated: September</p>';
				 echo '<a class="cta" href="'. esc_url( home_url() ).'/country/australia">Find out more</a>';
				 echo '<div class="image"><img src="'.$img.'" /></div>';
				 echo '</dd>';
			 
			 endforeach;
			
			 echo '</dl>';
*/

		 ?>
		 <div class="no-results-text">
		  <h4><?php echo $noResults; ?></h4>
		 </div>
	
		  
		</div>
		
		
		<div class="alphabet">
			<?php 
				
				$alphas = range('A','Z');
				$count	=	0;
				
				
				foreach($alphas as $letter){
					if (in_array($letter, $initials)){
						if($count==0) echo '<a data-easing="linear" href="#target-'.strtolower($letter).'" class="active">' . $letter . '</a>';
						if($count>0) echo '<a data-easing="linear" href="#target-'.strtolower($letter).'">' . $letter . '</a>';
					} else {
						echo '<a class="disabled">' . $letter . '</a>';
					}
					$count++;
				}
			?>
		</div>
		
		
		
		
	</div>
</div>
</section>





