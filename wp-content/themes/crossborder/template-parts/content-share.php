<?php
/**
 * The template used for displaying share bar 
 */

?>

<div class="share-bar">
	
	<div class="bar">
	<?php $link = get_page_link();?>
		<!-- facebook -->
<!--
		<a class="share_facebook" href="http://www.facebook.com/sharer.php?u=<?php echo $link; ?>" target="_blank">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path class="a" d="M25,19.61H21.92v11H17.34v-11H15.16V15.71h2.18V13.2c0-1.8.86-4.62,4.62-4.62h3.39v3.78H22.89a0.93,0.93,0,0,0-1,1.06v2.29h3.49Z"/></svg>
		</a>
-->
		<!-- twitter -->		
		
		<a class="share_twitter" href="http://twitter.com/share?url=<?php echo $link;?>&amp;text=<?php echo get_the_title(); ?>+" target="_blank">
<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path d="M28.79,16.32c0,0.18,0,.37,0,0.55A12.06,12.06,0,0,1,10.24,27a8.66,8.66,0,0,0,1,.06,8.51,8.51,0,0,0,5.27-1.82,4.24,4.24,0,0,1-4-2.95,4.18,4.18,0,0,0,.8.08,4.25,4.25,0,0,0,1.12-.15,4.24,4.24,0,0,1-3.4-4.16V18a4.23,4.23,0,0,0,1.92.53,4.25,4.25,0,0,1-1.31-5.66,12,12,0,0,0,8.74,4.43,4.24,4.24,0,0,1,7.23-3.87,8.48,8.48,0,0,0,2.69-1,4.26,4.26,0,0,1-1.86,2.35,8.43,8.43,0,0,0,2.43-.67,8.61,8.61,0,0,1-2.11,2.2"/></svg>
		</a>
		
		<!--  pinterest -->
<!--
		<a class="share_pinterest" data-site="pinterest" href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','//assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><defs><style>.a{fill:#fff;}</style></defs><title>share-sprites</title><path class="a" d="M22.06,26.06c-1.44-.11-2-0.82-3.17-1.51-0.62,3.26-1.38,6.38-3.63,8-0.69-4.92,1-8.62,1.81-12.54-1.36-2.28.16-6.87,3-5.74,3.52,1.39-3,8.48,1.36,9.37,4.6,0.92,6.48-8,3.63-10.88-4.12-4.18-12-.09-11,5.89,0.24,1.46,1.75,1.91.61,3.93C12,22,11.23,19.92,11.33,17.15c0.16-4.54,4.08-7.72,8-8.16,5-.56,9.63,1.82,10.27,6.5,0.72,5.27-2.24,11-7.56,10.58"/></svg>
</a> 
-->
		<!-- google -->
<!--
		<a class="share_google" href="https://plus.google.com/share?url=<?php echo $link; ?>" target="_blank">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path d="M32.69,19.81H30v2.73h-1.9V19.81H25.42v-1.9h2.73V15.27H30v2.65h2.65v1.9ZM16.25,26.94A7.66,7.66,0,1,1,21.4,13.62c-1.6,1.51-1.55,1.6-2.18,2.23a4.33,4.33,0,0,0-3-1.12,4.55,4.55,0,0,0,0,9.1c2.48,0,3.49-1.07,4.14-3H16.25v-3H23.6c0.51,3.67-1,9.17-7.35,9.17"/></svg>
		</a>
		
-->
		<!--<a class="share_email" href="mailto:?subject=<?php echo get_the_title(); ?>&amp;body=%20<?php echo $link; ?>">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path d="M29.8,13.43H10.2a0.7,0.7,0,0,0-.7.7v12.3a0.7,0.7,0,0,0,.7.7H29.8a0.7,0.7,0,0,0,.7-0.7V14.13A0.7,0.7,0,0,0,29.8,13.43Zm-18.9,2,6.27,4.65L10.9,25.18V15.42ZM20,20.64l-7.82-5.81H27.82Zm-2,.08,1.59,1.18a0.61,0.61,0,0,0,.73,0L22,20.68h0l6.11,5H11.89ZM22.88,20l6.22-4.62v9.75Z"/></svg>
		</a>-->
		
<!-- Linkedin-->
<a  class="share_linkedin"  href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path d="M12.14,12.37a2,2,0,0,0,2,2,2.17,2.17,0,0,0,2.07-2,2,2,0,0,0-2-2h0a2,2,0,0,0-2,2h0m4,3.41H12.72c-0.43,0-.57,0-0.57.54V27.66H16.1V15.74Zm14.4,12V20.28a4,4,0,0,0-3-4.11,6.31,6.31,0,0,0-6.22.83V15.8H17.42V27.74h3.85V20.26a1.48,1.48,0,0,1,.33-0.85,2.83,2.83,0,0,1,2.57-.91,2.17,2.17,0,0,1,2.17,2.17v7.07h4.15Z"/></svg></a>


	<!--		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><defs><pathd="M28.62,12H11.38a1.83,1.83,0,0,0-1.77,1.84s0,7.23,0,7.25C9.72,26.7,14.33,30.27,20,30.27s10.28-3.55,10.39-9.15c0,0,0-7.28,0-7.28A1.82,1.82,0,0,0,28.62,12Zm-2.21,7.65-5.49,4.78a1.41,1.41,0,0,1-1.86,0l-5.48-4.78a1.41,1.41,0,1,1,1.86-2.13l4.56,4,4.56-4A1.41,1.41,0,1,1,26.41,19.65Z"/></svg> -->


<!-- <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 40 40"><path d="M12.14,12.37a2,2,0,0,0,2,2,2.17,2.17,0,0,0,2.07-2,2,2,0,0,0-2-2h0a2,2,0,0,0-2,2h0m4,3.41H12.72c-0.43,0-.57,0-0.57.54V27.66H16.1V15.74Zm14.4,12V20.28a4,4,0,0,0-3-4.11,6.31,6.31,0,0,0-6.22.83V15.8H17.42V27.74h3.85V20.26a1.48,1.48,0,0,1,.33-0.85,2.83,2.83,0,0,1,2.57-.91,2.17,2.17,0,0,1,2.17,2.17v7.07h4.15Z"/></svg> -->
	</div><!-- bar -->
 
</div>