<?php
	$post_id = get_the_ID();
	
?>

<article class="item driver entry--job">
<a href="<?php echo get_the_permalink();?>" class="mobile-cta"></a> 
	
		<span class="mobile-touch"></span>
		<div class="title">
			<?php  echo '<h3>' . get_the_title() . '</h3>';?>
			<div class="categories"> 
			<?php  if (function_exists('list_primary_term')):
				echo list_primary_term($post_id, 'location-category',false); 
				endif; ?> 
			</div>
			
		
			
		</div>
		<div class="copy">
		
			
			
		
			<?php xborder_posted_on()?>
			<?php echo subString(get_the_excerpt(),120);?>
			
		</div>
		
</article>
