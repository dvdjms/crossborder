<?php
/**
 * The template used for displaying page content in sinlgle-article.php
 */
	

?>

<section id="post-<?php the_ID(); ?>" class="child-content post-content">
	<div class="content fades">
        <div class="content-wrapper">
           
            <div class="copy">
            <?php $type = get_post_type();
               $type = $type .'s';
            ?>
                <h4><a href="<?php echo get_the_permalink(get_page_by_path( $type  ));?>">&lsaquo; Back to <?php echo $type;?></a></h4>
                <?php the_title('<h1>','</h1>');?>
                <?php if(get_field('show_date')) xborder_posted_on()?>
                <?php the_content(); ?>
            </div><!--end copy-->	
            
        </div>
	</div><!-- .entry-content -->
	
</section><!-- #post-## -->