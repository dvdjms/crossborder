
<?php if(get_field('signoff-image') || get_field('signoff-text')):
	
	if(get_field('background_colour')) $bgc = ' style="background-color:'.get_field('background_colour').';"';
?>
<section class="section--sign-off fades"<?php if($bgc) echo $bgc;?>>
	<div class="content">
	<?php
		$img = null;
		$i = get_field('signoff-image');
		$t = get_field('signoff-text');
		
		if($i) $img = getImage($i, 'medium');
		
		if($img) echo '<div class="image">'.$img['lazy'].'</div>';
		if($t) echo '<div class="copy">' . $t . '</div>';
		?>		
		
		</div>
</section>
<?php endif; ?>