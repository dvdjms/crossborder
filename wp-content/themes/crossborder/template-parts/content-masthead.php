<?php
//masthead

    $masthead = get_field('masthead') ? get_field('masthead') : null;	
	$intro_tagline = 	get_field('intro_tagline') ? get_field('intro_tagline') : null;
	$intro_title = 	get_field('intro_title') ? get_field('intro_title') : null;
    $intro_paragraph = 	get_field('intro_paragraph') ? 	get_field('intro_paragraph') : null;
    $panels = 	get_field('panels') ? 	get_field('panels') : null;

	$desktop =  $masthead['desktop_image'] ? img_acf($masthead['desktop_image'], 'hero','url') : null;
    $mobile =   $masthead['mobile_image'] ? img_acf($masthead['mobile_image'], 'hero','url') : null; 
    $mh_title =   $masthead['mh_title'] ? $masthead['mh_title'] : null; 
    $mh_text =   $masthead['mh_text'] ? $masthead['mh_text'] : null; 
    $buttons_title =   $masthead['buttons_title'] ? $masthead['buttons_title'] : null; 
    $link =   $masthead['first_link'] ? $masthead['first_link'] : null; 
    $link2 =   $masthead['second_link'] ? $masthead['second_link'] : null; 


    ?>
   
	
 <section class="section--module fades align-left module-masthead">

    <div class="content">
        <!-- <div class="content--inner"> -->
             <div class="copy">
                    <?php echo $mh_title ?'<h1>'.$mh_title.'</h1>' :  '';?>
                    <?php echo $mh_text ? $mh_text : ''; ?>
                    <?php echo $buttons_title ? '<p class="buttons_title">' . $buttons_title . '</p>' : ''; ?>
                    <?php echo $link ? '<a href="'.$link['url'].'" class="cta">' . $link['title'] . '</a>' : ''; ?>
                    <?php echo $link2 ? '<a href="'.$link2['url'].'" class="cta second">' . $link2['title'] . '</a>' : ''; ?>
                </div>
        </div>	
        <?php if($desktop || $mobile):?>
        <picture>        
               <source media="(min-width: 751px)" srcset="<?php echo $desktop;?>"  />
               <source media="(max-width: 751px)" srcset="<?php echo $mobile;?>"  />
               <img src="<?php echo $desktop;?>" alt="backup" />
       </picture>
        <?php endif;?>
    <!-- </div> -->
 </section>
    <div class="fades">
<section class="intro-paragraph fades">
<div class="content">
            <div class="copy">
            <?php if($intro_tagline) 	echo '<h4>'.$intro_tagline.'</h4>';?>
                    <?php if($intro_title) 	echo '<h2>'.$intro_title.'</h2>';?>
                    <?php if($intro_paragraph) 	echo $intro_paragraph;?>
            </div>
</div>

</section>

 <?php if($panels): $c = count($panels);?>
    <section class="section--panels num-<?php echo $c;?>">

        <div class="panel-drivers content">
                <?php while( the_repeater_field('panels') ):
                $panel_title = 	get_sub_field('panel_title') ? get_sub_field('panel_title') : null;
                $panel_text = 	get_sub_field('panel_text') ? get_sub_field('panel_text') : null;
                $panel_link = 	get_sub_field('panel_link') ? get_sub_field('panel_link') : null;
                $panel_background = get_sub_field('panel_background') ? get_sub_field('panel_background') : null;
                $dark_text = 	get_sub_field('dark_text') ? get_sub_field('dark_text') : null;

                $style = $panel_background ? ' style="background-color:' . $panel_background . '"' : null;
                
                ?>
                <a class="panel<?php echo $dark_text ? ' dark_text' : '';?>" 
                <?php echo $panel_link  ? 'href="' . $panel_link['url'] . '" target = "' .$panel_link['target']. '"' :  '';?>
                <?php echo $style  ? $style :  '';?>
                >
                <?php echo $panel_title  ?'<h3>'.$panel_title .'</h3>' :  '';?>
                <?php echo $panel_text  ?'<p>'.$panel_text .'</p>' :  '';?>
                </a>

            <?php 	endwhile; ?>
                                    
        </div>
                </section>
                </div>
<?php endif;?>