<?php
	
	$intro =		null;
	$people  =		null;
	$count = 1;
	
	$people =		get_sub_field('team_members');
	
	if ($people) $count = count($people);	
?>



	
<section class="section--people fades">

<div class="content">
	<div class="intro copy">
			<?php echo e_acf(get_sub_field('team-tagline'),'h4');?>
			<?php echo e_acf(get_sub_field('intro'), 'h2');?>
	</div>

	<?php if($people ): ?>
    <div class="people columns column--<?php echo $count;?>">
    
    <?php foreach( $people as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); 
	        
	        	$p = null;
	   			$i =  null;
	   			$img = null;
	   			$position = '';
	   			
	   			$p = get_field('position');
	   			$i = get_field('headshot');
	   			
	   			if($i) $img = getImage($i, 'large');
	   			if($p) $position = '<span class-"pos">' . $p . '</span>';
	   			
        ?>
        
        <div class="column card card--person">
	   				<?php if($img) echo '<a href="'.get_the_permalink().'" class="image">' .$img['lazy'] .'</a>';?>
	   			
	   				<div class="copy">
	   					<?php echo '<h3>' . get_the_title() . '</h3>';?>
	   					<?php echo '<p>' .  $position . '</p>';?>
	   				</div>
	   				
	   	</div>

    <?php endforeach; ?>
   
    </div>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>
	
</div>

</section>

