<?php
/**
 * ACF flexible content - Content blocks
 */

if(get_field('content_blocks')):  


while ( have_rows('content_blocks') ) : the_row();
/*-----------------------------------------------------------------------------
	
		Content

---------------------------------------------------------------------------*/ 
	



	
	if( get_row_layout() == 'section_module')include(get_template_directory().'/template-parts/content-module.php');
	
	if( get_row_layout() == 'story_module')include(get_template_directory().'/template-parts/content-story-module.php');
	
	if( get_row_layout() == 'carousel')include(get_template_directory().'/template-parts/content-carousel.php');
	
	if( get_row_layout() == 'team')include(get_template_directory().'/template-parts/content-people.php');

	if( get_row_layout() == 'locations')include(get_template_directory().'/template-parts/content-locations.php');
	
	if( get_row_layout() == 'latest_podcast')include(get_template_directory().'/template-parts/content-driver-podcast.php');
	
	if( get_row_layout() == 'quotes')include(get_template_directory().'/template-parts/content-employees.php');
	
	if( get_row_layout() == 'country_scroller')include(get_template_directory().'/template-parts/content-scroller.php');
	
	if( get_row_layout() == 'country_hub_module')include(get_template_directory().'/template-parts/content-country-module.php');
	
	
	endwhile; // end content sections while

endif; // get content sections ?>	








