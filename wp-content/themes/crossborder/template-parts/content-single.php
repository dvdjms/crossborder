<?php
/**
 * The template used for displaying page content in page.php
 */
	
$img = null;

if(get_the_content()):

if(has_post_thumbnail()):
	$img = get_the_post_thumbnail($post->ID,'hero');

endif;




$mycontent = $post->post_content;
$word = str_word_count(strip_tags($mycontent));
$m = floor($word / 200); $s = floor($word % 200 / (200 / 60));
if ($s >= 30) {
$m = ($m + 1);
}
$est = $m . ' minute' . ($m == 1 ? '' : 's'); 




?>

<section id="post-<?php the_ID(); ?>" class="child-content post-content">
	<div class="content">
	<div class="content-wrapper">
	<h4>News</h4>
	<?php the_title('<h1>','</h1>');?>
	
	<?php if ($est >= 1) { echo '<h4>' . $est , ' read</h4>'; } else { echo '<h4>Less than a minute to read</h4>'; } ?>
	
	<?php if($img) echo '<div class="image fades">' . $img . '</div>'; ?>
	 

	
	<div class="copy">
		
		<?php the_content(); ?>
	</div><!--end copy-->		

	</div>
	</div><!-- .entry-content -->
	
</section><!-- #post-## -->



<section class="section--carousel fades type-news">	
    <div class="content">
		<div class="intro-wrapper">
			<div class="copy intro">
				<h4>News</h4>
				<h2>If it's worth sharing, it's here</h2>
			</div>
		</div>
		<div class="carousel-wrapper">
	   		
		<?php include(get_template_directory().'/template-parts/carousel-news.php');?>
		  		
		<?php include(get_template_directory().'/template-parts/carousel-buttons.php');?>
<!--
		<div class="pagination">
			<a class="prev-person">prev</a>
			<a class="next-person">next</a>
		</div>
	
-->
		</div>
	</div>

</section>

<?php endif;?>