<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 */



?>

		


<div class="content-blocks">	
<section class="section--none">
<div class="content fades">
	<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'xborder' ); ?></h1>
		<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php printf( wp_kses( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'xborder' ), array( 'a' => array( 'href' => array() ) ) ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'xborder' ); ?></p>
			

		<?php else : ?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for.', 'xborder' ); ?></p>
		

		<?php endif; ?>
</div>
</section>
</div>