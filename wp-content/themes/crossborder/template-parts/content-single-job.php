<?php
/**
 * The template used for displaying page content in page.php
 */
	
$img = null;

if(get_the_content()):

if(has_post_thumbnail()):
	$img = get_the_post_thumbnail($post->ID,'hero');

endif;




//$mycontent = $post->post_content;




?>

<section id="post-<?php the_ID(); ?>" class="child-content post-content fades">
	<div class="content">
	<div class="content-wrapper">
	<h4>Current opportunities</h4>
	<?php the_title('<h1>','</h1>');?>

	<h1 class="title-location">
	<?php if (function_exists('list_primary_term')):
		echo list_primary_term($post->ID, 'location-category',false); 
		endif; ?>
	</h1>
	
	
	<?php if($img) echo '<div class="image fades">' . $img . '</div>'; ?>
	 

	
	<div class="copy">
		
		<?php the_content(); ?>
	</div><!--end copy-->		

	</div>
	</div><!-- .entry-content -->
	
</section><!-- #post-## -->



<?php endif;?>