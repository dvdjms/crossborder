<section class="section--driver fades">
	<div class="content">
		
		<div class="copy">
		<?php $tagline = 	null;
				$title = 	null;
				$text = 	null;
				$tagline = get_sub_field('podcast-tagline');
				$title = get_sub_field('podcast-title');
				$text = get_sub_field('intro_text');
				$link = get_sub_field('podcast-link'); 
				
				
				if($tagline) 	echo '<h4>'.$tagline.'</h4>';
				if($title) 	echo '<h2>'.$title.'</h2>';
				if($text) 	echo $text;
				if($link)	echo '<a href="'.$link['url'].'" class="cta">' . $link['title'] . '</a>'; ?>
		</div>
		<?php 
		
		$args = array( 'posts_per_page' => 1, 'post_type'=>'podcast' );
		$podcasts = get_posts( $args );
		
			foreach ( $podcasts as $post ) : setup_postdata( $post );
					$i = 	null;
		   			$img = 	null;
		   		
		   			$i = 	get_field('image');
		   			if($i) $img = getImage($i,'square');
			?>
			<article class="driver driver--podcast">
				<div class="copy">
						<?php
						if(get_field('episode_number')) echo '<h4>Episode '.get_field('episode_number').'</h4> ';
	
					  	echo '<h3>' . get_the_title() . '</h3>';
						echo subString( get_field('description'),130);?>
				</div>
				
				<div class="image-holder">
				
						<div class="image">
							<?php if($img):
										echo $img['img'];
										 else:?>
										<img src="<?php echo get_template_directory_uri();?>/placeholders/podcast.png" alt="podcast image"/>
										<?php endif;?>
		
						</div>

				<?php	if(get_field('itunes_link')) echo '<a href="'.get_field('itunes_link').'" class="cta invert itunes">Listen on iTunes</a>';
						if(get_field('spotify_link')) echo  '<a href="'.get_field('spotify_link').'" class="cta spotify">Listen on Spotify</a>';?>
					
				</div>
		</article>
			<?php endforeach; 
			wp_reset_postdata();?>

	
	</div>
</section>
