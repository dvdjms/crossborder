<section class="section--employees fades">
	<div class="content">
		<div class="content--inner">
				<div class="intro copy">
	<?php echo e_acf(get_sub_field('quotes-tagline'),'h4');?>
	<?php echo e_acf(get_sub_field('quotes-intro'), 'h2');?>
	
	</div>

	  <?php  if (get_sub_field('quotes')): $itemscount = count(get_sub_field('quotes'));?>
		  <div class="carousel" id="carousel-employees" data-loop=true>
		 <?php while( the_repeater_field('quotes') ):
		    			 
		    			
			   			$i =  null;
			   			$img = null;
			   			$q = null;
			   		
			   			
			   			$q = get_sub_field('quote_text');
			   			$i = get_sub_field('quote_image');
			   			
			   			if($i) $img = getImage($i, 'large');
			   			
			   			?>
			   			<div class="item--employee">
			   			<?php if($img):?>
			   				<div class="employee-image"><div class="image"><?php echo $img['img'];?></div></div>
			   			<?php endif;
			   			
			   			if($q):?>
			   				<div class="quote"><?php echo $q;?></div>
			   			<?php endif;?>
			   			</div>
			   				   			
			   							   	
		    <?php endwhile; ?>
			</div>
			 <div class="carousel-pagination dots">
		   		<?php for($i=0; $i < $itemscount; $i++ ):
			   	
			   		echo '<button class="dot"></button>';
		   			 endfor; ?>
		   	</div>
		
		   <?php  endif; ?>

	
		<?php if($itemscount != 1) include(get_template_directory().'/template-parts/carousel-buttons.php');?>

		  

		</div>
		
	</div>
	
</section>


