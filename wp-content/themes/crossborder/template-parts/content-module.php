<?php
	//settings
	$settings = null;
	$stacking = null;
	$type = null;
	$podcast = null;
	
	$tagline = 	null;
	$title = 	null;
	$text = 	null;
	$link = 	null;
		
	$style='';
	
	$type = 	get_sub_field('type');	
	$settings = get_sub_field('settings');	
	$stacking = get_sub_field('image_stack');
	$bgClass =' no-bg';
	
	if($settings['background_colour'] && $settings['background_colour'] != '#000000'){
		$style = ' style="background-color:' . $settings['background_colour'] . '"';
		$bgClass =' has-bg';
	}
	$desktop = null;
	$mobile = null;


	$tagline = 	get_sub_field('module-tagline');
	$title = 	get_sub_field('module-title');
	$text = 	get_sub_field('text');
	$link = 	get_sub_field('module-link');
	$link2 = 	get_sub_field('second_module-link');

	if($settings['desktop_image']) $desktop = img_acf($settings['desktop_image'], 'hero','url');
	if($settings['mobile_image']) $mobile = img_acf($settings['mobile_image'], 'hero','url');
	
	
	if($type=="podcast"):
	 	$post = get_sub_field('choose_podcast');
	 	$podcast_copy ='';
	 	setup_postdata( $post ); 
		  	$podcast_copy .= '<h2>'; 
		  	if(get_field('episode_number')) $podcast_copy .= 'Episode '.get_field('episode_number').': ';
		  	$podcast_copy .= get_the_title() . '</h2>';
			$podcast_copy .=  get_field('description');
		
			if(get_field('itunes_link')) $podcast_copy .= '<a href="'.get_field('itunes_link').'" class="cta cta-invert">Listen to episode on iTunes</a>';
			if(get_field('spotify_link')) $podcast_copy .=  '<a href="'.get_field('spotify_link').'" class="cta">Listen to episode on Spotify</a>'; 
		wp_reset_postdata();
	
	endif;
	 
	if($type=="latest_podcast"):
				$svg = get_template_directory().'/placeholders/radio-tower.svg';

		$args = array( 'posts_per_page' => 1, 'post_type'=>'podcast' );
		$podcasts = get_posts( $args );
		$podcast_copy ='<h4>CHECK OUT MY LATEST PODCAST</h4>';
			foreach ( $podcasts as $post ) : setup_postdata( $post ); 
					$podcast_copy .= '<h2>'; 
				  	if(get_field('episode_number')) $podcast_copy .= 'Episode '.get_field('episode_number').': ';
				  	$podcast_copy .= get_the_title() . '</h2>';
					$podcast_copy .=  get_field('description');
		
					if(get_field('itunes_link')) $podcast_copy .= '<a href="'.get_field('itunes_link').'" class="cta invert itunes">Listen on Apple Podcasts</a>';
					if(get_field('spotify_link')) $podcast_copy .=  '<a href="'.get_field('spotify_link').'" class="cta spotify">Listen on Spotify</a>';?>
				
			<?php endforeach; 
			wp_reset_postdata();?>
	<?php endif;	
	?>



	
<section class="section--module fades align-<?php echo  e_acf($settings['alignment']).$bgClass; ?><?php if($stacking) echo ' stacking'; ?><?php if($type) echo ' type-'.$type; ?>">

<div class="content">
	<div class="content--inner"<?php echo $style;?>>
		
		

		<?php if($type == "podcast"):?>
			<div class="copy">
			
			<?php echo $podcast_copy;?>
			</div>
		
		<?php elseif($type == "latest_podcast"):?>
			<div class="copy">
			<?php echo $podcast_copy;?>
			</div>
		<?php else:?>
			<div class="copy">
		
				<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
				<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
				<?php if($text) 	echo $text;?>
				<?php if($link)		echo '<a href="'.$link['url'].'" class="cta">' . $link['title'] . '</a>'; ?>
				<?php if($link2)		echo '<a href="'.$link2['url'].'" class="cta second">' . $link2['title'] . '</a>'; ?>
				
			
			</div>
		<?php endif;?>
		
		<?php if($stacking):?>
			<div class="stack">
				  <?php  if (get_sub_field('stack_images')): ?>
				   		
				   		<?php while( the_repeater_field('stack_images') ):
				   			 
				   			$i = get_sub_field('image');
				   			$img = getImage($i, 'large');
				   			?>
				   			<div class="image"><?php echo $img['lazy'];?></div>

				   <?php 	endwhile; ?>
				  <?php  endif; ?>						 
			</div>
		
		
		<?php endif;?>
	
	
	
	</div>	





	<?php if(($desktop || $mobile) && $type != "latest_podcast" ):?>
	
	<picture>        
		   <source media="(min-width: 751px)" srcset="<?php echo $desktop;?>"  />
		   <source media="(max-width: 751px)" srcset="<?php echo $mobile;?>"  />
		   <img src="<?php echo $desktop;?>" alt="backup" />
   
   </picture>
	<?php endif;?>
	<?php if($type == "latest_podcast"):?>
		<?php include ($svg);?>
	<?php endif;?>
</div>
</section>

