
<?php 
global $numCarousel;
	
	if(!is_numeric($numCarousel)) {
		$numCarousel = 1;
		} else{
		$numCarousel++;
	}

	
	$podcasts =		null;
	$args = array( 'posts_per_page' => 6, 'post_type'=>'podcast' );
	$podcasts = get_posts( $args );

	
	if ($podcasts): ?>
		   <div class="carousel-pagination">
		   		<?php foreach( $podcasts as $post): setup_postdata($post);
		   			$en = '';
		   			if(get_field('episode_number')) $en ='Episode '.get_field('episode_number').': ';
		   			echo '<button class=""><span>' .$en. get_the_title() . '</span></button>';
		   		endforeach; wp_reset_postdata();?>
		   </div>
	<?php  endif; 
	        
		if ($podcasts): ?>
		   <div id="carousel-<?php echo $numCarousel;?>"  class="carousel">
		   		<?php foreach( $podcasts as $post): setup_postdata($post);
			   		
			   	
			   		$tx =	null;
		   			$i = 	null;
		   			$img = 	null;
		   		
		   			$tx = 	get_field('description');
		   			$i = 	get_field('image');
		   			if($i) $img = getImage($i,'square');
		   			
			   			
		   		?>
		   		
		   		<article class="item driver driver--podcast">
						<div class="copy">
							<?php 
								$en = '';
								$name = null;
								$title = null;
								if(get_field('name')) $name =  get_field('name');
								
								if(get_field('episode_number')) $en ='Episode '.get_field('episode_number');
								
								$title = '<h4>';
								if($en)   $title.= $en;
								//if($name) $title .= ': star guest';
								$title .= '</h4>';
								
								//echo $img['url'];
							?>
							
							<?php if($title) echo $title;?>
							<?php if($name) echo '<h3>' . $name . '</h3>';?>
							<p><?php echo subString(get_field('description'),320);?> <p>
						</div>
						<div class="image-holder">
							<div class="image">
								<?php if($img):
								echo $img['img'];
								 else:?>
								<img src="<?php echo get_template_directory_uri();?>/placeholders/podcast.png" />
								<?php endif;?>
							</div>
							<?php if(get_field('itunes_link')) echo '<a href="'.get_field('itunes_link').'" class="cta invert itunes">Listen on Apple Podcasts</a>';?>
							<?php if(get_field('spotify_link')) echo '<a href="'.get_field('spotify_link').'" class="cta spotify">Listen on Spotify</a>';?>
							
						</div>
					</article>
		   		
		   		
		   		
<!--
		   		<div class="item driver">
					<div class="copy">
						<?php if($t) echo '<h3>' . $t . '</h3>';?>
						<?php if($tx) echo  $tx;?>
						<?php if($l) echo link_acf($l, 'cta'); ?>	
					</div>
					<div class="image">
						<?php if($i):
							$img = getImage($i,'large');
							echo $img['lazy'];
							endif?>
					</div>
				</div>
-->

		   <?php endforeach; wp_reset_postdata();?>
		   </div>
		<?php  endif; ?>