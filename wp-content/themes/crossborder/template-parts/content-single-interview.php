<?php
/**
 * The template used for displaying page content in sinlgle-article.php
 */
	

$img = get_field('podcast_image')	? getImage(get_field('podcast_image'), 'medium') : null;
$itunes_link = get_field('itunes_link') ? get_field('itunes_link') : null;
$spotify_link = get_field('spotify_link') ? get_field('spotify_link') : null;
?>	

<section id="post-<?php the_ID(); ?>" class="child-content post-content">
	<div class="content fades">
        <div class="content-wrapper<?php if($spotify_link ||$itunes_link) echo ' has-podcast';?>">
            
               <?php include(get_template_directory().'/template-parts/content-sidebar-podcast.php');?>
            
            <div class="copy">
            <?php $type = get_post_type();
               $type = $type .'s';
             ?>
                <h4><a href="<?php echo get_the_permalink(get_page_by_path( $type  ));?>">&lsaquo; Back to <?php echo $type;?></a></h4>
                <?php the_title('<h1>','</h1>');?>
                <?php if(get_field('show_date')) xborder_posted_on()?>
                <?php the_content(); ?>
            </div><!--end copy-->	
            
        </div>
	</div><!-- .entry-content -->
	
</section><!-- #post-## -->