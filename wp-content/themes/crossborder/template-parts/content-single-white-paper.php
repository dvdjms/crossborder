<?php
/**
 * The template used for displaying page content in sinlgle-whitepaper.php
 */
	

if(get_the_content()):

$whitepaper_cover = get_field('whitepaper_cover') ? getImage(get_field('whitepaper_cover'), 'large') : null;
$read_more_links = get_field('read_more_links') ? get_field('read_more_links') : null;
$author = get_field('author') ? get_field('author') : null;
$author_title = get_field('author_title') ? get_field('author_title') : null;
$author_image = get_field('author_image')? getImage(get_field('author_image'), 'medium') : null;
$author_bio = get_field('author_bio') ? get_field('author_bio') : null;

$download = get_field('download_link') ? get_field('download_link') : null;

?>

<section id="post-<?php the_ID(); ?>" class="child-content post-content">
	<div class="content fades">
        <div class="content-wrapper">
            <div class="cover">
            <?php  if($download) echo '<a href="'.$download.'" target=_blank>'; ?>
            <?php  echo $whitepaper_cover ?  $whitepaper_cover['lazy'] : '';  ?>
            <?php  if($download) echo '</a>';?>
            <?php  if($download) echo '<a href="'.$download.'" target=_blank class="download">Download White paper</a>'; ?>
            </div>
            <div class="copy">
            <h4><a href="<?php echo get_the_permalink(get_page_by_path( 'white-papers' ));?>">&lsaquo; Back to White papers</a></h4>
                <h1 <?php echo $author ? 'class= "has-author"' : ''?>><?php echo get_the_title();?> </h1>
                <?php if($author): 
                  
                    echo '<h4>';
                    echo $author_bio ?  '<a href="#" class="show-bio">' : '';
                    echo $author;
                    echo $author_title ? '<span>' . $author_title. '</span>' : '';
                    echo $author_bio ?  '</a>' : '';
                    echo '</h4>';
                   
                endif; ?>
                
                <div class="copy-inner">
               
                    <div class="copy">
                    <?php if(get_field('show_date')) xborder_posted_on()?>
                    <?php the_content(); ?>
                    </div>
                   

                    <?php include (get_template_directory().'/template-parts/content-readmore.php');?>
               
                 </div>
            </div><!--end copy-->	
          
        </div>
	</div><!-- .entry-content -->
	
</section><!-- #post-## -->


<div class="people-modal bio-modal">
<div class="shadow close-bio"></div>

<section class="section--person child-content" id="modal-content">
	<div class="content">
       <?php if($author_image): ?>
        <div class="image">
             <?php echo $author_image['img'];?>
        </div>		
       <?php endif;?>
		<div class="copy">
        <?php echo $author ? '<h1>' . $author .'</h1>' : ''; ?> 
        <?php echo $author_title ? ' <h4><span>' . $author_title .'</span></h4>' : ''; ?>     
        <?php echo $author_bio ? '<p>' . $author_bio . '</p>' : ''; ?>
		</div><!--end copy-->		

	</div><!-- .entry-content -->

<a class="close-people-modal close-bio"></a></section>
</div>
<?php endif;?>