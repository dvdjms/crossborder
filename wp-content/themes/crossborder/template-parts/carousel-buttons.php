<div class="button-controls">
	<button class="prev">
		<svg xmlns="http://www.w3.org/2000/svg" width="12" height="19" viewBox="0 0 12 19">
			<polyline points="10.06 17.28 2.28 9.5 10.06 1.72" style="stroke-width:3px"/>
		</svg>
	</button>
	<button class="next">
		<svg xmlns="http://www.w3.org/2000/svg" width="12" height="19" viewBox="0 0 12 19">
			<polyline points="1.94 1.72 9.72 9.5 1.94 17.28" style="stroke-width:3px"/>
		</svg>
	</button>
</div>