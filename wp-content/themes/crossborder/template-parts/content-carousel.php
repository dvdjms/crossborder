<?php 
	
	$type = 	null;
	$intro = 	'';
	$type = get_sub_field('type');
	$itemscount = null;
	
	if(get_sub_field('intro_text')) 
		$intro = '<div class="copy intro">';
		$intro .= 	e_acf(get_sub_field('carousel-tagline'),'h4');
		$intro .= 	e_acf(get_sub_field('intro_text'), 'h2');
		$intro .= 	e_acf(get_sub_field('carousel-text'));
		$intro.=  '</div>';
	
	if($type != 'intro-left') $intro ='<div class="intro-wrapper">' . $intro . '</div>';
	
?>


<section class="section--carousel fades type-<?php echo $type;?>">	
    <div class="content">
		<?php echo $intro;?>
	
	    <?php if($type == 'intro-left' || $type=='podcasts' || $type=='news' || $type=='jobs') echo '<div class="carousel-wrapper">';?>
	    

		<?php if($type == 'intro-left' || $type=='full') include(get_template_directory().'/template-parts/carousel-items.php');?>
		
		<?php if($type == 'podcasts') include(get_template_directory().'/template-parts/carousel-podcasts.php');?>
		
		<?php if($type == 'news') include(get_template_directory().'/template-parts/carousel-news.php');?>
		
		<?php if($type == 'jobs') include(get_template_directory().'/template-parts/carousel-jobs.php');?>
		  		
		
		<?php if($itemscount != 1) include(get_template_directory().'/template-parts/carousel-buttons.php');?>
	
	
	<?php if($type == 'intro-left' || $type=='podcasts' || $type=='news'  || $type=='jobs') echo '</div>'; ?>
	</div>
</section>
