<?php
	$size = 'medium-fixed';
	$thumb_id = get_post_thumbnail_id();
		
	if($thumb_id) $thumb_url_array = wp_get_attachment_image_src($thumb_id, $size, false);
	($thumb_id)	? $class="" : $class=" no-image"
	
	?>
	<div class="entry<?php echo $class; ?>">		
	
	<?php if($thumb_id):?>
		
		<div class="entry-thumb">
                <a class="image thumb" href="<?php echo get_permalink(); ?>">
					<img src="<?php echo $thumb_url_array[0]; ?>" data-src="<?php echo $thumb_url_array[0];?>" width="<?php echo $thumb_url_array[1];?>" height="<?php echo $thumb_url_array[2];?>" alt="<?php echo $value['alt'];?>" />
				</a>
          </div>
	
	<?php endif;?>



	 <div class="entry-content">
              	<?php xborder_posted_on()?>
               	<h3><a href="<?php the_permalink(); ?>"><?php echo get_the_title();?></a></h3>
			   	<?php //the_excerpt();?>
			  <?php echo subString(get_the_excerpt(),160);?>
			   	<a class="read-more small-cta"  href="<?php the_permalink() ?>"> Read more</a>
       </div>

	
			
	</div>	

