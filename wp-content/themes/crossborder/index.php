<?php
/**
 * The main template file.
 *
 */

get_header(); ?>

	
	

		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'template-parts/content', get_post_format() );
				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>

		<?php else : ?>
<h2>index</h2>
			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>


<?php //get_sidebar(); ?>
<?php get_footer(); ?>
