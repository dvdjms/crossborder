<?php
/**
 * The template for displaying feature pages.

 */

get_header(); ?>
<!--page -->
<?php while ( have_posts() ) : the_post(); ?>

<?php if(get_the_content()):?>
<!-- Country page -->
<?php
	//$stacking = get_field('stack_images');
	$img = null;
	$i = get_field('header_image');
	$rp = get_field('risk_profile');
	$lu = get_field('last_updated');
	$cName = get_the_title();
	if($i) $img = getImage($i,'medium');
	?>

<section id="post-<?php the_ID(); ?>" class="section--module wordpress-content country-header fades">
	
	<div class="content">
		<div class="content--inner">
		<div class="copy copy-header">
			<?php if(get_field('country_introduction', 'options')) echo '<h4>' . get_field('country_introduction', 'options') . '</h4>'; ?>
			<?php the_title('<h1>','</h1>');?>
			<?php // the_content(); ?>
		<?php if(get_field('introduction_text')) echo '<p>' . get_field('introduction_text') . '</p>'; ?>
		</div>
			<?php if($rp || $lu):?>
				<div class="details">
					<?php if($rp) echo '<p>Risk profile: '.$rp.'</p>'; ?>
					<?php if($lu) echo '<p>Filing deadline: '.$lu.'</p>'; ?>
				</div>
			<?php endif;?>
		<?php if($img):?>
		<div class="image header-image">
					<?php echo $img['lazy']; ?>
		</div>
		
		
		<?php endif;?>
		
		</div>
	</div><!-- .entry-content -->
	

	<div class="content country--content">
		<div class="content--inner">
	
		<div class="copy">
		<?php //echo get_field('content'); ?>
		
		   <?php  if (get_field('content_list')): ?>
		   		
		   		<?php while( the_repeater_field('content_list') ):
			   		$tt = null;
		   			$tx = null;
		   			$tt = get_sub_field('title');
		   			$tx = get_sub_field('text');
		   			
		   			if($tt) echo '<h5>'. $tt . '</h5>';
		   			if ($tx) echo '<p>' . $tx . '</p>';
		   			
		   			?>
		   			
		   			
		   <?php 	endwhile; ?>
		  <?php  endif; ?>
		</div>
	
	
	
		<?php if(get_field('side_bar_title', 'options')):?>
		<div class="side-bar">
			<?php echo '<h4>' . get_field('side_bar_title', 'options') . '</h4>';?>
	   		<div class="item driver">
	   				
					<div class="copy">
						<?php 
							$copy =  null;
							$title = null;
							$link =  null;
							
							$copy = get_field('side_bar_copy', 'options');
							$title = get_field('sidebar-title', 'options');
							$link = get_field('sidebar-link', 'options'); ?>
						
						<?php if($title) echo '<h3>' . $title . '</h3>';?>


						<?php if($copy) echo  $copy;?>
						<?php if($link)		echo '<a href="'.$link['url'].'" class="cta">' . $link['title'] . '</a>'; ?>
					</div>
					<div class="image">
						<?php if(get_field('side_bar_image', 'options')):
							$img = getImage(get_field('side_bar_image', 'options'),'large');
							echo $img['lazy'];
							endif?>
					</div>
				</div>
		</div>
		<?php endif;?>
	
		</div>
	</div>
</section>
<?php  endif;?>



<?php include(get_template_directory().'/template-parts/content-scroller.php' ); ?>

<?php endwhile; // End of the loop. ?>
<?php get_footer(); ?>
