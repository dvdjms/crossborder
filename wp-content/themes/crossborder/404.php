<?php
/**
 * The template for displaying 404 pages (not found).
 *
 */

get_header(); ?>


<?php //include(get_template_directory().'/template-parts/content-header.php'); ?>	
	<section class="error-404">
			
			<div class="content">
			

					<?php get_template_part( 'template-parts/content', 'none' ); ?>
		
				</div>
			</section><!-- .error-404 -->

		
<?php get_footer(); ?>
