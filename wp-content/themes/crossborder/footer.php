<?php include(get_template_directory().'/template-parts/content-sign-off.php'); ?>
</main>

<footer class="site-footer fades">

  <?php  if (get_field('offices','options')): $count = count(get_field('offices','options'));?>
<section class="footer--offices num-<?php echo $count;?>">
	<div class="content">
	
	 
	   		
	   		<?php while( the_repeater_field('offices','options') ):
		   		$t = null;
		   		$e = null;
		   		$t = get_sub_field('phone');
		   		$e = get_sub_field('email');
		   		
		   		
		   		$tstrip = str_replace(' ', '', $t);
		   		$tstrip = str_replace('(', '', $tstrip);
		   		$tstrip = str_replace(')', '', $tstrip);
		   		
		   		?>
		   		<div class="office">
		   		
	   		<?php 	echo e_acf(get_sub_field('title'),'h4');
	   			if($t) echo '<p><a href="tel:' . $tstrip . '">' . $t . '</a></p>';
	   			if($e) echo '<p><a href="mailto:' . $e . '">' . $e . '</a></p>';
	   		?>
		   		</div>
	   <?php 	endwhile; ?>

	</div>
	
</section>
<?php  endif; ?>
<?php /*//---------------------------------------------------------------------------
	
	Navigation
	
	---------------------------------------------------------------------------*/ ?>

<section class="footer--navigation">
	<div class="content">
	<div class="footer-wrap">
		<div class="small-copy-wrap">
		<nav>	
			<span class="copyright">&copy; CrossBorder Solutions <?php echo date('Y'); ?> All Rights Reserved. </span>
			<?php 	
				$wrap = ' <ul class="%2$s">%3$s</ul>';
									
					wp_nav_menu( array( 'theme_location' => 'footer-menu', 
												'menu_id' =>'',
												 'menu_class'=>'', 
												 'container' =>'', 
												 'container_id' =>'',
												  'items_wrap' => $wrap) ); ?>
												  												 
		</nav>
		
		<?php echo e_acf(get_field('small_print','options'), 'span', 'class="smallcopy"');?>
		</div>		
		<div class="social-wrap">
		<?php	$wrap = '<ul class="social-menu">%3$s</ul>';
							
			wp_nav_menu( array( 'theme_location' => 'social', 
										'menu_id' =>'',
										 'menu_class'=>'', 
										 'container' =>'', 
										 'container_id' =>'',
										  'items_wrap' => $wrap) ); ?>
										  
		<a class="veracode image" href="https://www.veracode.com/verified/directory/cross-border-solutions" target="_blank"><img src="<?php echo get_template_directory_uri();?>/images/png/veracode.png"></a>
		</div>	
			
	</div>
	</div>
</section>

</footer>

<div id="modal-holder" class="modal-form">
	
	<div class="modal-content">
		<a href="#" class="hide-modal">close</a>
		<div class="content" id = "form-holder">			
			<?php  
					$tagline = 	null;
					$title = 	null;
					$text = 	null;
					
					$tagline = get_field('form-tagline', 'options');
					$title = get_field('form-title', 'options');
					$text = get_field('form-text', 'options');?>
					
					<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
					<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
					<?php if($text) 	echo $text;?>

				
				
				<?php if( get_field('form_intro_text','options')) echo get_field('form_intro_text','options'); ?>
			<?php $shortcode = get_field('form_shortcode','options');
				
					echo do_shortcode($shortcode);?>
		</div>	
	</div>

</div>



<?php wp_footer(); ?>

</body>

<script type="text/javascript"> _linkedin_partner_id = "1363548"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1363548&fmt=gif" /> </noscript>

<script type="text/javascript"> _linkedin_partner_id = "1659156"; window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || []; window._linkedin_data_partner_ids.push(_linkedin_partner_id); </script><script type="text/javascript"> (function(){var s = document.getElementsByTagName("script")[0]; var b = document.createElement("script"); b.type = "text/javascript";b.async = true; b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js"; s.parentNode.insertBefore(b, s);})(); </script> <noscript> <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1659156&fmt=gif" /> </noscript>
</html>