<?php
/**
 * The template for displaying the Rewards pages.

 Template Name: Points page template
 */

get_header(); ?>
<!--Career page -->
<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'template-parts/content', 'page' ); ?>


	
<?php if(get_field('table_rows')):
	
	
	get_field('activity_title') ? $at = get_field('activity_title') : $at = 'Activity';
	get_field('points_title') ? $pt = get_field('points_title') : $pt = 'Points';?>
	<a id="points" class="anchor"></a>
	<section class="section--points fades" >
	<div class="content">
	<?php get_field('title') ? $title = get_field('title') : $title = 'How to earn points'; ?>
		<h3 class="table-title"><?php echo $title; ?></h3>
		
		<table class="points-table">
		<tr><th><?php echo $at;?></th><th><?php echo $pt;?></th></tr>
		<?php
		while( the_repeater_field('table_rows') ):	   			 
			$a = get_sub_field('activity');
			$p = get_sub_field('fiona_points_earned');?>
	
		 <tr><td><?php echo $a;?></td><td><?php echo number_format($p);?> Points</td></tr>  			
		<?php endwhile; ?>
		</table>
	</div>
</section>


<?php endif; ?>

<?php get_template_part( 'template-parts/content', 'blocks' ); ?>
<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
