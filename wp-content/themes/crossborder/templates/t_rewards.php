<?php
/**
 * The template for displaying the Rewards pages.

 Template Name: Rewards page template
 */

get_header(); ?>
<!--Career page -->
<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'template-parts/content', 'page' ); ?>
<?php get_template_part( 'template-parts/content', 'blocks' ); ?>

	

<a id="rewards" class="anchor"></a>
<section class="section--rewards fades" >


	<div class="content">
	
			<div class="copy">
				<?php echo $title ?	 '<h2>'.$title.'</h2>' : '<h2>Browse rewards</h2>';?>
				
				<ul class="category-list">
				<?php  
					
					 $args = array(
				      	'taxonomy' => 'reward-category',
				        'separator'           => '',
				        'hide_title_if_empty' => true,
				        'show_count'          => 0,
				        'title_li'    => ''
				    );?>
					<li class="cat-item"><a href="<?php echo get_post_type_archive_link('reward'); ?>" class="active">Browse all</a></li>
				<?php wp_list_categories( $args); ?>
				   				
				
					</ul>
					<form name="rewardFilter" id="reward-filter" class="listing-filters" action="<?php echo get_post_type_archive_link('reward'); ?>" method="post" onsubmit="return false;">
						<fieldset>
							<p class="legend">Sort by:</p>
							<input type ="radio" name="sort" value="newest" 		id="r_1" checked ><label for="r_1">Newest</label>
							<input type ="radio" name="sort" value="point_value" 	id="r_2"><label for="r_2">Low to high</label>
						</fieldset>
						<fieldset class="fieldset--search">
							<label>What can you get for your points:</label><input type="number" name="points" value="" placeholder="5000">
							<a href="" class="clear-input">clear</a>
						</fieldset>

					</form>

				
				
			</div>
		
		
		<?php // JS LOADS IN rewards ?>
		<div class="wrapper"  id="load-holder" >
			<div class="listing--rewards listing"> </div>
			
			
			<?php 
				
				//next_posts_link( 'Load more', $the_query->max_num_pages );
	
				?>
		</div>
	</div>
</section>
<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
