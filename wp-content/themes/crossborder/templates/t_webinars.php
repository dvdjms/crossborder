<?php
/**
 * The template for displaying the Webinar pages.

 Template Name: Webinar page template
 */
get_header(); ?>
<?php while ( have_posts() ) : the_post();?>

<section class="section--webinar">

    <div class="content fades">
        <div class="copy">
            <h1><?php echo get_the_title();?></h1>
            <?php the_content(); ?>
        </div>
    </div>
    <div class="content webinar-intro fades">
        <?php if ( get_field('webinar_sections') ) : while( have_rows('webinar_sections') ) : the_row(); 
              $webinar_image = get_sub_field('webinar_image') ? getImage(get_sub_field('webinar_image'),'large') : null;   ?>
                <div class="webinar-intro-section desktop">
                 <?php  echo  $webinar_image ? '<div class="image">' . $webinar_image['lazy'] . '</div>' : ''; ?>
                 <?php  echo  get_sub_field('intro_text') ? '<div class="copy">' . get_sub_field('intro_text') . '</div>' : ''; ?>
                </div>
         <?php endwhile; endif; ?>
    </div>

    <div class="content webinars-lists fades">

         <?php if ( get_field('webinar_sections') ) : while( have_rows('webinar_sections') ) : the_row(); $count = 1;
            $webinar_image = get_sub_field('webinar_image') ? getImage(get_sub_field('webinar_image'),'large') : null;   
           $class = 'webinar-section-'. $count;?>
            <div class="webinar-section <?php echo $class;?>"> 
            <?php  echo  $webinar_image ? '<div class="image mobile">' . $webinar_image['lazy'] . '</div>' : ''; ?>
                 <?php  echo  get_sub_field('intro_text') ? '<div class="copy mobile">' . get_sub_field('intro_text') . '</div>' : ''; ?>
               
                <?php /* FILTERING */ ?>
                 <?php if ( get_sub_field('filters') ) :?>
                    <div class="archive-categories webinar-categories" onclick="toggleClass(event)">
                    <div class="legend"><span>All Webinars </span>
                    <svg height="4" viewBox="0 0 10 4" width="10" xmlns="http://www.w3.org/2000/svg"><path d="m1151 19 4 4 4-4" fill="none" stroke="#ffffff" transform="translate(-1150 -19)"></path></svg>
                    </div>
                    <ul class="list-categories">
                    <li class="cat-item"><a href="#" class="active" data-filter="all" data-section="<?php echo $class;?>">All Webinars </a></li>
                
                 <?php while( have_rows('filters') ) : the_row();  
                     $filter = get_sub_field('filter');
                     $data_filter = cleanStringForClass($filter); 
                     ?> 
                 <li class="cat-item">
                    <a href="#" data-filter ="<?php echo $data_filter;?>" data-section="<?php echo $class;?>"><?php echo $filter; ?></a>
                </li>
                
                      
                 <?php endwhile; ?> 
                    </ul>    
                    </div>
                    <script>
                    function toggleClass(event) {
                        event.target.classList.toggle('list-open')
                        }
                    </script>
                <?php endif;?>
               

                <?php /* Webinars */ ?>
                <?php if ( get_sub_field('webinars') ) : while( have_rows('webinars') ) : the_row();   
                        $text = get_sub_field('text') ? get_sub_field('text') : null;
                        $link = get_sub_field('link') ? get_sub_field('link') : null;
                        $line_above = get_sub_field('line_above') ? get_sub_field('line_above') : null;
                        $number = get_sub_field('number') ? get_sub_field('number') : null;
                        if( $link ): 
                            $link_url = $link['url'];
                            $link_target = $link['target'] ? $link['target'] : '_self';
                           
                            $data = $number ? ' ' . cleanStringForClass($number)  : '';
                        endif; ?>
                       
                        <article class="entry<?php if($data) echo $data; ?>">
                        <?php if($line_above ) echo '<hr>';?>
                            <?php if( $link ):?>
                            <a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>">   
                            <?php endif;?>
                              
                                <?php echo  $number  ?  '<span>' . $number   .'</span>' : '';?>
                                <?php echo  get_sub_field('title') ?  '<h1>' . get_sub_field('title')  .'</h1>' : '';?>
                                <?php echo  get_sub_field('text') ?  '<div class="copy">' . get_sub_field('text')  .'</div>' : '';?>
                           
                            <?php if( $link ):?>
                            </a>
                            <?php endif;?>
                        </article>
                       
                 <?php endwhile; endif; ?>
            </div>
        <?php endwhile; endif; ?>
    
    </div>

</section>


<?php get_template_part( 'template-parts/content', 'blocks' ); ?>


<?php endwhile; // End of the loop. ?>
<?php get_footer(); ?>