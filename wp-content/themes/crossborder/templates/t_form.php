<?php
/**
 * The template for displaying the Form pages.

 Template Name: Form page template
 */
get_header(); ?>
<section class="section--form">

<div class="content fades">
<div class="form-holder"  id = "form-holder">			
    <?php  

            $tagline = get_field('form-tagline') ? get_field('form-tagline' ) : null;
            $title = get_field('form-title' ) ? get_field('form-title' )  : null;
            $text = get_field('form-text' ) ? get_field('form-text' ) : null;
            $image = get_field('image' ) ? getImage(get_field('image' ), 'large') : null;
            if($tagline) 	echo '<h4>'.$tagline.'</h4>';
            if($title) 	echo '<h2>'.$title.'</h2>';
            if($text) 	echo '<p class="form-intro">' . $text .'</p>';
           

    $shortcode = get_field('form_shortcode',);
        
            echo do_shortcode($shortcode);?>
</div>
<?php  if( $image) 	echo '<div class="image">' . $image['lazy'] . '</div>';?>	
</div>

</secion>

<?php get_footer(); ?>