<?php
/**
 * The template for displaying the Archive pages.

 Template Name: Archive page template
 */
get_header(); 


$image = get_field('image') ? getImage(get_field('image'), 'large') : null;
$archive_type = get_field('archive_type') ? get_field('archive_type') : null;
if($archive_type):
    $taxonomies = get_object_taxonomies($archive_type, 'objects');
   
?>


<section class="section--archive" data-archive="<?php echo get_post_type_archive_link($archive_type); ?>">

<div class="content fades">
    <div class="copy">
        <h1><?php echo get_the_title();?></h1>
        <?php the_content(); ?>

        <?php  if( $taxonomies): foreach($taxonomies as $taxonomy):
             
				$args = array(
                       'taxonomy' => $taxonomy->name,
                       'separator'           => '',
                       'hide_title_if_empty' => true,
                       'show_count'          => 0,
                       'title_li'    => ''
                   );
                 $n = count(get_categories( $args));
                 if($n > 0):  ?>
                 <div class="archive-categories" onclick="toggleClass(event)">
                    <div class="legend"><span><?php echo $taxonomy->label;?> </span>
                    <svg height="4" viewBox="0 0 10 4" width="10" xmlns="http://www.w3.org/2000/svg"><path d="m1151 19 4 4 4-4" fill="none" stroke="#ffffff" transform="translate(-1150 -19)"></path></svg>
                    </div>
                    <ul class="list-categories">
                      <li class="cat-item"><a href="<?php echo get_post_type_archive_link($archive_type); ?>" class="active">All <?php echo $taxonomy->label;?></a></li>
                      <?php wp_list_categories( $args); ?>
                    </ul>    
                </div>
                <script>
                function toggleClass(event) {
                    event.target.classList.toggle('list-open')
                    }
                </script>
      <?php  endif; endforeach; endif;?>
      <div class="wrapper"  id="load-holder" >
        <div class="archive-list-holder load-content fades">	
       
           
        </div>
        </div>
    </div>
<?php  

echo $image ? '<div class="image">' . $image['lazy'] . '</div>' : null ;?>	
</div>

</section>

<?php endif;?>
<?php get_template_part( 'template-parts/content', 'blocks' ); ?>


<?php get_footer(); ?>