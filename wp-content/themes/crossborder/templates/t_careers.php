<?php
/**
 * The template for displaying the Careers pages.

 Template Name: Careers page template
 */

get_header(); ?>
<!--Career page -->
<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'template-parts/content', 'page' ); ?>
<?php get_template_part( 'template-parts/content', 'blocks' ); ?>



<?php if(get_field('carousel_text')): 
	$dotsHtml ='';
	$tagline = get_field('diversity-tagline');
	$title = get_field('diversity-title');
	$text = get_field('diversity-text');
	$count = count(get_field('carousel_text'));
?>
<section class="section--diversity fades">
	<div class="content">
		<div class="content--inner">
		<div class="copy">
			<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
			<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
			<?php if($text) 	echo $text;?>
		</div>
		<div class="diversity-carousel">
			<div class="carousel" id="carousel--diversity" data-loop = true data-autoplay = true data-duration="600">  		
   	<?php while( the_repeater_field('carousel_text') ):
	   		$tl = null;
   			$bl = null;
   			$tl = get_sub_field('top_line');
   			$bl = get_sub_field('bottom_line');
   			$dotsHtml .= '<button class="dot"></button>';
   			 ?>
   			<div class="item">
					<div class="item-copy">
						<p class="big-title"><?php echo $tl;?></p>
						<p><?php echo $bl;?></p>
					</div>
				</div>
   	<?php endwhile; ?>
   			</div>
   			<?php if($count>1):?>
			 <div class="carousel-pagination dots">
		   		<?php echo $dotsHtml; ?>
		   	</div>
			<?php endif;?>
		</div>
		</div>
	</div>
</section>

   			
<?php  endif; ?>


<?php  if (get_field('benefits')): 
	$dotsHtml ='';
	$tagline = get_field('benefits-tagline');
	$title = get_field('benefits-title');
	$text = get_field('benefits-text');
	$count = count(get_field('benefits'));
?>
<section class="section--benefits fades">
	<div class="content">
		<div class="content--inner">
		
		<div class="copy">
			<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
			<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
			<?php if($text) 	echo $text;?>
		</div>
	
		<div class="listing--benefits mobile-carousel" id="benefits">	
		<?php while( the_repeater_field('benefits') ):
			$ic = get_sub_field('icon');
			$tx = get_sub_field('text');
			if($ic) $img = getImage($ic,'large');
			$dotsHtml .= '<button class="dot"></button>';
		?>
		<article class="item entry--benefit">
			<?php if($img) echo'<div class="image">' . $img['img'] . '</div>'; ?>
			<?php if($tx) echo '<p>' . $tx . '</p>'; ?>				
		</article>
	<?php 	endwhile; ?>
	</div>
	<?php include(get_template_directory().'/template-parts/carousel-buttons.php');?>
	<?php if($count>1):?>
			 <div class="carousel-pagination dots">
		   		<?php echo $dotsHtml; ?>
		   	</div>
	<?php endif;?>
		</div>
	</div>
</section>
<?php  endif; ?>
	
<?php
	$tagline = get_field('jobs-tagline');
	$title = get_field('jobs-title');
	$text = get_field('jobs-text');
?>


<?php /*
<a id="roles" class="anchor"></a>
<section class="section--jobs fades" >

<div class="content">
	<div class="content--inner">
		<div class="copy">
			<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
			<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
			<?php if($text) 	echo $text;?>
		</div>
	</div>

	<div id="new-list">
	
	  <div id="lever-jobs-filter" class="listing-filters" >

		
		<fieldset class="multiple-select lever-jobs-filter-departments"><legend><span>All departments</span><svg height="4" viewBox="0 0 10 4" width="10" xmlns="http://www.w3.org/2000/svg"><path d="m1151 19 4 4 4-4" fill="none" stroke="#ffffff" transform="translate(-1150 -19)"/></svg></legend><div class="checks"><label class="label-checkbox"><input type="checkbox" value="All departments" name="department"><span class="psuedo-checkbox">All departments</span></label></div></fieldset>
		
		<fieldset class="multiple-select lever-jobs-filter-locations"><legend><span>All locations</span><svg height="4" viewBox="0 0 10 4" width="10" xmlns="http://www.w3.org/2000/svg"><path d="m1151 19 4 4 4-4" fill="none" stroke="#ffffff" transform="translate(-1150 -19)"/></svg></legend><div class="checks"><label class="label-checkbox"><input type="checkbox" value="All locations" name="location"><span class="psuedo-checkbox">All locations</span></label></div></fieldset>
		

	  
	  </div>
	   <div class="list listing--jobs">
	   	
	   </div>
	     <div id="lever-no-results" style="display: none;">
	     <h1 class="page-title">No matching vacancies</h1>
		 <p>We don’t currently have what you’re looking for. Please check out our other vacancies.</p>
		 </div>

	</div>

<div id='lever-jobs-container'></div> 

</div>
</section>
*/ ?>
<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
