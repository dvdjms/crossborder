<?php
/**
 * The template for displaying feature pages.

 Template Name: Feature template
 */

get_header(); ?>
<!--page -->
<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'template-parts/content', 'page' ); ?>

<section class="section--content fades">
	<div class="content">
		<div class="content--inner">
	
		<div class="copy">
		<?php echo get_field('content'); ?>
		</div>
	
	
	
		<?php if(get_field('side_bar_title')):?>
		<div class="side-bar">
			<?php echo '<h4>' . get_field('side_bar_title') . '</h4>';?>
	   		<div class="item driver">
	   				
					<div class="copy">
						
						<?php if(get_field('side_bar_copy')) echo  get_field('side_bar_copy');?>
						
					</div>
					<div class="image">
						<?php if(get_field('side_bar_image')):
							$img = getImage(get_field('side_bar_image'),'large');
							echo $img['lazy'];
							endif?>
					</div>
				</div>
		</div>
		<?php endif;?>
	
		</div>
	</div>
</section>



<?php
global $post;
$direct_parent = $post->post_parent;

if(get_field('content_blocks',$direct_parent) && get_field('show_carousel')):  

while ( have_rows('content_blocks', $direct_parent) ) : the_row();
/*-----------------------------------------------------------------------------
	
		Carousel

---------------------------------------------------------------------------*/ 

	$hide_item = 0;
	if(get_field('ignore')) $hide_item = get_field('ignore');
	
	if( get_row_layout() == 'carousel'):
		if(get_sub_field('type') == 'full') include(get_template_directory().'/template-parts/content-carousel.php');
	endif;
	
	endwhile; // end content sections while

endif; // get content sections; 
?>	



<?php endwhile; // End of the loop. ?>
<?php get_footer(); ?>
