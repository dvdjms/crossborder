<?php
/**
 * The template for displaying single jobs.
 	
 
 Template Name: Single job template	
 	
 */

get_header(); ?>
<?php
	 $array=[];
	
	 $jobId = null;
if(isset($_GET['id'])) {
	$jobId = $_GET['id'];
	$url = 'https://api.lever.co/v0/postings/xbs/'. $jobId;
	$json = getJson($url);
	$array = cvf_convert_object_to_array($json);
}


if (isset($array['id']) && $array['id'] == $jobId && !empty($array)):
				
				foreach($array as $key2 => $value2) {
					$$key2 = $value2;
				//	echo $key2.' : '.$value2. '<br>';
					
				}?>
			
			
	<section id="post-<?php the_ID(); ?>" class="child-content post-content fades">
	<div class="content">
	<div class="content-wrapper">
	<h4>Current opportunities</h4>
	<?php if($text) echo '<h1>' . $text . '</h1>'; ?>
	<?php if($categories['location']) echo '<h1 class="title-location">' . $categories['location'] . '</h1>';?>

	<div class="copy">
		<div class="lever_description"><?php if($description) echo $description; ?></div>
		<?php if($lists):?>
			<div class="lever_lists">
			<?php foreach($lists as $list):
			
				if(isset($list['text'])) echo '<strong>' . $list['text'] . '</strong>';
				if(isset($list['content'])) echo '<ul>' . $list['content'] . '</ul>';	
							
			endforeach; ?>
			</div>
		<?php endif; ?>
		
		<?php if(isset($additional)) echo '<div class="lever_additional">' .$additional . '</div>'; ?>
		
		
		<?php if($applyUrl) echo '<a href="'.$applyUrl.'" class="lever_cta cta" target="_blank">Apply here</a>';?>
	</div><!--end copy-->		

	</div>
	</div><!-- .entry-content -->
	
</section><!-- #post-## -->		
			
			
			
		<?php	 else: 
						
		
			return;
endif;	

?>




<?php while ( have_posts() ) : the_post(); ?>




<?php endwhile; // End of the loop. ?>
<?php get_footer(); ?>
