<?php
/**
 * The template for displaying single jobs.
 	
 	
 	
 */

get_header(); ?>
<!--page -->
<?php while ( have_posts() ) : the_post(); ?>
<?php get_template_part( 'template-parts/content', 'single-job' ); ?>



<?php endwhile; // End of the loop. ?>
<?php get_footer(); ?>
