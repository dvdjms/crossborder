<?php
/**
 * The template for displaying all single article posts.

 */

get_header(); ?>

<!--single article-->
			<?php while ( have_posts() ) : the_post(); ?>
			
			<?php get_template_part( 'template-parts/content', 'single-article' ); ?>

		<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
