<?php
/**
 * The template for displaying archive pages.
 *
 */

get_header(); ?>

	<!---archive main -->

		<section class="archive-jobs">
			<div class="content">	
				<div class="listing--jobs">
			
			<?php if ( have_posts() ) : ?>
			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					 include(get_template_directory().'/template-parts/entry--job.php'); 

				?>

			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>


		<?php else : ?>
	
			<?php get_template_part( 'template-parts/content', 'none' ); ?>


		<?php endif; ?>
		
		</div>
			</div>
	</section>
<?php get_footer(); ?>
