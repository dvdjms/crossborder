<?php
/**
 * The template for displaying the Careers pages.

 Template Name: Careers page template
 */

get_header(); ?>
<!--Career page -->
<?php while ( have_posts() ) : the_post(); ?>

<?php get_template_part( 'template-parts/content', 'page' ); ?>
<?php get_template_part( 'template-parts/content', 'blocks' ); ?>



<?php if(get_field('carousel_text')): 
	$dotsHtml ='';
	$tagline = get_field('diversity-tagline');
	$title = get_field('diversity-title');
	$text = get_field('diversity-text');
	$count = count(get_field('carousel_text'));
?>
<section class="section--diversity fades">
	<div class="content">
		<div class="content--inner">
		<div class="copy">
			<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
			<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
			<?php if($text) 	echo $text;?>
		</div>
		<div class="diversity-carousel">
			<div class="carousel" id="carousel--diversity" data-loop = true data-autoplay = true data-duration="600">  		
   	<?php while( the_repeater_field('carousel_text') ):
	   		$tl = null;
   			$bl = null;
   			$tl = get_sub_field('top_line');
   			$bl = get_sub_field('bottom_line');
   			$dotsHtml .= '<button class="dot"></button>';
   			 ?>
   			<div class="item">
					<div class="item-copy">
						<p class="big-title"><?php echo $tl;?></p>
						<p><?php echo $bl;?></p>
					</div>
				</div>
   	<?php endwhile; ?>
   			</div>
   			<?php if($count>1):?>
			 <div class="carousel-pagination dots">
		   		<?php echo $dotsHtml; ?>
		   	</div>
			<?php endif;?>
		</div>
		</div>
	</div>
</section>

   			
<?php  endif; ?>


<?php  if (get_field('benefits')): 
	$dotsHtml ='';
	$tagline = get_field('benefits-tagline');
	$title = get_field('benefits-title');
	$text = get_field('benefits-text');
	$count = count(get_field('benefits'));
?>
<section class="section--benefits fades">
	<div class="content">
		<div class="content--inner">
		
		<div class="copy">
			<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
			<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
			<?php if($text) 	echo $text;?>
		</div>
	
		<div class="listing--benefits mobile-carousel" id="benefits">	
		<?php while( the_repeater_field('benefits') ):
			$ic = get_sub_field('icon');
			$tx = get_sub_field('text');
			if($ic) $img = getImage($ic,'large');
			$dotsHtml .= '<button class="dot"></button>';
		?>
		<article class="item entry--benefit">
			<?php if($img) echo'<div class="image">' . $img['img'] . '</div>'; ?>
			<?php if($tx) echo '<p>' . $tx . '</p>'; ?>				
		</article>
	<?php 	endwhile; ?>
	</div>
	<?php include(get_template_directory().'/template-parts/carousel-buttons.php');?>
	<?php if($count>1):?>
			 <div class="carousel-pagination dots">
		   		<?php echo $dotsHtml; ?>
		   	</div>
	<?php endif;?>
		</div>
	</div>
</section>
<?php  endif; ?>
	
<?php
	$tagline = get_field('jobs-tagline');
	$title = get_field('jobs-title');
	$text = get_field('jobs-text');
?>



<a id="roles" class="anchor"></a>



<section class="section--jobs fades" >


	<div class="content">
	<div class="content--inner">
		<div class="copy">
			<?php if($tagline) 	echo '<h4>'.$tagline.'</h4>';?>
			<?php if($title) 	echo '<h2>'.$title.'</h2>';?>
			<?php if($text) 	echo $text;?>
		</div>
	</div>
	<?php
		$departments = get_terms(array('taxonomy' => 'department-category'));
		$locations = get_terms(array('taxonomy' => 'location-category'));		
	?>
			
	<form name="jobFilter" id="job-filter" class="listing-filters" action="<?php echo get_post_type_archive_link('job'); ?>" method="post">
	<?php echo buildSelect($departments, 'All departments'); ?>
	<?php echo buildSelect($locations, 'All locations'); ?>
	<input type="submit" value ="filter jobs" class="job-filter-submit">
	</form>
	<?php // JS LOADS IN JOBS ?>
	<div class="wrapper"  id="load-holder" >
			<div class="listing--jobs"> </div>
	</div>
	</div>
</section>
<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
