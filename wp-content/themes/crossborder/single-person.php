<?php
/**
 * The template for displaying all single People.

 */

get_header(); ?>

<!--single person -->

<?php while ( have_posts() ) : the_post(); 
			

	
$img = null;


$p = null;
$i =  null;
$img = null;
$position = '';
$li = null;
$em = null;

$p = get_field('position');
$i = get_field('headshot');

if($i) $img = getImage($i, 'large');
if($p) $position = '<span class-"pos">' . $p . '</span>';

$li = get_field('linkedin');
$em = get_field('email');
?>

<section class="section--person child-content" id="modal-content">
	<div class="content">
	<?php if($img) echo '<div class="image">' .$img['lazy'] .'</div>';?>
		
	
		<div class="copy">
			<?php echo '<h1>' . get_the_title() .'</h1>';
				echo '<h4>' . $position . '</h4>';
				
				the_content();
				if($em ||$li):
				echo '<div class="links">';
				if($em) echo '<a href="mailto:'.$em.'" class="email-link">'.$em.'</a>';
				if($li) echo '<a href="'.$li.'" class="linkedin-link">'.$li.'</a>';
				echo '</div>';
				endif;
 ?>
		</div><!--end copy-->		

	</div><!-- .entry-content -->

</section><!-- #post-## -->



		<?php endwhile; // End of the loop. ?>

<?php get_footer(); ?>
