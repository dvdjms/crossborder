<?php
/**
 * The template for displaying archive taxonomy page.
 *
 */

get_header(); 

?>

<section class="archive-articles">
	<div class="content">	

		<?php if ( have_posts() ) :  ?>
		
			<div class="wrapper"  id="load-holder">
				<div class="archive-list-holder load-content fades">
			<?php while ( have_posts() ) : the_post();  
				
				include(get_template_directory().'/template-parts/entry-hub.php');  
			 
			 endwhile;
			 
			echo '<div class="load-more">'; 
				next_posts_link( 'Load more' );
			echo '</div>';
			?>
			</div>
			</div>

		
		
		<?php else : ?>
			<div class="wrapper"  id="load-holder">
			<div class="archive-list-holder load-content fades">
			<?php //get_template_part( 'template-parts/content', 'no-rewards' ); ?>
			</div>
			</div>
		<?php endif; ?>
		
	</div>
</section>



<?php get_footer(); ?>