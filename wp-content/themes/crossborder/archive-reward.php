<?php
/**
 * The template for displaying archive job pages.
 *
 */
if(strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    wp_redirect( get_permalink( get_page_by_path( 'Rewards' ) ));
    //echo $_SERVER['HTTP_X_REQUESTED_WITH'];
}

get_header(); ?>
<!---archive reward -->
<?php include(get_template_directory().'/template-parts/content-rewards.php'); ?>
<?php get_footer(); ?>