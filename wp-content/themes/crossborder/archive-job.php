<?php
/**
 * The template for displaying archive job pages.
 *
 */

get_header(); ?>

	<!---archive main -->

		<section class="archive-jobs">
			<div class="content">	
<!-- 				<div class="wrapper"  id="load-holder" data-max="<?php echo $max; ?>" data-page="<?php echo $paged; ?>"> -->

		
		<?php if ( have_posts() ) : 
			if( $wp_query->found_posts ) $n = $wp_query->found_posts;
			$class=' no-arm';
			if($n > 2) $class=' show-arm'
		?>
		
			<?php /* Start the Loop */ ?>
				<div class="wrapper"  id="load-holder">
				<div class="listing--jobs listing load-content<?php echo $class;?>">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					 include(get_template_directory().'/template-parts/entry-job.php'); 

				?>

			<?php endwhile; ?>

			<?php 	the_posts_pagination( array( 'mid_size'  => 1, 'prev_next' => true ) ); ?>
			</div>
			</div>

		<?php else : ?>
			<div class="wrapper"  id="load-holder">
			<div class="listing--jobs listing load-content">
			<?php get_template_part( 'template-parts/content', 'no-jobs' ); ?>
			</div>
			</div>
		<?php endif; ?>
		

			</div>
	</section>
<?php get_footer(); ?>



