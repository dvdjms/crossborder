<!DOCTYPE html>
<?php
function language()
 {
     if (!function_exists('pll_the_languages')) {
         return;
     }
     return pll_the_languages();
 }
 ?>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php if ( is_singular( 'reward' ) ) echo '<meta name="robots" content="noindex">';?>
<script>
	document.write('<style> .fades{opacity: 0};</style>');
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WFPMF7S');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WFPMF7S"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<a class="screen-reader-text" href="#maincontent">Skip to main content</a>

<header class="header--site fades">
	<div class="content">
	<a href="<?php echo esc_url( home_url() ); ?>" class="logo desktop" id="site-logo-desktop">
		<img src="<?php echo get_template_directory_uri() .'/images/svg/logo.svg'; ?>"  alt="cross border logo" />	
	</a>
	<a href="<?php echo esc_url( home_url() ); ?>" class="logo mobile" id="site-logo">
		<img src="<?php echo get_template_directory_uri() .'/images/svg/logo-mobile.svg'; ?>"  alt="cross border logo" />	
	</a>
	
	<button class="menu-button">  <span>toggle menu</span></button> 
			
	<nav class="nav--main">	
	<?php $dropdown = '<div class="drop-down" id="getstarted"> <div class="links"><a href="https://www.crossborder.ai//book-a-transfer-pricing-demo/">Transfer Pricing</a> <a href="https://www.crossborder.ai//book-an-rd-tax-credit-demo/">R&D Tax Credit</a> </div></div>';?>

	<?php $wrap = '<ul class="%2$s">%3$s<li><a href="#getstarted" class="cta">Book a demo</a>'. $dropdown.'</li></ul>';
								 
			   wp_nav_menu( array( 
					'theme_location' => 'main-menu', 
					'menu_id' => '', 
					'menu_class'=>'main-menu', 
					'container' =>'ul', 
					'container_id' =>'', 
					'items_wrap' => $wrap) ); ?>
	</nav>
	</div>
</header>
<main id="maincontent">





